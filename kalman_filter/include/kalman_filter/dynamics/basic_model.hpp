/**
 * @File: basic_model.hpp
 * @Date: May 2022
 * @Author: James Swedeen
 *
 * @brief
 * A simple model for 3 dimensional systems.
 **/

#ifndef KALMAN_FILTER_DYNAMICS_BASIC_MODEL_HPP
#define KALMAN_FILTER_DYNAMICS_BASIC_MODEL_HPP

/* C++ Headers */
#include<memory>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */
#include<kalman_filter/helpers/dimension_struct.hpp>
#include<kalman_filter/math/quaternion.hpp>
#include<kalman_filter/noise/noise_base.hpp>
#include<kalman_filter/dynamics/dynamics_base.hpp>
#include<kalman_filter/sensors/inertial_measurements/imu_base.hpp>

namespace kf
{
namespace dynamics
{
template<typename SCALAR, Eigen::StorageOptions OPTIONS>
class BasicModel;

template<typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
using BasicModelPtr = std::shared_ptr<BasicModel<SCALAR,OPTIONS>>;

/**
 * @BasicModelDim
 **/
struct BasicModelDim
 : public Dimensions<15,25,25,7,24,24,6,6,15>
{
public:
  struct REF
  {
  public:
    inline static constexpr const Eigen::Index NORTH_IND      = 0;
    inline static constexpr const Eigen::Index EAST_IND       = 1;
    inline static constexpr const Eigen::Index DOWN_IND       = 2;
    inline static constexpr const Eigen::Index ROLL_IND       = 3;
    inline static constexpr const Eigen::Index PITCH_IND      = 4;
    inline static constexpr const Eigen::Index YAW_IND        = 5;
    inline static constexpr const Eigen::Index NORTH_VEL_IND  = 6;
    inline static constexpr const Eigen::Index EAST_VEL_IND   = 7;
    inline static constexpr const Eigen::Index DOWN_VEL_IND   = 8;
    inline static constexpr const Eigen::Index ROLL_RATE_IND  = 9;
    inline static constexpr const Eigen::Index PITCH_RATE_IND = 10;
    inline static constexpr const Eigen::Index YAW_RATE_IND   = 11;
    inline static constexpr const Eigen::Index X_ACCEL_IND    = 12;
    inline static constexpr const Eigen::Index Y_ACCEL_IND    = 13;
    inline static constexpr const Eigen::Index Z_ACCEL_IND    = 14;

    inline static constexpr const Eigen::Index POS_START_IND        = NORTH_IND;
    inline static constexpr const Eigen::Index POS_END_IND          = DOWN_IND;
    inline static constexpr const Eigen::Index EULER_START_IND      = ROLL_IND;
    inline static constexpr const Eigen::Index EULER_END_IND        = YAW_IND;
    inline static constexpr const Eigen::Index VEL_START_IND        = NORTH_VEL_IND;
    inline static constexpr const Eigen::Index VEL_END_IND          = DOWN_VEL_IND;
    inline static constexpr const Eigen::Index EULER_RATE_START_IND = ROLL_RATE_IND;
    inline static constexpr const Eigen::Index EULER_RATE_END_IND   = YAW_RATE_IND;
    inline static constexpr const Eigen::Index ACCEL_START_IND      = X_ACCEL_IND;
    inline static constexpr const Eigen::Index ACCEL_END_IND        = Z_ACCEL_IND;
  };
  struct TRUTH
  {
  public:
    inline static constexpr const Eigen::Index NORTH_IND                      = 0;
    inline static constexpr const Eigen::Index EAST_IND                       = 1;
    inline static constexpr const Eigen::Index DOWN_IND                       = 2;
    inline static constexpr const Eigen::Index QUAT_W_IND                     = 3;
    inline static constexpr const Eigen::Index QUAT_X_IND                     = 4;
    inline static constexpr const Eigen::Index QUAT_Y_IND                     = 5;
    inline static constexpr const Eigen::Index QUAT_Z_IND                     = 6;
    inline static constexpr const Eigen::Index NORTH_VEL_IND                  = 7;
    inline static constexpr const Eigen::Index EAST_VEL_IND                   = 8;
    inline static constexpr const Eigen::Index DOWN_VEL_IND                   = 9;
    inline static constexpr const Eigen::Index HEADING_BIAS_IND               = 10;
    inline static constexpr const Eigen::Index ABS_PRESSURE_BIAS_IND          = 11;
    inline static constexpr const Eigen::Index FEATURE_RANGE_BIAS_IND         = 12;
    inline static constexpr const Eigen::Index FEATURE_BEARING_ROLL_BIAS_IND  = 13;
    inline static constexpr const Eigen::Index FEATURE_BEARING_PITCH_BIAS_IND = 14;
    inline static constexpr const Eigen::Index FEATURE_BEARING_YAW_BIAS_IND   = 15;
    inline static constexpr const Eigen::Index GPS_NORTH_BIAS_IND             = 16;
    inline static constexpr const Eigen::Index GPS_EAST_BIAS_IND              = 17;
    inline static constexpr const Eigen::Index GPS_DOWN_BIAS_IND              = 18;
    inline static constexpr const Eigen::Index GYRO_BIAS_X_IND                = 19;
    inline static constexpr const Eigen::Index GYRO_BIAS_Y_IND                = 20;
    inline static constexpr const Eigen::Index GYRO_BIAS_Z_IND                = 21;
    inline static constexpr const Eigen::Index ACCEL_BIAS_X_IND               = 22;
    inline static constexpr const Eigen::Index ACCEL_BIAS_Y_IND               = 23;
    inline static constexpr const Eigen::Index ACCEL_BIAS_Z_IND               = 24;

    inline static constexpr const Eigen::Index POS_START_IND                  = NORTH_IND;
    inline static constexpr const Eigen::Index POS_END_IND                    = DOWN_IND;
    inline static constexpr const Eigen::Index QUAT_START_IND                 = QUAT_W_IND;
    inline static constexpr const Eigen::Index QUAT_END_IND                   = QUAT_Z_IND;
    inline static constexpr const Eigen::Index VEL_START_IND                  = NORTH_VEL_IND;
    inline static constexpr const Eigen::Index VEL_END_IND                    = DOWN_VEL_IND;
    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_START_IND = FEATURE_BEARING_ROLL_BIAS_IND;
    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_END_IND   = FEATURE_BEARING_YAW_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_START_IND         = GPS_NORTH_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_END_IND           = GPS_DOWN_BIAS_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_START_IND            = GYRO_BIAS_X_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_END_IND              = GYRO_BIAS_Z_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_START_IND           = ACCEL_BIAS_X_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_END_IND             = ACCEL_BIAS_Z_IND;
  };
  struct NAV
  {
  public:
    inline static constexpr const Eigen::Index NORTH_IND                      = 0;
    inline static constexpr const Eigen::Index EAST_IND                       = 1;
    inline static constexpr const Eigen::Index DOWN_IND                       = 2;
    inline static constexpr const Eigen::Index QUAT_W_IND                     = 3;
    inline static constexpr const Eigen::Index QUAT_X_IND                     = 4;
    inline static constexpr const Eigen::Index QUAT_Y_IND                     = 5;
    inline static constexpr const Eigen::Index QUAT_Z_IND                     = 6;
    inline static constexpr const Eigen::Index NORTH_VEL_IND                  = 7;
    inline static constexpr const Eigen::Index EAST_VEL_IND                   = 8;
    inline static constexpr const Eigen::Index DOWN_VEL_IND                   = 9;
    inline static constexpr const Eigen::Index HEADING_BIAS_IND               = 10;
    inline static constexpr const Eigen::Index ABS_PRESSURE_BIAS_IND          = 11;
    inline static constexpr const Eigen::Index FEATURE_RANGE_BIAS_IND         = 12;
    inline static constexpr const Eigen::Index FEATURE_BEARING_ROLL_BIAS_IND  = 13;
    inline static constexpr const Eigen::Index FEATURE_BEARING_PITCH_BIAS_IND = 14;
    inline static constexpr const Eigen::Index FEATURE_BEARING_YAW_BIAS_IND   = 15;
    inline static constexpr const Eigen::Index GPS_NORTH_BIAS_IND             = 16;
    inline static constexpr const Eigen::Index GPS_EAST_BIAS_IND              = 17;
    inline static constexpr const Eigen::Index GPS_DOWN_BIAS_IND              = 18;
    inline static constexpr const Eigen::Index GYRO_BIAS_X_IND                = 19;
    inline static constexpr const Eigen::Index GYRO_BIAS_Y_IND                = 20;
    inline static constexpr const Eigen::Index GYRO_BIAS_Z_IND                = 21;
    inline static constexpr const Eigen::Index ACCEL_BIAS_X_IND               = 22;
    inline static constexpr const Eigen::Index ACCEL_BIAS_Y_IND               = 23;
    inline static constexpr const Eigen::Index ACCEL_BIAS_Z_IND               = 24;

    inline static constexpr const Eigen::Index POS_START_IND                  = NORTH_IND;
    inline static constexpr const Eigen::Index POS_END_IND                    = DOWN_IND;
    inline static constexpr const Eigen::Index QUAT_START_IND                 = QUAT_W_IND;
    inline static constexpr const Eigen::Index QUAT_END_IND                   = QUAT_Z_IND;
    inline static constexpr const Eigen::Index VEL_START_IND                  = NORTH_VEL_IND;
    inline static constexpr const Eigen::Index VEL_END_IND                    = DOWN_VEL_IND;
    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_START_IND = FEATURE_BEARING_ROLL_BIAS_IND;
    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_END_IND   = FEATURE_BEARING_YAW_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_START_IND         = GPS_NORTH_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_END_IND           = GPS_DOWN_BIAS_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_START_IND            = GYRO_BIAS_X_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_END_IND              = GYRO_BIAS_Z_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_START_IND           = ACCEL_BIAS_X_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_END_IND             = ACCEL_BIAS_Z_IND;
  };
  struct NUM_MEAS
  {
  public:
    inline static constexpr const Eigen::Index GPS_IND             = 0;
    inline static constexpr const Eigen::Index HEADING_IND         = 1;
    inline static constexpr const Eigen::Index ALTITUDE_IND        = 2;
    inline static constexpr const Eigen::Index ABS_PRESSURE_IND    = 3;
    inline static constexpr const Eigen::Index GROUND_VELOCITY_IND = 4;
    inline static constexpr const Eigen::Index FEATURE_RANGE_IND   = 5;
    inline static constexpr const Eigen::Index FEATURE_BEARING_IND = 6;
  };
  struct ERROR
  {
  public:
    inline static constexpr const Eigen::Index NORTH_IND                      = 0;
    inline static constexpr const Eigen::Index EAST_IND                       = 1;
    inline static constexpr const Eigen::Index DOWN_IND                       = 2;
    inline static constexpr const Eigen::Index ROLL_IND                       = 3;
    inline static constexpr const Eigen::Index PITCH_IND                      = 4;
    inline static constexpr const Eigen::Index YAW_IND                        = 5;
    inline static constexpr const Eigen::Index NORTH_VEL_IND                  = 6;
    inline static constexpr const Eigen::Index EAST_VEL_IND                   = 7;
    inline static constexpr const Eigen::Index DOWN_VEL_IND                   = 8;
    inline static constexpr const Eigen::Index HEADING_BIAS_IND               = 9;
    inline static constexpr const Eigen::Index ABS_PRESSURE_BIAS_IND          = 10;
    inline static constexpr const Eigen::Index FEATURE_RANGE_BIAS_IND         = 11;
    inline static constexpr const Eigen::Index FEATURE_BEARING_ROLL_BIAS_IND  = 12;
    inline static constexpr const Eigen::Index FEATURE_BEARING_PITCH_BIAS_IND = 13;
    inline static constexpr const Eigen::Index FEATURE_BEARING_YAW_BIAS_IND   = 14;
    inline static constexpr const Eigen::Index GPS_NORTH_BIAS_IND             = 15;
    inline static constexpr const Eigen::Index GPS_EAST_BIAS_IND              = 16;
    inline static constexpr const Eigen::Index GPS_DOWN_BIAS_IND              = 17;
    inline static constexpr const Eigen::Index GYRO_BIAS_X_IND                = 18;
    inline static constexpr const Eigen::Index GYRO_BIAS_Y_IND                = 19;
    inline static constexpr const Eigen::Index GYRO_BIAS_Z_IND                = 20;
    inline static constexpr const Eigen::Index ACCEL_BIAS_X_IND               = 21;
    inline static constexpr const Eigen::Index ACCEL_BIAS_Y_IND               = 22;
    inline static constexpr const Eigen::Index ACCEL_BIAS_Z_IND               = 23;

    inline static constexpr const Eigen::Index POS_START_IND                  = NORTH_IND;
    inline static constexpr const Eigen::Index POS_END_IND                    = DOWN_IND;
    inline static constexpr const Eigen::Index EULER_START_IND                = ROLL_IND;
    inline static constexpr const Eigen::Index EULER_END_IND                  = YAW_IND;
    inline static constexpr const Eigen::Index VEL_START_IND                  = NORTH_VEL_IND;
    inline static constexpr const Eigen::Index VEL_END_IND                    = DOWN_VEL_IND;
    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_START_IND = FEATURE_BEARING_ROLL_BIAS_IND;
    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_END_IND   = FEATURE_BEARING_YAW_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_START_IND         = GPS_NORTH_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_END_IND           = GPS_DOWN_BIAS_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_START_IND            = GYRO_BIAS_X_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_END_IND              = GYRO_BIAS_Z_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_START_IND           = ACCEL_BIAS_X_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_END_IND             = ACCEL_BIAS_Z_IND;
  };
  struct TRUTH_DISP
  {
  public:
    inline static constexpr const Eigen::Index NORTH_IND                      = 0;
    inline static constexpr const Eigen::Index EAST_IND                       = 1;
    inline static constexpr const Eigen::Index DOWN_IND                       = 2;
    inline static constexpr const Eigen::Index ROLL_IND                       = 3;
    inline static constexpr const Eigen::Index PITCH_IND                      = 4;
    inline static constexpr const Eigen::Index YAW_IND                        = 5;
    inline static constexpr const Eigen::Index NORTH_VEL_IND                  = 6;
    inline static constexpr const Eigen::Index EAST_VEL_IND                   = 7;
    inline static constexpr const Eigen::Index DOWN_VEL_IND                   = 8;
    inline static constexpr const Eigen::Index HEADING_BIAS_IND               = 9;
    inline static constexpr const Eigen::Index ABS_PRESSURE_BIAS_IND          = 10;
    inline static constexpr const Eigen::Index FEATURE_RANGE_BIAS_IND         = 11;
    inline static constexpr const Eigen::Index FEATURE_BEARING_ROLL_BIAS_IND  = 12;
    inline static constexpr const Eigen::Index FEATURE_BEARING_PITCH_BIAS_IND = 13;
    inline static constexpr const Eigen::Index FEATURE_BEARING_YAW_BIAS_IND   = 14;
    inline static constexpr const Eigen::Index GPS_NORTH_BIAS_IND             = 15;
    inline static constexpr const Eigen::Index GPS_EAST_BIAS_IND              = 16;
    inline static constexpr const Eigen::Index GPS_DOWN_BIAS_IND              = 17;
    inline static constexpr const Eigen::Index GYRO_BIAS_X_IND                = 18;
    inline static constexpr const Eigen::Index GYRO_BIAS_Y_IND                = 19;
    inline static constexpr const Eigen::Index GYRO_BIAS_Z_IND                = 20;
    inline static constexpr const Eigen::Index ACCEL_BIAS_X_IND               = 21;
    inline static constexpr const Eigen::Index ACCEL_BIAS_Y_IND               = 22;
    inline static constexpr const Eigen::Index ACCEL_BIAS_Z_IND               = 23;

    inline static constexpr const Eigen::Index POS_START_IND                  = NORTH_IND;
    inline static constexpr const Eigen::Index POS_END_IND                    = DOWN_IND;
    inline static constexpr const Eigen::Index EULER_START_IND                = ROLL_IND;
    inline static constexpr const Eigen::Index EULER_END_IND                  = YAW_IND;
    inline static constexpr const Eigen::Index VEL_START_IND                  = NORTH_VEL_IND;
    inline static constexpr const Eigen::Index VEL_END_IND                    = DOWN_VEL_IND;
    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_START_IND = FEATURE_BEARING_ROLL_BIAS_IND;
    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_END_IND   = FEATURE_BEARING_YAW_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_START_IND         = GPS_NORTH_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_END_IND           = GPS_DOWN_BIAS_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_START_IND            = GYRO_BIAS_X_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_END_IND              = GYRO_BIAS_Z_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_START_IND           = ACCEL_BIAS_X_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_END_IND             = ACCEL_BIAS_Z_IND;
  };
  struct INER_MEAS
  {
  public:
    inline static constexpr const Eigen::Index ROLL_RATE_IND  = 0;
    inline static constexpr const Eigen::Index PITCH_RATE_IND = 1;
    inline static constexpr const Eigen::Index YAW_RATE_IND   = 2;
    inline static constexpr const Eigen::Index X_ACCEL_IND    = 3;
    inline static constexpr const Eigen::Index Y_ACCEL_IND    = 4;
    inline static constexpr const Eigen::Index Z_ACCEL_IND    = 5;

    inline static constexpr const Eigen::Index GYRO_START_IND  = ROLL_RATE_IND;
    inline static constexpr const Eigen::Index GYRO_END_IND    = YAW_RATE_IND;
    inline static constexpr const Eigen::Index ACCEL_START_IND = X_ACCEL_IND;
    inline static constexpr const Eigen::Index ACCEL_END_IND   = Z_ACCEL_IND;
  };
  struct CONTROL
  {
  public:
    inline static constexpr const Eigen::Index ROLL_RATE_IND  = 0;
    inline static constexpr const Eigen::Index PITCH_RATE_IND = 1;
    inline static constexpr const Eigen::Index YAW_RATE_IND   = 2;
    inline static constexpr const Eigen::Index X_ACCEL_IND    = 3;
    inline static constexpr const Eigen::Index Y_ACCEL_IND    = 4;
    inline static constexpr const Eigen::Index Z_ACCEL_IND    = 5;

    inline static constexpr const Eigen::Index GYRO_START_IND  = ROLL_RATE_IND;
    inline static constexpr const Eigen::Index GYRO_END_IND    = YAW_RATE_IND;
    inline static constexpr const Eigen::Index ACCEL_START_IND = X_ACCEL_IND;
    inline static constexpr const Eigen::Index ACCEL_END_IND   = Z_ACCEL_IND;
  };
  struct TRUTH_NOISE
  {
  public:
    inline static constexpr const Eigen::Index HEADING_BIAS_IND               = 0;
    inline static constexpr const Eigen::Index ABS_PRESSURE_BIAS_IND          = 1;
    inline static constexpr const Eigen::Index FEATURE_RANGE_BIAS_IND         = 2;
    inline static constexpr const Eigen::Index FEATURE_BEARING_ROLL_BIAS_IND  = 3;
    inline static constexpr const Eigen::Index FEATURE_BEARING_PITCH_BIAS_IND = 4;
    inline static constexpr const Eigen::Index FEATURE_BEARING_YAW_BIAS_IND   = 5;
    inline static constexpr const Eigen::Index GPS_NORTH_BIAS_IND             = 6;
    inline static constexpr const Eigen::Index GPS_EAST_BIAS_IND              = 7;
    inline static constexpr const Eigen::Index GPS_DOWN_BIAS_IND              = 8;
    inline static constexpr const Eigen::Index ROLL_RATE_BIAS_IND             = 9;
    inline static constexpr const Eigen::Index PITCH_RATE_BIAS_IND            = 10;
    inline static constexpr const Eigen::Index YAW_RATE_BIAS_IND              = 11;
    inline static constexpr const Eigen::Index X_ACCEL_BIAS_IND               = 12;
    inline static constexpr const Eigen::Index Y_ACCEL_BIAS_IND               = 13;
    inline static constexpr const Eigen::Index Z_ACCEL_BIAS_IND               = 14;

    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_START_IND = FEATURE_BEARING_ROLL_BIAS_IND;
    inline static constexpr const Eigen::Index FEATURE_BEARING_BIAS_END_IND   = FEATURE_BEARING_YAW_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_START_IND         = GPS_NORTH_BIAS_IND;
    inline static constexpr const Eigen::Index GPS_POS_BIAS_END_IND           = GPS_DOWN_BIAS_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_START_IND            = ROLL_RATE_BIAS_IND;
    inline static constexpr const Eigen::Index GYRO_BIAS_END_IND              = YAW_RATE_BIAS_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_START_IND           = X_ACCEL_BIAS_IND;
    inline static constexpr const Eigen::Index ACCEL_BIAS_END_IND             = Z_ACCEL_BIAS_IND;
  };
};

/**
 * @SCALAR
 * The object type that each dimension will be represented with.
 *
 * @OPTIONS
 * Eigen Matrix options.
 **/
template<typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
class BasicModel
 : public DynamicsBase<BasicModelDim,SCALAR,OPTIONS>
{
public:
  /**
   * @Default Constructor
   **/
  BasicModel() = delete;
  /**
   * @Copy Constructor
   **/
  BasicModel(const BasicModel&) = default;
  /**
   * @Move Constructor
   **/
  BasicModel(BasicModel&&) = default;
  /**
   * @Constructor
   *
   * @brief
   * Initializes the class for use.
   *
   * @templates
   * DERIVED: The matrix type that the imu noise covariance
   *
   * @parameters
   * heading_bias_noise: The noise that is added to the time derivative of the heading bias
   * abs_pressure_bias_noise: The noise that is added to the time derivative of the abs pressure bias
   * feature_range_bias_noise: The noise that is added to the time derivative of the feature range bias
   * feature_bearing_bias_noise: The noise that is added to the time derivative of the feature bearing bias
   * gps_position_bias_noise: The noise that is added to the time derivative of the GPS position reading's bias
   * accel_bias_noise: The noise that is added to the time derivative of the accelerometer bias
   * gyro_bias_noise: The noise that is added to the time derivative of the gyroscope bias
   * nominal_velocity: The nominal velocity of the vehicle
   * gravity_accel: The magnitude of the acceleration from gravity
   * heading_time_constant: The first order Gauss Markov time constant for the heading bias
   * abs_pressure_time_constant: The first order Gauss Markov time constant for the abs pressure bias
   * feature_range_time_constant: The first order Gauss Markov time constant for the feature range bias
   * feature_bearing_time_constant: The first order Gauss Markov time constant for the feature bearing bias
   * gps_position_time_constant: The first order Gauss Markov time constant for the GPS position reading's bias
   * accel_time_constant: The first order Gauss Markov time constant for the accelerometer bias
   * gyro_time_constant: The first order Gauss Markov time constant for the gyroscope bias
   **/
  BasicModel(const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& heading_bias_noise,
             const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& abs_pressure_bias_noise,
             const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& feature_range_bias_noise,
             const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& feature_bearing_bias_noise,
             const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& gps_position_bias_noise,
             const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& accel_bias_noise,
             const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& gyro_bias_noise,
             const SCALAR                                 nominal_velocity,
             const SCALAR                                 gravity_accel,
             const SCALAR                                 heading_time_constant,
             const SCALAR                                 abs_pressure_time_constant,
             const SCALAR                                 feature_range_time_constant,
             const SCALAR                                 feature_bearing_time_constant,
             const SCALAR                                 gps_position_time_constant,
             const SCALAR                                 accel_time_constant,
             const SCALAR                                 gyro_time_constant);
  /**
   * @Deconstructor
   **/
  ~BasicModel() override = default;
  /**
   * @Assignment Operators
   **/
  BasicModel& operator=(const BasicModel&)  = default;
  BasicModel& operator=(      BasicModel&&) = default;
  /**
   * @getTruthStateTimeDerivative
   *
   * @brief
   * Finds the time derivative of the truth state with additive process noise.
   *
   * @parameters
   * time: The current simulation time in terms of the number of simulation time steps have passed since the start time
   * truth_state: The current truth state vector
   * control_input: The current control vector
   * process_noise: The process noise on the truth state dynamics
   *
   * @return
   * The time derivative of the truth state vector.
   **/
  inline Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_DIM,OPTIONS>
    getTruthStateTimeDerivative(const SCALAR                                                                            time,
                                const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_DIM,      OPTIONS>>& truth_state,
                                const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::CONTROL_DIM,    OPTIONS>>& control_input,
                                const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS>>& process_noise) override;
  /**
   * @getNavStateTimeDerivative
   *
   * @brief
   * Finds the time derivative of the navigation state.
   *
   * @parameters
   * time: The current simulation time in terms of the number of simulation time steps have passed since the start time
   * nav_state: The current navigation state vector
   * control_input: The current control vector
   * inertial_reading: The inertial measurements with biases and noise
   *
   * @return
   * The time derivative of the navigation state vector.
   **/
  inline Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,OPTIONS>
    getNavStateTimeDerivative(const SCALAR                                                                          time,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,      OPTIONS>>& nav_state,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::CONTROL_DIM,  OPTIONS>>& control_input,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& inertial_reading) override;
  /**
   * @getLinearizedTruthStateDynamicsMatrix
   *
   * @brief
   * Finds the derivative of the truth state dynamics with respect to the truth state and then evaluated
   * along the nominal reference trajectory.
   *
   * @parameters
   * ref_truth_state: The current state from the reference trajectory mapped into a truth state vector
   * ref_inertial_reading: The inertial measurements without biases and noise
   *
   * @return
   * The derivative of the truth state dynamics with respect to the truth state.
   **/
  inline Eigen::Matrix<SCALAR,BasicModelDim::TRUTH_DISP_DIM,BasicModelDim::TRUTH_DISP_DIM,OPTIONS>
    getLinearizedTruthStateDynamicsMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_DIM,    OPTIONS>>& ref_truth_state,
                                          const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& ref_inertial_reading) override;
  /**
   * @getLinearizedTruthStateProcessNoiseMatrix
   *
   * @brief
   * Finds the derivative of the true state dynamics with respect to the process noise vector and then
   * evaluated along the nominal reference trajectory.
   *
   * @parameters
   * ref_truth_state: The current state from the reference trajectory mapped into a truth state vector
   *
   * @return
   * The derivative of the true state dynamics with respect to the process noise vector.
   **/
  inline Eigen::Matrix<SCALAR,BasicModelDim::TRUTH_DISP_DIM,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS>
    getLinearizedTruthStateProcessNoiseMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_DIM,OPTIONS>>& ref_truth_state) override;
  /**
   * @getLinearizedNavStateDynamicsMatrix
   *
   * @brief
   * Finds the derivative of the navigation state dynamics with respect to the navigation state and then
   * evaluated along the nominal reference trajectory.
   *
   * @parameters
   * ref_nav_state: The current state from the reference trajectory mapped into a navigation state vector
   * ref_inertial_reading: The inertial measurements without biases and noise
   *
   * @return
   * The derivative of the navigation state dynamics with respect to the navigation state.
   **/
  inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS>
    getLinearizedNavStateDynamicsMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,      OPTIONS>>& ref_nav_state,
                                        const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& ref_inertial_reading) override;
  /**
   * @getLinearizedNavStateInertialMeasurementMatrix
   *
   * @brief
   * Finds the derivative of the navigation state dynamics with respect to the inertial measurement vector and then
   * evaluated along the nominal reference trajectory.
   *
   * @parameters
   * ref_nav_state: The current state from the reference trajectory mapped into a navigation state vector
   *
   * @return
   * The derivative of the navigation state dynamics with respect to the inertial measurement vector.
   **/
  inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::INER_MEAS_DIM,OPTIONS>
    getLinearizedNavStateInertialMeasurementMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,OPTIONS>>& ref_nav_state) override;
  /**
   * @getLinearizedErrorStateDynamicsMatrix
   *
   * @brief
   * The matrix that you get from linearizing the error state dynamics about the .
   *
   * @parameters
   * time: The current simulation time in terms of the number of simulation time steps have passed since the start time
   * nav_state: The current navigation state vector
   * inertial_reading: The inertial measurements with biases and noise
   *
   * @return
   * The matrix that you get from linearizing the error state dynamics about the navigation state.
   **/
  inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS>
    getLinearizedErrorStateDynamicsMatrix(const SCALAR                                                                          time,
                                          const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,      OPTIONS>>& nav_state,
                                          const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& inertial_reading) override;
  /**
   * @getErrorStateNoiseCovariance
   *
   * @brief
   * Gets the covariance matrix of the error state dynamics.
   *
   * @parameters
   * nav_state: The current navigation state vector
   * inertial_noise_covariance: The covariance of the noise from the inertial measurements
   *
   * @return
   * The covariance matrix of the additive noise for the error state.
   **/
  inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS>
    getErrorStateNoiseCovariance(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,                           BasicModelDim::NAV_DIM,      OPTIONS>>& nav_state,
                                 const Eigen::Ref<const Eigen::Matrix<SCALAR,BasicModelDim::INER_MEAS_DIM,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& inertial_noise_covariance) override;
  /**
   * @findTimeStep
   *
   * @brief
   * Used to calculate the time that is spanned between two state vectors along the simulation vector.
   *
   * @parameters
   * prev_ref_state: The reference state of the previous time step
   * cur_ref_state: The reference state of the current time step
   *
   * @return
   * The time that is spanned between prev_ref_state and cur_ref_state in seconds.
   **/
  inline SCALAR findTimeStep(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::REF_DIM,OPTIONS>>& prev_ref_state,
                             const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::REF_DIM,OPTIONS>>& cur_ref_state) override;
  /**
   * @getProcessNoise
   *
   * @brief
   * Gets the process noise on the truth state dynamics other then the inertial measurement noise.
   *
   * @parameters
   * time_step: The time step between the last state vector and the current one
   *
   * @return
   * The process noise.
   **/
  inline Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS> getProcessNoise(const SCALAR time_step) override;
  /**
   * @getTruthStateNoiseCovariance
   *
   * @brief
   * Gets the covariance matrix of the additive process noise.
   *
   * @parameters
   * inertial_noise_covariance: The covariance of the noise from the inertial measurements
   *
   * @return
   * The covariance matrix of the additive noise for the truth state.
   **/
  inline Eigen::Matrix<SCALAR,BasicModelDim::TRUTH_NOISE_DIM,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS>
    getTruthStateNoiseCovariance() override;
private:
  noise::NoiseBasePtr<1,SCALAR,OPTIONS> heading_bias_noise;
  noise::NoiseBasePtr<1,SCALAR,OPTIONS> abs_pressure_bias_noise;
  noise::NoiseBasePtr<1,SCALAR,OPTIONS> feature_range_bias_noise;
  noise::NoiseBasePtr<3,SCALAR,OPTIONS> feature_bearing_bias_noise;
  noise::NoiseBasePtr<3,SCALAR,OPTIONS> gps_position_bias_noise;
  noise::NoiseBasePtr<3,SCALAR,OPTIONS> accel_bias_noise;
  noise::NoiseBasePtr<3,SCALAR,OPTIONS> gyro_bias_noise;
  SCALAR                                nominal_velocity;
  SCALAR                                gravity_accel;
  SCALAR                                heading_time_constant;
  SCALAR                                abs_pressure_time_constant;
  SCALAR                                feature_range_time_constant;
  SCALAR                                feature_bearing_time_constant;
  SCALAR                                gps_position_time_constant;
  SCALAR                                accel_time_constant;
  SCALAR                                gyro_time_constant;
};

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
BasicModel<SCALAR,OPTIONS>::BasicModel(const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& heading_bias_noise,
                                       const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& abs_pressure_bias_noise,
                                       const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& feature_range_bias_noise,
                                       const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& feature_bearing_bias_noise,
                                       const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& gps_position_bias_noise,
                                       const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& accel_bias_noise,
                                       const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& gyro_bias_noise,
                                       const SCALAR                                 nominal_velocity,
                                       const SCALAR                                 gravity_accel,
                                       const SCALAR                                 heading_time_constant,
                                       const SCALAR                                 abs_pressure_time_constant,
                                       const SCALAR                                 feature_range_time_constant,
                                       const SCALAR                                 feature_bearing_time_constant,
                                       const SCALAR                                 gps_position_time_constant,
                                       const SCALAR                                 accel_time_constant,
                                       const SCALAR                                 gyro_time_constant)
 : DynamicsBase<BasicModelDim,SCALAR,OPTIONS>(),
   heading_bias_noise(heading_bias_noise),
   abs_pressure_bias_noise(abs_pressure_bias_noise),
   feature_range_bias_noise(feature_range_bias_noise),
   feature_bearing_bias_noise(feature_bearing_bias_noise),
   gps_position_bias_noise(gps_position_bias_noise),
   accel_bias_noise(accel_bias_noise),
   gyro_bias_noise(gyro_bias_noise),
   nominal_velocity(nominal_velocity),
   gravity_accel(gravity_accel),
   heading_time_constant(heading_time_constant),
   abs_pressure_time_constant(abs_pressure_time_constant),
   feature_range_time_constant(feature_range_time_constant),
   feature_bearing_time_constant(feature_bearing_time_constant),
   gps_position_time_constant(gps_position_time_constant),
   accel_time_constant(accel_time_constant),
   gyro_time_constant(gyro_time_constant)
{}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_DIM,OPTIONS> BasicModel<SCALAR,OPTIONS>::
  getTruthStateTimeDerivative(const SCALAR                                                                            /* time */,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_DIM,      OPTIONS>>& truth_state,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::CONTROL_DIM,    OPTIONS>>& control_input,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS>>& process_noise)
{
  Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_DIM,OPTIONS> output;

  // Make helper information
  Eigen::Matrix<SCALAR,1,3,OPTIONS> gravity_vec;
  Eigen::Matrix<SCALAR,3,3,OPTIONS> body_to_ned_rotation;
  Eigen::Matrix<SCALAR,1,4,OPTIONS> angular_rotation_quat;

  gravity_vec[0] = 0;
  gravity_vec[1] = 0;
  gravity_vec[2] = this->gravity_accel;
  body_to_ned_rotation = math::quat::quaternionToDirectionCosineMatrix(truth_state.template middleCols<4>(BasicModelDim::TRUTH::QUAT_START_IND));
  angular_rotation_quat[0] = 0;
  angular_rotation_quat.template rightCols<3>() = control_input.template middleCols<3>(BasicModelDim::CONTROL::GYRO_START_IND);

  output.template middleCols<3>(BasicModelDim::TRUTH::POS_START_IND) =
    truth_state.template middleCols<3>(BasicModelDim::TRUTH::VEL_START_IND);
  output.template middleCols<4>(BasicModelDim::TRUTH::QUAT_START_IND) =
    (SCALAR(1)/SCALAR(2)) * math::quat::product(truth_state.template middleCols<4>(BasicModelDim::TRUTH::QUAT_START_IND),
                                                angular_rotation_quat).array();
  output.template middleCols<3>(BasicModelDim::TRUTH::VEL_START_IND) =
    (control_input.template middleCols<3>(BasicModelDim::CONTROL::ACCEL_START_IND) * body_to_ned_rotation.transpose()) + gravity_vec;
  output[BasicModelDim::TRUTH::HEADING_BIAS_IND] =
    ((SCALAR(-1)/this->heading_time_constant) * truth_state[BasicModelDim::TRUTH::HEADING_BIAS_IND]) +
      process_noise[BasicModelDim::TRUTH_NOISE::HEADING_BIAS_IND];
  output[BasicModelDim::TRUTH::ABS_PRESSURE_BIAS_IND] =
    ((SCALAR(-1)/this->abs_pressure_time_constant) * truth_state[BasicModelDim::TRUTH::ABS_PRESSURE_BIAS_IND]) +
      process_noise[BasicModelDim::TRUTH_NOISE::ABS_PRESSURE_BIAS_IND];
  output[BasicModelDim::TRUTH::FEATURE_RANGE_BIAS_IND] =
    ((SCALAR(-1)/this->feature_range_time_constant) * truth_state[BasicModelDim::TRUTH::FEATURE_RANGE_BIAS_IND]) +
      process_noise[BasicModelDim::TRUTH_NOISE::FEATURE_RANGE_BIAS_IND];
  output.template middleCols<3>(BasicModelDim::TRUTH::FEATURE_BEARING_BIAS_START_IND) =
    ((SCALAR(-1)/this->feature_bearing_time_constant) * truth_state.template middleCols<3>(BasicModelDim::TRUTH::FEATURE_BEARING_BIAS_START_IND).array()).matrix() +
      process_noise.template middleCols<3>(BasicModelDim::TRUTH_NOISE::FEATURE_BEARING_BIAS_START_IND);
  output.template middleCols<3>(BasicModelDim::TRUTH::GPS_POS_BIAS_START_IND) =
    ((SCALAR(-1)/this->gps_position_time_constant) * truth_state.template middleCols<3>(BasicModelDim::TRUTH::GPS_POS_BIAS_START_IND).array()).matrix() +
      process_noise.template middleCols<3>(BasicModelDim::TRUTH_NOISE::GPS_POS_BIAS_START_IND);
  output.template middleCols<3>(BasicModelDim::TRUTH::GYRO_BIAS_START_IND) =
    ((SCALAR(-1)/this->gyro_time_constant) * truth_state.template middleCols<3>(BasicModelDim::TRUTH::GYRO_BIAS_START_IND).array()).matrix() +
      process_noise.template middleCols<3>(BasicModelDim::TRUTH_NOISE::GYRO_BIAS_START_IND);
  output.template middleCols<3>(BasicModelDim::TRUTH::ACCEL_BIAS_START_IND) =
    ((SCALAR(-1)/this->accel_time_constant) * truth_state.template middleCols<3>(BasicModelDim::TRUTH::ACCEL_BIAS_START_IND).array()).matrix() +
      process_noise.template middleCols<3>(BasicModelDim::TRUTH_NOISE::ACCEL_BIAS_START_IND);

  return output;
}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,OPTIONS> BasicModel<SCALAR,OPTIONS>::
  getNavStateTimeDerivative(const SCALAR                                                                          /* time */,
                            const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,      OPTIONS>>& nav_state,
                            const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::CONTROL_DIM,  OPTIONS>>& /* control_input */,
                            const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& inertial_reading)
{
  Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,OPTIONS> output;

  // Make helper information
  Eigen::Matrix<SCALAR,1,3,OPTIONS> gravity_vec;
  Eigen::Matrix<SCALAR,3,3,OPTIONS> body_to_ned_rotation;
  Eigen::Matrix<SCALAR,1,4,OPTIONS> angular_rotation_quat;

  gravity_vec[0] = 0;
  gravity_vec[1] = 0;
  gravity_vec[2] = this->gravity_accel;
  body_to_ned_rotation = math::quat::quaternionToDirectionCosineMatrix(nav_state.template middleCols<4>(BasicModelDim::NAV::QUAT_START_IND));
  angular_rotation_quat[0] = 0;
  angular_rotation_quat.template rightCols<3>() = inertial_reading.template middleCols<3>(BasicModelDim::INER_MEAS::GYRO_START_IND) -
                                                  nav_state.       template middleCols<3>(BasicModelDim::NAV::GYRO_BIAS_START_IND);

  output.template middleCols<3>(BasicModelDim::NAV::POS_START_IND) =
    nav_state.template middleCols<3>(BasicModelDim::NAV::VEL_START_IND);
  output.template middleCols<4>(BasicModelDim::NAV::QUAT_START_IND) =
    (SCALAR(1)/SCALAR(2)) * math::quat::product(nav_state.template middleCols<4>(BasicModelDim::NAV::QUAT_START_IND),
                                                angular_rotation_quat).array();
  output.template middleCols<3>(BasicModelDim::NAV::VEL_START_IND) =
    ((inertial_reading.template middleCols<3>(BasicModelDim::INER_MEAS::ACCEL_START_IND) -
      nav_state.       template middleCols<3>(BasicModelDim::NAV::ACCEL_BIAS_START_IND)) * body_to_ned_rotation.transpose()) + gravity_vec;
  output[BasicModelDim::NAV::HEADING_BIAS_IND] = (SCALAR(-1)/this->heading_time_constant) * nav_state[BasicModelDim::NAV::HEADING_BIAS_IND];
  output[BasicModelDim::NAV::ABS_PRESSURE_BIAS_IND] = (SCALAR(-1)/this->abs_pressure_time_constant) * nav_state[BasicModelDim::NAV::ABS_PRESSURE_BIAS_IND];
  output[BasicModelDim::NAV::FEATURE_RANGE_BIAS_IND] = (SCALAR(-1)/this->feature_range_time_constant) * nav_state[BasicModelDim::NAV::FEATURE_RANGE_BIAS_IND];
  output.template middleCols<3>(BasicModelDim::NAV::FEATURE_BEARING_BIAS_START_IND) =
    (SCALAR(-1)/this->feature_bearing_time_constant) * nav_state.template middleCols<3>(BasicModelDim::NAV::FEATURE_BEARING_BIAS_START_IND).array();
  output.template middleCols<3>(BasicModelDim::NAV::GPS_POS_BIAS_START_IND) =
    (SCALAR(-1)/this->gps_position_time_constant) * nav_state.template middleCols<3>(BasicModelDim::NAV::GPS_POS_BIAS_START_IND).array();
  output.template middleCols<3>(BasicModelDim::NAV::GYRO_BIAS_START_IND) =
    (SCALAR(-1)/this->gyro_time_constant) * nav_state.template middleCols<3>(BasicModelDim::NAV::GYRO_BIAS_START_IND).array();
  output.template middleCols<3>(BasicModelDim::NAV::ACCEL_BIAS_START_IND) =
    (SCALAR(-1)/this->accel_time_constant) * nav_state.template middleCols<3>(BasicModelDim::NAV::ACCEL_BIAS_START_IND).array();

  return output;

}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS> BasicModel<SCALAR,OPTIONS>::
  getLinearizedTruthStateDynamicsMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_DIM,    OPTIONS>>& ref_truth_state,
                                        const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& ref_inertial_reading)
{
  Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS> output;

  // Helper variables
  Eigen::Matrix<SCALAR,3,3,OPTIONS> body_to_ned_rotation =
    math::quat::quaternionToDirectionCosineMatrix(ref_truth_state.template middleCols<4>(BasicModelDim::TRUTH::QUAT_START_IND));

  output.setZero();
  // Position terms
  output.template block<3,3>(BasicModelDim::ERROR::POS_START_IND, BasicModelDim::ERROR::VEL_START_IND) =
    Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity();
  // Velocity terms
  output.template block<3,3>(BasicModelDim::ERROR::VEL_START_IND, BasicModelDim::ERROR::EULER_START_IND) =
    math::crossProductMatrix(body_to_ned_rotation * ref_inertial_reading.template middleCols<3>(BasicModelDim::INER_MEAS::ACCEL_START_IND).transpose());
  // Other bias terms
  output(BasicModelDim::ERROR::HEADING_BIAS_IND,      BasicModelDim::ERROR::HEADING_BIAS_IND)       = SCALAR(-1)/this->heading_time_constant;
  output(BasicModelDim::ERROR::ABS_PRESSURE_BIAS_IND, BasicModelDim::ERROR::ABS_PRESSURE_BIAS_IND)  = SCALAR(-1)/this->abs_pressure_time_constant;
  output(BasicModelDim::ERROR::FEATURE_RANGE_BIAS_IND,BasicModelDim::ERROR::FEATURE_RANGE_BIAS_IND) = SCALAR(-1)/this->feature_range_time_constant;
  output.template block<3,3>(BasicModelDim::ERROR::FEATURE_BEARING_BIAS_START_IND, BasicModelDim::ERROR::FEATURE_BEARING_BIAS_START_IND) =
    (SCALAR(-1)/this->feature_bearing_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();
  output.template block<3,3>(BasicModelDim::ERROR::GPS_POS_BIAS_START_IND, BasicModelDim::ERROR::GPS_POS_BIAS_START_IND) =
    (SCALAR(-1)/this->gps_position_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();
  // Gyroscope bias terms
  output.template block<3,3>(BasicModelDim::ERROR::GYRO_BIAS_START_IND, BasicModelDim::ERROR::GYRO_BIAS_START_IND) =
    (SCALAR(-1)/this->gyro_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();
  // Accelerometer bias terms
  output.template block<3,3>(BasicModelDim::ERROR::ACCEL_BIAS_START_IND, BasicModelDim::ERROR::ACCEL_BIAS_START_IND) =
    (SCALAR(-1)/this->accel_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();

  return output;
}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS> BasicModel<SCALAR,OPTIONS>::
  getLinearizedTruthStateProcessNoiseMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_DIM,OPTIONS>>& /* ref_truth_state */)
{
  Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS> output;

  output.setZero();
  output(BasicModelDim::ERROR::HEADING_BIAS_IND,      BasicModelDim::TRUTH_NOISE::HEADING_BIAS_IND)       = 1;
  output(BasicModelDim::ERROR::ABS_PRESSURE_BIAS_IND, BasicModelDim::TRUTH_NOISE::ABS_PRESSURE_BIAS_IND)  = 1;
  output(BasicModelDim::ERROR::FEATURE_RANGE_BIAS_IND,BasicModelDim::TRUTH_NOISE::FEATURE_RANGE_BIAS_IND) = 1;
  output.template block<3,3>(BasicModelDim::ERROR::FEATURE_BEARING_BIAS_START_IND,BasicModelDim::TRUTH_NOISE::FEATURE_BEARING_BIAS_START_IND).setIdentity();
  output.template block<3,3>(BasicModelDim::ERROR::GPS_POS_BIAS_START_IND,        BasicModelDim::TRUTH_NOISE::GPS_POS_BIAS_START_IND).        setIdentity();
  output.template block<3,3>(BasicModelDim::ERROR::GYRO_BIAS_START_IND,           BasicModelDim::TRUTH_NOISE::GYRO_BIAS_START_IND).           setIdentity();
  output.template block<3,3>(BasicModelDim::ERROR::ACCEL_BIAS_START_IND,          BasicModelDim::TRUTH_NOISE::ACCEL_BIAS_START_IND).          setIdentity();

  return output;
}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS> BasicModel<SCALAR,OPTIONS>::
  getLinearizedNavStateDynamicsMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,      OPTIONS>>& ref_nav_state,
                                      const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& ref_inertial_reading)
{
  Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS> output;

  // Helper variables
  Eigen::Matrix<SCALAR,3,3,OPTIONS> body_to_ned_rotation =
    math::quat::quaternionToDirectionCosineMatrix(ref_nav_state.template middleCols<4>(BasicModelDim::NAV::QUAT_START_IND));

  output.setZero();
  // Position terms
  output.template block<3,3>(BasicModelDim::ERROR::POS_START_IND, BasicModelDim::ERROR::VEL_START_IND) =
    Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity();
  // Euler angles terms
  output.template block<3,3>(BasicModelDim::ERROR::EULER_START_IND, BasicModelDim::ERROR::GYRO_BIAS_START_IND) =
    body_to_ned_rotation;
  // Velocity terms
  output.template block<3,3>(BasicModelDim::ERROR::VEL_START_IND, BasicModelDim::ERROR::EULER_START_IND) =
    math::crossProductMatrix(body_to_ned_rotation * (ref_inertial_reading.template middleCols<3>(BasicModelDim::INER_MEAS::ACCEL_START_IND) -
                                                     ref_nav_state.       template middleCols<3>(BasicModelDim::NAV::ACCEL_BIAS_START_IND)).transpose());
  output.template block<3,3>(BasicModelDim::ERROR::VEL_START_IND, BasicModelDim::ERROR::ACCEL_BIAS_START_IND) =
    -body_to_ned_rotation;
  // Other bias terms
  output(BasicModelDim::ERROR::HEADING_BIAS_IND,      BasicModelDim::ERROR::HEADING_BIAS_IND)       = SCALAR(-1)/this->heading_time_constant;
  output(BasicModelDim::ERROR::ABS_PRESSURE_BIAS_IND, BasicModelDim::ERROR::ABS_PRESSURE_BIAS_IND)  = SCALAR(-1)/this->abs_pressure_time_constant;
  output(BasicModelDim::ERROR::FEATURE_RANGE_BIAS_IND,BasicModelDim::ERROR::FEATURE_RANGE_BIAS_IND) = SCALAR(-1)/this->feature_range_time_constant;
  output.template block<3,3>(BasicModelDim::ERROR::FEATURE_BEARING_BIAS_START_IND, BasicModelDim::ERROR::FEATURE_BEARING_BIAS_START_IND) =
    (SCALAR(-1)/this->feature_bearing_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();
  output.template block<3,3>(BasicModelDim::ERROR::GPS_POS_BIAS_START_IND, BasicModelDim::ERROR::GPS_POS_BIAS_START_IND) =
    (SCALAR(-1)/this->gps_position_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();
  // Gyroscope bias terms
  output.template block<3,3>(BasicModelDim::ERROR::GYRO_BIAS_START_IND, BasicModelDim::ERROR::GYRO_BIAS_START_IND) =
    (SCALAR(-1)/this->gyro_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();
  // Accelerometer bias terms
  output.template block<3,3>(BasicModelDim::ERROR::ACCEL_BIAS_START_IND, BasicModelDim::ERROR::ACCEL_BIAS_START_IND) =
    (SCALAR(-1)/this->accel_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();

  return output;
}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::INER_MEAS_DIM,OPTIONS> BasicModel<SCALAR,OPTIONS>::
  getLinearizedNavStateInertialMeasurementMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,OPTIONS>>& ref_nav_state)
{
  Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::INER_MEAS_DIM,OPTIONS> output;

  // Helper variables
  Eigen::Matrix<SCALAR,3,3,OPTIONS> body_to_ned_rotation =
    math::quat::quaternionToDirectionCosineMatrix(ref_nav_state.template middleCols<4>(BasicModelDim::NAV::QUAT_START_IND));

  output.setZero();
  output.template block<3,3>(BasicModelDim::ERROR::VEL_START_IND,  BasicModelDim::INER_MEAS::ACCEL_START_IND) =  body_to_ned_rotation;
  output.template block<3,3>(BasicModelDim::ERROR::EULER_START_IND,BasicModelDim::INER_MEAS::GYRO_START_IND)  = -body_to_ned_rotation;

  return output;
}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS> BasicModel<SCALAR,OPTIONS>::
  getLinearizedErrorStateDynamicsMatrix(const SCALAR                                                                          /* time */,
                                        const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::NAV_DIM,      OPTIONS>>& nav_state,
                                        const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& inertial_reading)
{
  Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS> output;

  // Helper variables
  Eigen::Matrix<SCALAR,3,3,OPTIONS> body_to_ned_rotation =
    math::quat::quaternionToDirectionCosineMatrix(nav_state.template middleCols<4>(BasicModelDim::NAV::QUAT_START_IND));

  output.setZero();
  // Position terms
  output.template block<3,3>(BasicModelDim::ERROR::POS_START_IND, BasicModelDim::ERROR::VEL_START_IND) =
    Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity();
  // Euler angles terms
  output.template block<3,3>(BasicModelDim::ERROR::EULER_START_IND, BasicModelDim::ERROR::GYRO_BIAS_START_IND) =
    body_to_ned_rotation;
  // Velocity terms
  output.template block<3,3>(BasicModelDim::ERROR::VEL_START_IND, BasicModelDim::ERROR::EULER_START_IND) =
    math::crossProductMatrix(body_to_ned_rotation * (inertial_reading.template middleCols<3>(BasicModelDim::INER_MEAS::ACCEL_START_IND) -
                                                     nav_state.       template middleCols<3>(BasicModelDim::NAV::ACCEL_BIAS_START_IND)).transpose());
  output.template block<3,3>(BasicModelDim::ERROR::VEL_START_IND, BasicModelDim::ERROR::ACCEL_BIAS_START_IND) =
    -body_to_ned_rotation;
  // Other bias terms
  output(BasicModelDim::ERROR::HEADING_BIAS_IND,      BasicModelDim::ERROR::HEADING_BIAS_IND)       = SCALAR(-1)/this->heading_time_constant;
  output(BasicModelDim::ERROR::ABS_PRESSURE_BIAS_IND, BasicModelDim::ERROR::ABS_PRESSURE_BIAS_IND)  = SCALAR(-1)/this->abs_pressure_time_constant;
  output(BasicModelDim::ERROR::FEATURE_RANGE_BIAS_IND,BasicModelDim::ERROR::FEATURE_RANGE_BIAS_IND) = SCALAR(-1)/this->feature_range_time_constant;
  output.template block<3,3>(BasicModelDim::ERROR::FEATURE_BEARING_BIAS_START_IND, BasicModelDim::ERROR::FEATURE_BEARING_BIAS_START_IND) =
    (SCALAR(-1)/this->feature_bearing_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();
  output.template block<3,3>(BasicModelDim::ERROR::GPS_POS_BIAS_START_IND, BasicModelDim::ERROR::GPS_POS_BIAS_START_IND) =
    (SCALAR(-1)/this->gps_position_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();
  // Gyroscope bias terms
  output.template block<3,3>(BasicModelDim::ERROR::GYRO_BIAS_START_IND, BasicModelDim::ERROR::GYRO_BIAS_START_IND) =
    (SCALAR(-1)/this->gyro_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();
  // Accelerometer bias terms
  output.template block<3,3>(BasicModelDim::ERROR::ACCEL_BIAS_START_IND, BasicModelDim::ERROR::ACCEL_BIAS_START_IND) =
    (SCALAR(-1)/this->accel_time_constant) * Eigen::Matrix<SCALAR,3,3,OPTIONS>::Identity().array();

  return output;
}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,BasicModelDim::ERROR_DIM,OPTIONS> BasicModel<SCALAR,OPTIONS>::
  getErrorStateNoiseCovariance(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,                           BasicModelDim::NAV_DIM,      OPTIONS>>& nav_state,
                               const Eigen::Ref<const Eigen::Matrix<SCALAR,BasicModelDim::INER_MEAS_DIM,BasicModelDim::INER_MEAS_DIM,OPTIONS>>& inertial_noise_covariance)
{
  Eigen::Matrix<SCALAR,BasicModelDim::ERROR_DIM,
                       BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE_DIM,OPTIONS> noise_mixer;
  Eigen::Matrix<SCALAR,BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE_DIM,
                       BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE_DIM,OPTIONS> truth_noise_cov;

  // Helper variables
  const Eigen::Matrix<SCALAR,3,3,OPTIONS> body_to_ned_rotation
    = math::quat::quaternionToDirectionCosineMatrix(nav_state.template middleCols<4>(BasicModelDim::NAV::QUAT_START_IND));

  noise_mixer.setZero();
  noise_mixer.template block<3,3>(BasicModelDim::ERROR::VEL_START_IND,  BasicModelDim::INER_MEAS::ACCEL_START_IND) = -body_to_ned_rotation;
  noise_mixer.template block<3,3>(BasicModelDim::ERROR::EULER_START_IND,BasicModelDim::INER_MEAS::GYRO_START_IND)  =  body_to_ned_rotation;
  noise_mixer(BasicModelDim::ERROR::HEADING_BIAS_IND,      BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE::HEADING_BIAS_IND)       = 1;
  noise_mixer(BasicModelDim::ERROR::ABS_PRESSURE_BIAS_IND, BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE::ABS_PRESSURE_BIAS_IND)  = 1;
  noise_mixer(BasicModelDim::ERROR::FEATURE_RANGE_BIAS_IND,BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE::FEATURE_RANGE_BIAS_IND) = 1;
  noise_mixer.template block<3,3>(BasicModelDim::ERROR::FEATURE_BEARING_BIAS_START_IND,BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE::FEATURE_BEARING_BIAS_START_IND).setIdentity();
  noise_mixer.template block<3,3>(BasicModelDim::ERROR::GPS_POS_BIAS_START_IND,        BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE::GPS_POS_BIAS_START_IND).        setIdentity();
  noise_mixer.template block<3,3>(BasicModelDim::ERROR::GYRO_BIAS_START_IND,           BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE::GYRO_BIAS_START_IND).           setIdentity();
  noise_mixer.template block<3,3>(BasicModelDim::ERROR::ACCEL_BIAS_START_IND,          BasicModelDim::INER_MEAS_DIM+BasicModelDim::TRUTH_NOISE::ACCEL_BIAS_START_IND).          setIdentity();

  truth_noise_cov.setZero();
  truth_noise_cov.template block<BasicModelDim::INER_MEAS_DIM,BasicModelDim::INER_MEAS_DIM>(0,0) = inertial_noise_covariance;
  truth_noise_cov.template block<BasicModelDim::TRUTH_NOISE_DIM,
                                 BasicModelDim::TRUTH_NOISE_DIM>(BasicModelDim::INER_MEAS_DIM,
                                                                 BasicModelDim::INER_MEAS_DIM) = this->getTruthStateNoiseCovariance();

  return noise_mixer * truth_noise_cov * noise_mixer.transpose();
}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline SCALAR BasicModel<SCALAR,OPTIONS>::
  findTimeStep(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::REF_DIM,OPTIONS>>& prev_ref_state,
               const Eigen::Ref<const Eigen::Matrix<SCALAR,1,BasicModelDim::REF_DIM,OPTIONS>>& cur_ref_state)
{
  const SCALAR dist_traveled = (prev_ref_state.template middleCols<3>(BasicModelDim::REF::POS_START_IND) -
                                cur_ref_state. template middleCols<3>(BasicModelDim::REF::POS_START_IND)).norm();
  return dist_traveled / this->nominal_velocity;
}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS>
  BasicModel<SCALAR,OPTIONS>::getProcessNoise(const SCALAR time_step)
{
  Eigen::Matrix<SCALAR,1,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS> output;

  output.template middleCols<1>(BasicModelDim::TRUTH_NOISE::HEADING_BIAS_IND) =
    this->heading_bias_noise->getContinuousNoise(time_step);
  output.template middleCols<1>(BasicModelDim::TRUTH_NOISE::ABS_PRESSURE_BIAS_IND) =
    this->abs_pressure_bias_noise->getContinuousNoise(time_step);
  output.template middleCols<1>(BasicModelDim::TRUTH_NOISE::FEATURE_RANGE_BIAS_IND) =
    this->feature_range_bias_noise->getContinuousNoise(time_step);
  output.template middleCols<3>(BasicModelDim::TRUTH_NOISE::FEATURE_BEARING_BIAS_START_IND) =
    this->feature_bearing_bias_noise->getContinuousNoise(time_step);
  output.template middleCols<3>(BasicModelDim::TRUTH_NOISE::GPS_POS_BIAS_START_IND) =
    this->gps_position_bias_noise->getContinuousNoise(time_step);
  output.template middleCols<3>(BasicModelDim::TRUTH_NOISE::GYRO_BIAS_START_IND) =
    this->gyro_bias_noise->getContinuousNoise(time_step);
  output.template middleCols<3>(BasicModelDim::TRUTH_NOISE::ACCEL_BIAS_START_IND) =
    this->accel_bias_noise->getContinuousNoise(time_step);

  return output;
}

template<typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,BasicModelDim::TRUTH_NOISE_DIM,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS>
  BasicModel<SCALAR,OPTIONS>::getTruthStateNoiseCovariance()
{
  Eigen::Matrix<SCALAR,BasicModelDim::TRUTH_NOISE_DIM,BasicModelDim::TRUTH_NOISE_DIM,OPTIONS> output;

  output.setZero();
  output.template block<1,1>(BasicModelDim::TRUTH_NOISE::HEADING_BIAS_IND,BasicModelDim::TRUTH_NOISE::HEADING_BIAS_IND) =
    this->heading_bias_noise->getCovariance();
  output.template block<1,1>(BasicModelDim::TRUTH_NOISE::ABS_PRESSURE_BIAS_IND,BasicModelDim::TRUTH_NOISE::ABS_PRESSURE_BIAS_IND) =
    this->abs_pressure_bias_noise->getCovariance();
  output.template block<1,1>(BasicModelDim::TRUTH_NOISE::FEATURE_RANGE_BIAS_IND,BasicModelDim::TRUTH_NOISE::FEATURE_RANGE_BIAS_IND) =
    this->feature_range_bias_noise->getCovariance();
  output.template block<3,3>(BasicModelDim::TRUTH_NOISE::FEATURE_BEARING_BIAS_START_IND,BasicModelDim::TRUTH_NOISE::FEATURE_BEARING_BIAS_START_IND)  =
    this->feature_bearing_bias_noise->getCovariance();
  output.template block<3,3>(BasicModelDim::TRUTH_NOISE::GPS_POS_BIAS_START_IND,BasicModelDim::TRUTH_NOISE::GPS_POS_BIAS_START_IND)  =
    this->gps_position_bias_noise->getCovariance();
  output.template block<3,3>(BasicModelDim::TRUTH_NOISE::GYRO_BIAS_START_IND,BasicModelDim::TRUTH_NOISE::GYRO_BIAS_START_IND)  =
    this->gyro_bias_noise->getCovariance();
  output.template block<3,3>(BasicModelDim::TRUTH_NOISE::ACCEL_BIAS_START_IND,BasicModelDim::TRUTH_NOISE::ACCEL_BIAS_START_IND) =
    this->accel_bias_noise->getCovariance();

  return output;
}
} // dynamics
} // kf

#endif
/* basic_model.hpp */
