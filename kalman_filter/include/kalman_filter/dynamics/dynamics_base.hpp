/**
 * @File: dynamics_base.hpp
 * @Date: April 2022
 * @Author: James Swedeen
 *
 * @brief
 * A base class for defining dynamic systems.
 **/

#ifndef KALMAN_FILTER_DYNAMICS_DYNAMICS_BASE_HPP
#define KALMAN_FILTER_DYNAMICS_DYNAMICS_BASE_HPP

/* C++ Headers */
#include<memory>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */

namespace kf
{
namespace dynamics
{
template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
class DynamicsBase;

template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
using DynamicsBasePtr = std::shared_ptr<DynamicsBase<DIM_S,SCALAR,OPTIONS>>;

/**
 * @DIM_S
 * The type of a Dimensions object or an inheriting object that has information about the size of the state vectors.
 *
 * @SCALAR
 * The object type that each dimension will be represented with.
 *
 * @OPTIONS
 * Eigen Matrix options.
 **/
template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
class DynamicsBase
{
public:
  /**
   * @Default Constructor
   **/
  DynamicsBase() = default;
  /**
   * @Copy Constructor
   **/
  DynamicsBase(const DynamicsBase&) = default;
  /**
   * @Move Constructor
   **/
  DynamicsBase(DynamicsBase&&) = default;
  /**
   * @Deconstructor
   **/
  virtual ~DynamicsBase() = default;
  /**
   * @Assignment Operators
   **/
  DynamicsBase& operator=(const DynamicsBase&)  = default;
  DynamicsBase& operator=(      DynamicsBase&&) = default;
  /**
   * @getTruthStateTimeDerivative
   *
   * @brief
   * Finds the time derivative of the truth state with additive process noise.
   *
   * @parameters
   * time: The current simulation time in terms of the number of simulation time steps have passed since the start time
   * truth_state: The current truth state vector
   * control_input: The current control vector
   * process_noise: The process noise on the truth state dynamics
   *
   * @return
   * The time derivative of the truth state vector.
   **/
  inline virtual Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>
    getTruthStateTimeDerivative(const SCALAR                                                                    time,
                                const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,      OPTIONS>>& truth_state,
                                const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,    OPTIONS>>& control_input,
                                const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_NOISE_DIM,OPTIONS>>& process_noise) = 0;
  /**
   * @getNavStateTimeDerivative
   *
   * @brief
   * Finds the time derivative of the navigation state.
   *
   * @parameters
   * time: The current simulation time in terms of the number of simulation time steps have passed since the start time
   * nav_state: The current navigation state vector
   * control_input: The current control vector
   * inertial_reading: The inertial measurements with biases and noise
   *
   * @return
   * The time derivative of the navigation state vector.
   **/
  inline virtual Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>
    getNavStateTimeDerivative(const SCALAR                                                                  time,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,      OPTIONS>>& nav_state,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,  OPTIONS>>& control_input,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::INER_MEAS_DIM,OPTIONS>>& inertial_reading) = 0;
  /**
   * @getLinearizedTruthStateDynamicsMatrix
   *
   * @brief
   * Finds the derivative of the truth state dynamics with respect to the truth state and then evaluated
   * along the nominal reference trajectory.
   *
   * @parameters
   * ref_truth_state: The current state from the reference trajectory mapped into a truth state vector
   * ref_inertial_reading: The inertial measurements without biases and noise
   *
   * @return
   * The derivative of the truth state dynamics with respect to the truth state.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM_S::TRUTH_DISP_DIM,DIM_S::TRUTH_DISP_DIM,OPTIONS>
    getLinearizedTruthStateDynamicsMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,    OPTIONS>>& ref_truth_state,
                                          const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::INER_MEAS_DIM,OPTIONS>>& ref_inertial_reading) = 0;
  /**
   * @getLinearizedTruthStateProcessNoiseMatrix
   *
   * @brief
   * Finds the derivative of the true state dynamics with respect to the process noise vector and then
   * evaluated along the nominal reference trajectory.
   *
   * @parameters
   * ref_truth_state: The current state from the reference trajectory mapped into a truth state vector
   *
   * @return
   * The derivative of the true state dynamics with respect to the process noise vector.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM_S::TRUTH_DISP_DIM,DIM_S::TRUTH_NOISE_DIM,OPTIONS>
    getLinearizedTruthStateProcessNoiseMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& ref_truth_state) = 0;
  /**
   * @getLinearizedNavStateDynamicsMatrix
   *
   * @brief
   * Finds the derivative of the navigation state dynamics with respect to the navigation state and then
   * evaluated along the nominal reference trajectory.
   *
   * @parameters
   * ref_nav_state: The current state from the reference trajectory mapped into a navigation state vector
   * ref_inertial_reading: The inertial measurements without biases and noise
   *
   * @return
   * The derivative of the navigation state dynamics with respect to the navigation state.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>
    getLinearizedNavStateDynamicsMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,      OPTIONS>>& ref_nav_state,
                                        const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::INER_MEAS_DIM,OPTIONS>>& ref_inertial_reading) = 0;
  /**
   * @getLinearizedNavStateInertialMeasurementMatrix
   *
   * @brief
   * Finds the derivative of the navigation state dynamics with respect to the inertial measurement vector and then
   * evaluated along the nominal reference trajectory.
   *
   * @parameters
   * ref_nav_state: The current state from the reference trajectory mapped into a navigation state vector
   *
   * @return
   * The derivative of the navigation state dynamics with respect to the inertial measurement vector.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::INER_MEAS_DIM,OPTIONS>
    getLinearizedNavStateInertialMeasurementMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& ref_nav_state) = 0;
  /**
   * @getLinearizedErrorStateDynamicsMatrix
   *
   * @brief
   * The matrix that you get from linearizing the error state dynamics about the nominal trajectory.
   *
   * @parameters
   * time: The current simulation time in terms of the number of simulation time steps have passed since the start time
   * nav_state: The current navigation state vector
   * inertial_reading: The inertial measurements with biases and noise
   *
   * @return
   * The matrix that you get from linearizing the error state dynamics about the navigation state.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>
    getLinearizedErrorStateDynamicsMatrix(const SCALAR                                                                  time,
                                          const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,      OPTIONS>>& nav_state,
                                          const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::INER_MEAS_DIM,OPTIONS>>& inertial_reading) = 0;
  /**
   * @getErrorStateNoiseCovariance
   *
   * @brief
   * Gets the covariance matrix of the error state dynamics.
   *
   * @parameters
   * nav_state: The current navigation state vector
   * inertial_noise_covariance: The covariance of the noise from the inertial measurements
   *
   * @return
   * The covariance matrix of the additive noise for the error state.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>
    getErrorStateNoiseCovariance(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,                   DIM_S::NAV_DIM,      OPTIONS>>& nav_state,
                                 const Eigen::Ref<const Eigen::Matrix<SCALAR,DIM_S::INER_MEAS_DIM,DIM_S::INER_MEAS_DIM,OPTIONS>>& inertial_noise_covariance) = 0;
  /**
   * @findTimeStep
   *
   * @brief
   * Used to calculate the time that is spanned between two state vectors along the simulation vector.
   *
   * @parameters
   * prev_ref_state: The reference state of the previous time step
   * cur_ref_state: The reference state of the current time step
   *
   * @return
   * The time that is spanned between prev_ref_state and cur_ref_state in seconds.
   **/
  inline virtual SCALAR findTimeStep(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::REF_DIM,OPTIONS>>& prev_ref_state,
                                     const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::REF_DIM,OPTIONS>>& cur_ref_state) = 0;
  /**
   * @getProcessNoise
   *
   * @brief
   * Gets the process noise on the truth state dynamics other then the inertial measurement noise.
   *
   * @parameters
   * time_step: The time step between the last state vector and the current one
   *
   * @return
   * The process noise.
   **/
  inline virtual Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_NOISE_DIM,OPTIONS> getProcessNoise(const SCALAR time_step) = 0;
  /**
   * @getTruthStateNoiseCovariance
   *
   * @brief
   * Gets the covariance matrix of the additive process noise.
   *
   * @parameters
   * inertial_noise_covariance: The covariance of the noise from the inertial measurements
   *
   * @return
   * The covariance matrix of the additive noise for the truth state.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM_S::TRUTH_NOISE_DIM,DIM_S::TRUTH_NOISE_DIM,OPTIONS>
    getTruthStateNoiseCovariance() = 0;
};
} // dynamics
} // kf

#endif
/* dynamics_base.hpp */
