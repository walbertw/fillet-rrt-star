/**
 * @File: propagate_augmented_covariance.hpp
 * @Date: May 2022
 * @Author: James Swedeen
 *
 * @brief
 * Function used to propagate the augmented state covariance matrix.
 **/

#ifndef KALMAN_FILTER_HELPERS_PROPAGATE_AUGMENTED_COVARIANCE_HPP
#define KALMAN_FILTER_HELPERS_PROPAGATE_AUGMENTED_COVARIANCE_HPP

/* C++ Headers */
#include<functional>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */
#include<kalman_filter/helpers/rk4_step.hpp>
#include<kalman_filter/dynamics/dynamics_base.hpp>
#include<kalman_filter/sensors/inertial_measurements/inertial_measurement_base.hpp>

namespace kf
{
/**
 * @propagateAugCovariance
 *
 * @brief
 * The function that propagates the augmented state covariance matrix.
 *
 * @templates
 * DIM_S: The type of a Dimensions object or an inheriting object that has information
 *        about the size of the state vectors
 * SCALAR: The object type that each dimension will be represented with
 * OPTIONS: Eigen Matrix options
 * AUG_COV_DERIVED: The matrix type of the augmented state covariance
 * NAV_DERIVED: The matrix type of the navigation state
 * TRUTH_DERIVED: The matrix type of the truth state
 * INER_MEAS_DERIVED: The matrix type of the inertial measurement state
 *
 * @parameters
 * prev_aug_covariance: The old augmented state covariance matrix
 * dynamics_func: Defines the truth and navigation state dynamics
 * inertial_measurements_func: Produces inertial measurement readings
 * simulation_dt: The time step between each state vector in the simulation
 * ref_truth_state: The current state from the reference trajectory mapped into a truth state vector
 * ref_nav_state: The current state from the reference trajectory mapped into a navigation state vector
 * ref_inertial_reading: The inertial measurements without biases and noise
 *
 * @return
 * The new augmented state covariance matrix.
 **/
template<typename              DIM_S,
         typename              SCALAR,
         Eigen::StorageOptions OPTIONS,
         typename              AUG_COV_DERIVED,
         typename              NAV_DERIVED,
         typename              TRUTH_DERIVED,
         typename              INER_MEAS_DERIVED>
inline typename AUG_COV_DERIVED::PlainMatrix
  propagateAugCovariance(const Eigen::MatrixBase<AUG_COV_DERIVED>&                        prev_aug_covariance,
                         const dynamics::DynamicsBasePtr<          DIM_S,SCALAR,OPTIONS>& dynamics_func,
                         const sensors::InertialMeasurementBasePtr<DIM_S,SCALAR,OPTIONS>& inertial_measurements_func,
                         const SCALAR                                                     simulation_dt,
                         const Eigen::MatrixBase<NAV_DERIVED>&                            ref_nav_state,
                         const Eigen::MatrixBase<TRUTH_DERIVED>&                          ref_truth_state,
                         const Eigen::MatrixBase<INER_MEAS_DERIVED>&                      ref_inertial_reading);
} // kf

template<typename              DIM_S,
         typename              SCALAR,
         Eigen::StorageOptions OPTIONS,
         typename              AUG_COV_DERIVED,
         typename              NAV_DERIVED,
         typename              TRUTH_DERIVED,
         typename              INER_MEAS_DERIVED>
inline typename AUG_COV_DERIVED::PlainMatrix
  kf::propagateAugCovariance(const Eigen::MatrixBase<AUG_COV_DERIVED>&                        prev_aug_covariance,
                             const dynamics::DynamicsBasePtr<          DIM_S,SCALAR,OPTIONS>& dynamics_func,
                             const sensors::InertialMeasurementBasePtr<DIM_S,SCALAR,OPTIONS>& inertial_measurements_func,
                             const SCALAR                                                     simulation_dt,
                             const Eigen::MatrixBase<NAV_DERIVED>&                            ref_nav_state,
                             const Eigen::MatrixBase<TRUTH_DERIVED>&                          ref_truth_state,
                             const Eigen::MatrixBase<INER_MEAS_DERIVED>&                      ref_inertial_reading)
{
  const Eigen::Matrix<SCALAR,DIM_S::TRUTH_DISP_DIM,DIM_S::TRUTH_DISP_DIM,OPTIONS> lin_truth_dynamics_truth =
    dynamics_func->getLinearizedTruthStateDynamicsMatrix(ref_truth_state, ref_inertial_reading);
  const Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS> lin_nav_dynamics_nav =
    dynamics_func->getLinearizedNavStateDynamicsMatrix(ref_nav_state, ref_inertial_reading);
  const Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::INER_MEAS_DIM,OPTIONS> lin_nav_dynamics_inertial_meas =
    dynamics_func->getLinearizedNavStateInertialMeasurementMatrix(ref_nav_state);
  const Eigen::Matrix<SCALAR,DIM_S::INER_MEAS_DIM,DIM_S::ERROR_DIM,OPTIONS> lin_meas_nav =
    inertial_measurements_func->getLinearizedInertialMeasurementMatrix(ref_nav_state);

  Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::INER_MEAS_DIM,OPTIONS> G;
  G.template topRows<   DIM_S::TRUTH_DISP_DIM>().setZero();
  G.template bottomRows<DIM_S::ERROR_DIM>() = lin_nav_dynamics_inertial_meas;

  Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::TRUTH_NOISE_DIM,OPTIONS> W;
  W.template topRows<   DIM_S::TRUTH_DISP_DIM>() = dynamics_func->getLinearizedTruthStateProcessNoiseMatrix(ref_truth_state);
  W.template bottomRows<DIM_S::ERROR_DIM>().setZero();

  const Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::LINCOV::AUG_DIM,OPTIONS> psd_inertial_measurements =
    G * inertial_measurements_func->getNoiseCovariance() * G.transpose();
  const Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::LINCOV::AUG_DIM,OPTIONS> psd_process_noise =
    W * dynamics_func->getTruthStateNoiseCovariance() * W.transpose();

  const Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::LINCOV::AUG_DIM,OPTIONS> psd_sum =
    psd_inertial_measurements + psd_process_noise;

  Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::LINCOV::AUG_DIM,OPTIONS> aug_dynamics_matrix;
  aug_dynamics_matrix.template topRows<   DIM_S::TRUTH_DISP_DIM>().template leftCols< DIM_S::TRUTH_DISP_DIM>() = lin_truth_dynamics_truth;
  aug_dynamics_matrix.template topRows<   DIM_S::TRUTH_DISP_DIM>().template rightCols<DIM_S::ERROR_DIM>().setZero();
  aug_dynamics_matrix.template bottomRows<DIM_S::ERROR_DIM>().     template leftCols< DIM_S::TRUTH_DISP_DIM>() = lin_nav_dynamics_inertial_meas * lin_meas_nav;
  aug_dynamics_matrix.template bottomRows<DIM_S::ERROR_DIM>().     template rightCols<DIM_S::ERROR_DIM>()      = lin_nav_dynamics_nav;

  const std::function<Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::LINCOV::AUG_DIM,OPTIONS>(
      const Eigen::Ref<const Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::LINCOV::AUG_DIM,OPTIONS>>&)> aug_covariance_de =
    [&aug_dynamics_matrix, &psd_sum]
    (const Eigen::Ref<const Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::LINCOV::AUG_DIM,OPTIONS>>& prev_aug_covariance)
    {
      return (aug_dynamics_matrix * prev_aug_covariance) +
             (prev_aug_covariance * aug_dynamics_matrix.transpose()) +
             psd_sum;
    };

  return rk4Step<SCALAR>(aug_covariance_de, prev_aug_covariance, simulation_dt);
}

#endif
/* propagate_augmented_covariance.hpp */
