/**
 * @File: propagate_error_covariance.hpp
 * @Date: May 2022
 * @Author: James Swedeen
 *
 * @brief
 * Function used to propagate the error state covariance matrix.
 **/

#ifndef KALMAN_FILTER_HELPERS_PROPAGATE_ERROR_COVARIANCE_HPP
#define KALMAN_FILTER_HELPERS_PROPAGATE_ERROR_COVARIANCE_HPP

/* C++ Headers */
#include<functional>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */
#include<kalman_filter/helpers/rk4_step.hpp>
#include<kalman_filter/dynamics/dynamics_base.hpp>

namespace kf
{
/**
 * @propagateErrorCovariance
 *
 * @brief
 * The function that propagates the error state covariance matrix.
 *
 * @templates
 * DIM_S: The type of a Dimensions object or an inheriting object that has information
 *        about the size of the state vectors
 * SCALAR: The object type that each dimension will be represented with
 * OPTIONS: Eigen Matrix options
 * ERROR_COV_DERIVED: The matrix type of the error state covariance
 * NAV_DERIVED: The matrix type of the navigation state
 * INER_MEAS_DERIVED: The matrix type of the inertial measurement state
 *
 * @parameters
 * prev_error_covariance: The old error state covariance matrix
 * dynamics_func: Defines the truth and navigation state dynamics
 * simulation_dt: The time step between each state vector in the simulation
 * time: The current simulation time
 * nav_state: The current navigation state vector
 * inertial_reading: The inertial measurements with biases and noise
 * inertial_noise_covariance: The covariance of the noise from the inertial measurements
 *
 * @return
 * The new error state covariance matrix.
 **/
template<typename              DIM_S,
         typename              SCALAR,
         Eigen::StorageOptions OPTIONS,
         typename              ERROR_COV_DERIVED,
         typename              NAV_DERIVED,
         typename              INER_MEAS_DERIVED,
         typename              INER_MEAS_COV_DERIVED>
inline typename ERROR_COV_DERIVED::PlainMatrix
  propagateErrorCovariance(const Eigen::MatrixBase<ERROR_COV_DERIVED>&            prev_error_covariance,
                           const dynamics::DynamicsBasePtr<DIM_S,SCALAR,OPTIONS>& dynamics_func,
                           const SCALAR                                           simulation_dt,
                           const SCALAR                                           time,
                           const Eigen::MatrixBase<NAV_DERIVED>&                  nav_state,
                           const Eigen::MatrixBase<INER_MEAS_DERIVED>&            inertial_reading,
                           const Eigen::MatrixBase<INER_MEAS_COV_DERIVED>&        inertial_noise_covariance);
} // kf

template<typename              DIM_S,
         typename              SCALAR,
         Eigen::StorageOptions OPTIONS,
         typename              ERROR_COV_DERIVED,
         typename              NAV_DERIVED,
         typename              INER_MEAS_DERIVED,
         typename              INER_MEAS_COV_DERIVED>
inline typename ERROR_COV_DERIVED::PlainMatrix
  kf::propagateErrorCovariance(const Eigen::MatrixBase<ERROR_COV_DERIVED>&            prev_error_covariance,
                               const dynamics::DynamicsBasePtr<DIM_S,SCALAR,OPTIONS>& dynamics_func,
                               const SCALAR                                           simulation_dt,
                               const SCALAR                                           time,
                               const Eigen::MatrixBase<NAV_DERIVED>&                  nav_state,
                               const Eigen::MatrixBase<INER_MEAS_DERIVED>&            inertial_reading,
                               const Eigen::MatrixBase<INER_MEAS_COV_DERIVED>&        inertial_noise_covariance)
{
  const Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS> linear_error_dynamics =
    dynamics_func->getLinearizedErrorStateDynamicsMatrix(time, nav_state, inertial_reading);
  const Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS> error_noise_covariance =
    dynamics_func->getErrorStateNoiseCovariance(nav_state, inertial_noise_covariance);

  const std::function<Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>(
      const Eigen::Ref<const Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>>&)> error_covariance_de =
    [&linear_error_dynamics, &error_noise_covariance]
    (const Eigen::Ref<const Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>>& prev_error_covariance)
    {
      return (linear_error_dynamics * prev_error_covariance) +
             (prev_error_covariance * linear_error_dynamics.transpose()) +
             error_noise_covariance;
    };

  return rk4Step<SCALAR>(error_covariance_de, prev_error_covariance, simulation_dt);
}

#endif
/* propagate_error_covariance.hpp */
