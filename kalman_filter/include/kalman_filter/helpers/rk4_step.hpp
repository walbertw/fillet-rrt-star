/**
 * @File: rk4_step.hpp
 * @Date: April 2022
 * @Author: James Swedeen
 *
 * @brief
 * Defines a function that implements one rk4 step.
 **/

#ifndef KALMAN_FILTER_HELPERS_RK4_STEP_HPP
#define KALMAN_FILTER_HELPERS_RK4_STEP_HPP

/* C++ Headers */
#include<functional>

/* Eigen Headers */
#include<Eigen/Dense>

namespace kf
{
/**
 * @rk4Step
 *
 * @brief
 * Used to perform one step of rk4 integration.
 *
 * @templates
 * SCALAR: The object type that each dimension will be represented with
 * FUNC_DERIVED: The matrix type that the derivative function uses
 * INPUT_DERIVED: The matrix type that this function works with
 *
 * @parameters
 * derivative_func: The derivative of the value we are integrating
 * prev_x: The old value
 * dt: The step over which we are integrating
 *
 * @return
 * The integrated state vector.
 **/
template<typename SCALAR, typename FUNC_DERIVED, typename INPUT_DERIVED>
inline typename INPUT_DERIVED::PlainMatrix
  rk4Step(const std::function<typename FUNC_DERIVED::PlainMatrix(const Eigen::Ref<const FUNC_DERIVED>&)>& derivative_func,
          const Eigen::MatrixBase<INPUT_DERIVED>&                                                         prev_x,
          const SCALAR                                                                                    dt);
} // kf

template<typename SCALAR, typename FUNC_DERIVED, typename INPUT_DERIVED>
inline typename INPUT_DERIVED::PlainMatrix
  kf::rk4Step(const std::function<typename FUNC_DERIVED::PlainMatrix(const Eigen::Ref<const FUNC_DERIVED>&)>& derivative_func,
              const Eigen::MatrixBase<INPUT_DERIVED>&                                                         prev_x,
              const SCALAR                                                                                    dt)
{
  const SCALAR half_dt = dt / SCALAR(2);

  const typename INPUT_DERIVED::PlainMatrix k1 = derivative_func(prev_x);
  const typename INPUT_DERIVED::PlainMatrix k2 = derivative_func(prev_x + (half_dt * k1.array()).matrix());
  const typename INPUT_DERIVED::PlainMatrix k3 = derivative_func(prev_x + (half_dt * k2.array()).matrix());
  const typename INPUT_DERIVED::PlainMatrix k4 = derivative_func(prev_x + (dt      * k3.array()).matrix());

  return prev_x + ((dt/SCALAR(6)) * (k1 + (SCALAR(2)*k2.array()).matrix() + (SCALAR(2)*k3.array()).matrix() + k4).array()).matrix();
}

#endif
/* rk4_step.hpp */
