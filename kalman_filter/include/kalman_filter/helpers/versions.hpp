/**
 * @File: versions.hpp
 * @Date: April 2022
 * @Author: James Swedeen
 *
 * @brief
 * Used to control which version of a Kalman filter will be run.
 **/

#ifndef KALMAN_FILTER_HELPERS_VERIONS_HPP
#define KALMAN_FILTER_HELPERS_VERIONS_HPP

/* C++ Headers */
#include<cstdint>

namespace kf
{
enum Versions : uint64_t
{
  /**
   * @NULL_VERSION
   *
   * This enumeration explicitly represents nothing.
   * The result of using it will be the baseline Kalman filter.
   **/
  NULL_VERSION = 0x0000'0000'0000'0000,
  /**
   * @OPEN_LOOP
   *
   * If enabled the Kalman filter will assume that the control commands are zeroed out.
   **/
  OPEN_LOOP = 0x0000'0000'0000'0001,
  /**
   * @APPLY_STATE_BOUNDS
   *
   * If true, the kalman filter will apply bounds on the states specified in the tools object.
   **/
  APPLY_STATE_BOUNDS = 0x0000'0000'0000'0002,
  /**
   * @RUNNING_ERROR_BUDGET
   *
   * To be used internally whenever the code is performing an error budget analysis.
   * When enabled lincov will use the pre-existing error covariance values.
   **/
  RUNNING_ERROR_BUDGET = 0x8000'0000'0000'0000,
};

/**
 * @valid
 *
 * @brief
 * Tests to see if the given configuration is valid.
 *
 * @parameters
 * config: The configuration to test
 **/
template<Versions VERSION>
constexpr void monteCarloValid() noexcept;
template<Versions VERSION>
constexpr void linCovValid()     noexcept;

/**
 * @test
 *
 * @brief
 * Each is used to test if a given attribute is held in the
 * given configuration.
 *
 * @parameters
 * config: The configuration to test
 *
 * @return
 * True if the attribute asked about is true in the configuration given.
 **/
constexpr bool nullVersion(       const Versions config) noexcept;
constexpr bool openLoop(          const Versions config) noexcept;
constexpr bool runningErrorBudget(const Versions config) noexcept;
constexpr bool applyStateBounds(  const Versions config) noexcept;
} // kf


template<kf::Versions VERSION>
constexpr void kf::monteCarloValid() noexcept
{
  static_assert(not runningErrorBudget(VERSION));
}

template<kf::Versions VERSION>
constexpr void kf::linCovValid() noexcept
{
  static_assert(not applyStateBounds(VERSION), "This isn't possible in LinCov");
}

constexpr bool kf::nullVersion(const Versions config) noexcept
{
  return Versions::NULL_VERSION == config;
}

constexpr bool kf::openLoop(const Versions config) noexcept
{
  return Versions::OPEN_LOOP == (config bitand Versions::OPEN_LOOP);
}

constexpr bool kf::runningErrorBudget(const Versions config) noexcept
{
  return Versions::RUNNING_ERROR_BUDGET == (config bitand Versions::RUNNING_ERROR_BUDGET);
}

constexpr bool kf::applyStateBounds(const Versions config) noexcept
{
  return Versions::APPLY_STATE_BOUNDS == (config bitand Versions::APPLY_STATE_BOUNDS);
}

#endif
/* versions.hpp */
