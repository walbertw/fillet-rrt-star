/**
 * @File: run_lin_cov.hpp
 * @Date: May 2022
 * @Author: James Swedeen
 *
 * @brief
 * Defines a function that runs one Linear Covariance simulation.
 **/

#ifndef KALMAN_FILTER_RUN_LIN_COV_HPP
#define KALMAN_FILTER_RUN_LIN_COV_HPP

/* C++ Headers */
#include<cstdint>
#include<functional>
#include<vector>
#include<execution>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */
#include<kalman_filter/helpers/versions.hpp>
#include<kalman_filter/helpers/rk4_step.hpp>
#include<kalman_filter/helpers/propagate_error_covariance.hpp>
#include<kalman_filter/helpers/propagate_augmented_covariance.hpp>
#include<kalman_filter/helpers/tools.hpp>

namespace kf
{
/**
 * @runLinCov
 *
 * @brief
 * The function that propagates the linearized kalman equations forward in time along a nominal trajectory.
 *
 * @templates
 * DIM_S: The type of a Dimensions object or an inheriting object that has information
 *        about the size of the state vectors
 * VERSION: Controls what type of simulation will be ran
 * SCALAR: The object type that each dimension will be represented with
 * OPTIONS: Eigen Matrix options
 *
 * @parameters
 * state_vector: The vector that holds all of the simulations states. See dimension_struct.hpp
 *               It is assumed that the first state and the reference trajectory is already set
 * tools: Holds all of the needed helper functions
 **/
template<typename DIM_S, Versions VERSION, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
inline void
  runLinCov(Eigen::Ref<Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::LINCOV::FULL_STATE_LEN,OPTIONS>> state_vector,
            const Tools<DIM_S,SCALAR,OPTIONS>&                                                     tools);
} // kf

template<typename DIM_S, kf::Versions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void
  kf::runLinCov(Eigen::Ref<Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::LINCOV::FULL_STATE_LEN,OPTIONS>> state_vector,
                const Tools<DIM_S,SCALAR,OPTIONS>&                                                     tools)
{
  linCovValid<VERSION>();

  const Eigen::Index state_vector_len = state_vector.rows();
  for(Eigen::Index row_it = 1; row_it < state_vector_len; ++row_it)
  {
    // Extract important vectors
    const SCALAR prev_time = state_vector(row_it-1, DIM_S::TIME_IND);

    const Eigen::Map<const Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>> prev_error_covariance(
      state_vector.template block<1,DIM_S::ERROR_COV_LEN>(row_it-1, DIM_S::ERROR_COV_START_IND).data());
          Eigen::Map<      Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>> cur_error_covariance(
      state_vector.template block<1,DIM_S::ERROR_COV_LEN>(row_it, DIM_S::ERROR_COV_START_IND).data());

    const Eigen::Map<const Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::LINCOV::AUG_DIM,OPTIONS>> prev_aug_covariance(
      state_vector.template block<1,DIM_S::LINCOV::AUG_COV_LEN>(row_it-1, DIM_S::LINCOV::AUG_COV_START_IND).data());
          Eigen::Map<      Eigen::Matrix<SCALAR,DIM_S::LINCOV::AUG_DIM,DIM_S::LINCOV::AUG_DIM,OPTIONS>> cur_aug_covariance(
      state_vector.template block<1,DIM_S::LINCOV::AUG_COV_LEN>(row_it, DIM_S::LINCOV::AUG_COV_START_IND).data());

    // Find time step
    const SCALAR time_step =
      tools.dynamics_func->findTimeStep(state_vector.template block<1,DIM_S::REF_DIM>(row_it-1, DIM_S::REF_START_IND),
                                        state_vector.template block<1,DIM_S::REF_DIM>(row_it,   DIM_S::REF_START_IND));
    if(0 == time_step)
    {
      // Copy the old state
      state_vector.row(row_it) = state_vector.row(row_it-1);
      continue;
    }
    // Set current time
    const SCALAR cur_time = prev_time + time_step;
    state_vector(row_it, DIM_S::TIME_IND) = cur_time;
    // Approximate truth and navigation states
    const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS> prev_truth_state =
      tools.mappings->mapRefTruth(state_vector.template block<1,DIM_S::REF_DIM>(row_it-1, DIM_S::REF_START_IND));
    const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS> cur_truth_state =
      tools.mappings->mapRefTruth(state_vector.template block<1,DIM_S::REF_DIM>(row_it, DIM_S::REF_START_IND));
    const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS> prev_nav_state =
      tools.mappings->mapRefNav(state_vector.template block<1,DIM_S::REF_DIM>(row_it-1, DIM_S::REF_START_IND));
    const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS> cur_nav_state =
      tools.mappings->mapRefNav(state_vector.template block<1,DIM_S::REF_DIM>(row_it, DIM_S::REF_START_IND));

    // Find the control input from the previous time step
    const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS> control_vec =
      tools.controller->getControl(prev_nav_state,
                                   state_vector.template block<1,DIM_S::REF_DIM>(row_it, DIM_S::REF_START_IND));

    // Calculate inertial measurements
    const Eigen::Matrix<SCALAR,1,DIM_S::INER_MEAS_DIM,OPTIONS> inertial_measurements =
      tools.inertial_measurements_func->getMeasurement(cur_time,
                                                       time_step,
                                                       cur_truth_state,
                                                       control_vec);
    // Propagate error state covariance
    if constexpr(not runningErrorBudget(VERSION))
    {
      cur_error_covariance =
        propagateErrorCovariance<DIM_S,SCALAR,OPTIONS>(prev_error_covariance,
                                                       tools.dynamics_func,
                                                       time_step,
                                                       cur_time,
                                                       prev_nav_state,
                                                       inertial_measurements,
                                                       tools.inertial_measurements_func->getNoiseCovariance());
    }
    // Propagate augmented state covariance
    cur_aug_covariance =
      propagateAugCovariance<DIM_S,SCALAR,OPTIONS>(prev_aug_covariance,
                                                   tools.dynamics_func,
                                                   tools.inertial_measurements_func,
                                                   time_step,
                                                   prev_nav_state,
                                                   prev_truth_state,
                                                   inertial_measurements);

    // Handle discreet measurement updates
    if constexpr(runningErrorBudget(VERSION))
    {
      state_vector.template block<1,DIM_S::NUM_MEAS_DIM>(row_it, DIM_S::NUM_MEAS_START_IND) =
        tools.measurement_controller->applyMeasurementsErrorBudget(cur_time,
                                                                   state_vector.template block<1,DIM_S::NUM_MEAS_DIM>(row_it-1, DIM_S::NUM_MEAS_START_IND),
                                                                   cur_truth_state,
                                                                   cur_nav_state,
                                                                   cur_error_covariance,
                                                                   cur_aug_covariance);
    }
    else // Not running error budget
    {
      state_vector.template block<1,DIM_S::NUM_MEAS_DIM>(row_it, DIM_S::NUM_MEAS_START_IND) =
        tools.measurement_controller->applyMeasurementsLinCov(cur_time,
                                                              state_vector.template block<1,DIM_S::NUM_MEAS_DIM>(row_it-1, DIM_S::NUM_MEAS_START_IND),
                                                              cur_truth_state,
                                                              cur_nav_state,
                                                              cur_error_covariance,
                                                              cur_aug_covariance);
    }
  }
}

#endif
/* run_lin_cov.hpp */
