/**
 * @File: run_monte_carlo.hpp
 * @Date: April 2022
 * @Author: James Swedeen
 *
 * @brief
 * Defines a function that runs one Monte Carlo simulation.
 **/

#ifndef KALMAN_FILTER_RUN_MONTE_CARLO_HPP
#define KALMAN_FILTER_RUN_MONTE_CARLO_HPP

/* C++ Headers */
#include<cstdint>
#include<functional>
#include<vector>
#include<execution>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */
#include<kalman_filter/helpers/versions.hpp>
#include<kalman_filter/helpers/rk4_step.hpp>
#include<kalman_filter/helpers/propagate_error_covariance.hpp>
#include<kalman_filter/helpers/tools.hpp>

namespace kf
{
/**
 * @runMonteCarlo
 *
 * @brief
 * The function that propagates the kalman equations forward in time along a nominal trajectory.
 *
 * @templates
 * DIM_S: The type of a Dimensions object or an inheriting object that has information
 *        about the size of the state vectors
 * VERSION: Controls what type of simulation will be ran
 * SCALAR: The object type that each dimension will be represented with
 * OPTIONS: Eigen Matrix options
 *
 * @parameters
 * state_vector: The vector that holds all of the simulations states. See dimension_struct.hpp
 *               It is assumed that the first state and the reference trajectory is already set
 * tools: Holds all of the needed helper functions
 **/
template<typename DIM_S, Versions VERSION, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
inline void runMonteCarlo(Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::MC::FULL_STATE_LEN,OPTIONS>& state_vector,
                          const Tools<DIM_S,SCALAR,OPTIONS>&                                      tools);
/**
 * @runMonteCarloSims
 *
 * @brief
 * The function that propagates the kalman equations forward in time along a nominal trajectory.
 *
 * @templates
 * DIM_S: The type of a Dimensions object or an inheriting object that has information
 *        about the size of the state vectors
 * SCALAR: The object type that each dimension will be represented with
 * VERSION: Controls what type of simulation will be ran
 * OPTIONS: Eigen Matrix options
 *
 * @parameters
 * reference_trajectory: The nominal reference trajectory for this set of simulations
 * nominal_initial_state: The starting_state for the simulation without noise
 * number_sim: The number of simulations to run
 * tools: Holds all of the needed helper functions
 * initial_noise_func: A function that is used to add noise to the initial state
 * sims_output: The output of the requested simulations
 **/
template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
using INITIAL_NOISE_FUNC_TYPE = std::function<void(Eigen::Ref<Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>,
                                                   Eigen::Ref<Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,  OPTIONS>>)>;

template<typename DIM_S, Versions VERSION, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
inline void runMonteCarloSims(const Eigen::Ref<const Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::REF_DIM,OPTIONS>>& reference_trajectory,
                              const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::MC::FULL_STATE_LEN,OPTIONS>>&   nominal_initial_state,
                              const size_t                                                                         number_sim,
                              const Tools<DIM_S,SCALAR,OPTIONS>&                                                   tools,
                              const INITIAL_NOISE_FUNC_TYPE<DIM_S,SCALAR,OPTIONS>&                                 initial_noise_func,
                              std::vector<Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::MC::FULL_STATE_LEN,OPTIONS>>& sims_output);
} // kf

template<typename DIM_S, kf::Versions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void kf::runMonteCarlo(Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::MC::FULL_STATE_LEN,OPTIONS>& state_vector,
                              const Tools<DIM_S,SCALAR,OPTIONS>&                                      tools)
{
  monteCarloValid<VERSION>();

  const Eigen::Index state_vector_len = state_vector.rows();
  for(Eigen::Index row_it = 1; row_it < state_vector_len; ++row_it)
  {
    // Extract important vectors
    const SCALAR prev_time = state_vector(row_it-1, DIM_S::TIME_IND);

    const Eigen::Map<const Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>> prev_error_covariance(
      state_vector.template block<1,DIM_S::ERROR_COV_LEN>(row_it-1, DIM_S::ERROR_COV_START_IND).data());
          Eigen::Map<      Eigen::Matrix<SCALAR,DIM_S::ERROR_DIM,DIM_S::ERROR_DIM,OPTIONS>> cur_error_covariance(
      state_vector.template block<1,DIM_S::ERROR_COV_LEN>(row_it, DIM_S::ERROR_COV_START_IND).data());

    // Find time step
    const SCALAR time_step =
      tools.dynamics_func->findTimeStep(state_vector.template block<1,DIM_S::REF_DIM>(row_it-1, DIM_S::REF_START_IND),
                                        state_vector.template block<1,DIM_S::REF_DIM>(row_it,   DIM_S::REF_START_IND));
    if(0 == time_step)
    {
      // Copy the old state
      state_vector.row(row_it) = state_vector.row(row_it-1);
      continue;
    }
    // Set current time
    const SCALAR cur_time = prev_time + time_step;
    state_vector(row_it, DIM_S::TIME_IND) = cur_time;
    // Find the control input from the previous time step
    const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS> control_vec =
      tools.controller->getControl(state_vector.template block<1,DIM_S::NAV_DIM>(row_it-1, DIM_S::MC::NAV_START_IND),
                                   state_vector.template block<1,DIM_S::REF_DIM>(row_it,   DIM_S::REF_START_IND));
    // Propagate truth state
    const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_NOISE_DIM,OPTIONS> process_noise =
      tools.dynamics_func->getProcessNoise(time_step);

    state_vector.template block<1,DIM_S::TRUTH_DIM>(row_it, DIM_S::MC::TRUTH_START_IND) =
      rk4Step<SCALAR,Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>(
        std::bind(&dynamics::DynamicsBase<DIM_S,SCALAR,OPTIONS>::getTruthStateTimeDerivative,
                  tools.dynamics_func.get(),
                  cur_time,
                  std::placeholders::_1,
                  control_vec,
                  process_noise),
        state_vector.template block<1,DIM_S::TRUTH_DIM>(row_it-1, DIM_S::MC::TRUTH_START_IND),
        time_step);
    // Calculate inertial measurements
    const Eigen::Matrix<SCALAR,1,DIM_S::INER_MEAS_DIM,OPTIONS> inertial_measurements =
      tools.inertial_measurements_func->getMeasurement(cur_time,
                                                       time_step,
                                                       state_vector.template block<1,DIM_S::TRUTH_DIM>(row_it, DIM_S::MC::TRUTH_START_IND),
                                                       control_vec);
    // Propagate navigation state
    state_vector.template block<1,DIM_S::NAV_DIM>(row_it, DIM_S::MC::NAV_START_IND) =
      rk4Step<SCALAR,Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>(
        std::bind(&dynamics::DynamicsBase<DIM_S,SCALAR,OPTIONS>::getNavStateTimeDerivative,
                  tools.dynamics_func.get(),
                  cur_time,
                  std::placeholders::_1,
                  control_vec,
                  inertial_measurements),
        state_vector.template block<1,DIM_S::NAV_DIM>(row_it-1, DIM_S::MC::NAV_START_IND),
        time_step);
    // Propagate error state covariance
    cur_error_covariance =
      propagateErrorCovariance<DIM_S,SCALAR,OPTIONS>(prev_error_covariance,
                                                     tools.dynamics_func,
                                                     time_step,
                                                     cur_time,
                                                     state_vector.template block<1,DIM_S::NAV_DIM>(row_it-1, DIM_S::MC::NAV_START_IND),
                                                     inertial_measurements,
                                                     tools.inertial_measurements_func->getNoiseCovariance());
    // Handle discreet measurement updates
    state_vector.template block<1,DIM_S::NUM_MEAS_DIM>(row_it, DIM_S::NUM_MEAS_START_IND) =
      tools.measurement_controller->applyMeasurements(tools.mappings,
                                                      cur_time,
                                                      state_vector.template block<1,DIM_S::NUM_MEAS_DIM>(row_it-1, DIM_S::NUM_MEAS_START_IND),
                                                      state_vector.template block<1,DIM_S::TRUTH_DIM>(   row_it,   DIM_S::MC::TRUTH_START_IND),
                                                      state_vector.template block<1,DIM_S::NAV_DIM>(     row_it,   DIM_S::MC::NAV_START_IND),
                                                      cur_error_covariance);
    // Apply state bounding if needed
    if constexpr(applyStateBounds(VERSION))
    {
      auto bounded_states = state_vector.template block<1,DIM_S::NAV_DIM>(row_it, DIM_S::MC::NAV_START_IND)(tools.bounded_indexes);

      bounded_states = (bounded_states.array() > tools.upper_bound.array()).select(tools.upper_bound, bounded_states);
      bounded_states = (bounded_states.array() < tools.upper_bound.array()).select(tools.lower_bound, bounded_states);
    }
  }
}

template<typename DIM_S, kf::Versions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void kf::runMonteCarloSims(const Eigen::Ref<const Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::REF_DIM,OPTIONS>>& reference_trajectory,
                                  const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::MC::FULL_STATE_LEN,OPTIONS>>&   nominal_initial_state,
                                  const size_t                                                                         number_sim,
                                  const Tools<DIM_S,SCALAR,OPTIONS>&                                                   tools,
                                  const INITIAL_NOISE_FUNC_TYPE<DIM_S,SCALAR,OPTIONS>&                                 initial_noise_func,
                                  std::vector<Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::MC::FULL_STATE_LEN,OPTIONS>>& sims_output)
{
  const std::function<void(Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::MC::FULL_STATE_LEN,OPTIONS>&)> run_one_sim_func =
    [&reference_trajectory, &nominal_initial_state, &tools, &initial_noise_func]
    (Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM_S::MC::FULL_STATE_LEN,OPTIONS>& state_vector)
    {
      state_vector.resize(reference_trajectory.rows(), Eigen::NoChange);
      // Set starting state
      state_vector.template topRows<1>() = nominal_initial_state;
      // Set reference trajectory
      state_vector.template middleCols<DIM_S::REF_DIM>(DIM_S::REF_START_IND) = reference_trajectory;
      // Add starting noise
      initial_noise_func(state_vector.template block<1,DIM_S::TRUTH_DIM>(0, DIM_S::MC::TRUTH_START_IND),
                         state_vector.template block<1,DIM_S::NAV_DIM>(  0, DIM_S::MC::NAV_START_IND));
      // Run simulation
      runMonteCarlo<DIM_S,VERSION,SCALAR,OPTIONS>(state_vector, tools);
     };

  // Get ready to run
  sims_output.resize(number_sim);
  // Run
  std::for_each(std::execution::par_unseq, sims_output.begin(), sims_output.end(), run_one_sim_func);
}

#endif
/* run_monte_carlo.hpp */
