/**
 * @File: imu_base.hpp
 * @Date: May 2022
 * @Author: James Swedeen
 *
 * @brief
 * Defines a base class for simulating an inertial measurement unit.
 **/

#ifndef KALMAN_FILTER_SENSORS_INERTIAL_MEASUREMENTS_IMU_BASE_HPP
#define KALMAN_FILTER_SENSORS_INERTIAL_MEASUREMENTS_IMU_BASE_HPP

/* C++ Headers */
#include<cstdint>
#include<memory>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */
#include<kalman_filter/helpers/dimension_struct.hpp>
#include<kalman_filter/sensors/inertial_measurements/inertial_measurement_base.hpp>

namespace kf
{
namespace sensors
{
template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
class IMUBase;

template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
using IMUBasePtr = std::shared_ptr<IMUBase<DIM_S,SCALAR,OPTIONS>>;

/**
 * @DIM_S
 * The type of a Dimensions object or an inheriting object that has information about the size of the state vectors.
 *
 * @SCALAR
 * The object type that each dimension will be represented with.
 *
 * @OPTIONS
 * Eigen Matrix options.
 **/
template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
class IMUBase
 : public InertialMeasurementBase<DIM_S,SCALAR,OPTIONS>
{
public:
  /**
   * @Default Constructor
   **/
  IMUBase() = default;
  /**
   * @Copy Constructor
   **/
  IMUBase(const IMUBase&) = default;
  /**
   * @Move Constructor
   **/
  IMUBase(IMUBase&&) = default;
  /**
   * @Deconstructor
   **/
  ~IMUBase() override = default;
  /**
   * @Assignment Operators
   **/
  IMUBase& operator=(const IMUBase&)  = default;
  IMUBase& operator=(      IMUBase&&) = default;
  /**
   * @getMeasurement
   *
   * @brief
   * Used to synthesize a measurement with noise.
   *
   * @parameters
   * time: The current simulation time
   * time_step: The time difference between the last state in the simulation and the current one
   * truth_state: The current truth state vector
   * control_input: The current control vector
   *
   * @return
   * The vector of inertial measurements.
   **/
  inline Eigen::Matrix<SCALAR,1,DIM_S::INER_MEAS_DIM,OPTIONS>
    getMeasurement(const SCALAR                                                                time,
                   const SCALAR                                                                time_step,
                   const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,  OPTIONS>>& truth_state,
                   const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS>>& control_input) override = 0;
  /**
   * @getLinearizedInertialMeasurementMatrix
   *
   * @brief
   * Finds the derivative of the inertial measurement function with respect to the navigation state and then
   * evaluated along the nominal reference trajectory.
   *
   * @parameters
   * ref_nav_state: The current state from the reference trajectory mapped into a navigation state vector
   *
   * @return
   * The time derivative of the truth state vector.
   **/
  inline Eigen::Matrix<SCALAR,6,DIM_S::ERROR_DIM,OPTIONS>
    getLinearizedInertialMeasurementMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& ref_nav_state) override = 0;
  /**
   * @getNoiseCovariance
   *
   * @brief
   * Gets the covariance matrix of the additive noise.
   *
   * @return
   * The covariance matrix of the additive noise.
   **/
  inline Eigen::Matrix<SCALAR,DIM_S::INER_MEAS_DIM,DIM_S::INER_MEAS_DIM,OPTIONS> getNoiseCovariance() override = 0;
  /**
   * @IND
   *
   * @brief
   * Helper enumeration that defines where each element of the inertial measurements vector is.
   **/
  enum class IND : Eigen::Index
  {
    ROLL_RATE  = 0, // In body frame
    PITCH_RATE = 1, // In body frame
    YAW_RATE   = 2, // In body frame
    X_ACCEL    = 3, // Accelerations in the body frame
    Y_ACCEL    = 4, // Accelerations in the body frame
    Z_ACCEL    = 5  // Accelerations in the body frame
  };
};
} // sensors
} // kf

#endif
/* imu_base.hpp */
