/**
 * @File: open_loop_imu.hpp
 * @Date: May 2022
 * @Author: James Swedeen
 *
 * @brief
 * Defines a class for simulation a IMU unit in open loop analysis.
 **/

#ifndef KALMAN_FILTER_SENSORS_INERTIAL_MEASUREMENTS_OPEN_LOOP_IMU_HPP
#define KALMAN_FILTER_SENSORS_INERTIAL_MEASUREMENTS_OPEN_LOOP_IMU_HPP

/* C++ Headers */
#include<cstdint>
#include<memory>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */
#include<kalman_filter/noise/noise_base.hpp>
#include<kalman_filter/sensors/inertial_measurements/imu_base.hpp>

namespace kf
{
namespace sensors
{
template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
class OpenLoopIMU;

template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
using OpenLoopIMUPtr = std::shared_ptr<OpenLoopIMU<DIM_S,SCALAR,OPTIONS>>;

/**
 * @DIM_S
 * The type of a Dimensions object or an inheriting object that has information about the size of the state vectors.
 *
 * @SCALAR
 * The object type that each dimension will be represented with.
 *
 * @OPTIONS
 * Eigen Matrix options.
 **/
template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
class OpenLoopIMU
 : public IMUBase<DIM_S,SCALAR,OPTIONS>
{
public:
  /**
   * @Default Constructor
   **/
  OpenLoopIMU() = delete;
  /**
   * @Copy Constructor
   **/
  OpenLoopIMU(const OpenLoopIMU&) = default;
  /**
   * @Move Constructor
   **/
  OpenLoopIMU(OpenLoopIMU&&) = default;
  /**
   * @Constructor
   *
   * @brief
   * Initializes the class for use.
   *
   * @parameters
   * accel_noise: Additive noise on the accelerometer readings
   * gyro_noise: Additive noise on the gyroscope readings
   **/
  OpenLoopIMU(const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& accel_noise,
              const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& gyro_noise);
  /**
   * @Deconstructor
   **/
  ~OpenLoopIMU() override = default;
  /**
   * @Assignment Operators
   **/
  OpenLoopIMU& operator=(const OpenLoopIMU&)  = default;
  OpenLoopIMU& operator=(      OpenLoopIMU&&) = default;
  /**
   * @getMeasurement
   *
   * @brief
   * Used to synthesize a measurement with noise.
   *
   * @parameters
   * time: The current simulation time
   * time_step: The time difference between the last state in the simulation and the current one
   * truth_state: The current truth state vector
   * control_input: The current control vector
   *
   * @return
   * The vector of inertial measurements.
   **/
  inline Eigen::Matrix<SCALAR,1,6,OPTIONS>
    getMeasurement(const SCALAR                                                                time,
                   const SCALAR                                                                time_step,
                   const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,  OPTIONS>>& truth_state,
                   const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS>>& control_input) override;
  /**
   * @getLinearizedInertialMeasurementMatrix
   *
   * @brief
   * Finds the derivative of the inertial measurement function with respect to the navigation state and then
   * evaluated along the nominal reference trajectory.
   *
   * @parameters
   * ref_nav_state: The current state from the reference trajectory mapped into a navigation state vector
   *
   * @return
   * The time derivative of the truth state vector.
   **/
  inline Eigen::Matrix<SCALAR,6,DIM_S::ERROR_DIM,OPTIONS>
    getLinearizedInertialMeasurementMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& ref_nav_state) override;
  /**
   * @getNoiseCovariance
   *
   * @brief
   * Gets the covariance matrix of the additive noise.
   *
   * @return
   * The covariance matrix of the additive noise.
   **/
  inline Eigen::Matrix<SCALAR,6,6,OPTIONS> getNoiseCovariance() override;
private:
  noise::NoiseBasePtr<3,SCALAR,OPTIONS> accel_noise;
  noise::NoiseBasePtr<3,SCALAR,OPTIONS> gyro_noise;
};

template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
OpenLoopIMU<DIM_S,SCALAR,OPTIONS>::
  OpenLoopIMU(const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& accel_noise,
              const noise::NoiseBasePtr<3,SCALAR,OPTIONS>& gyro_noise)
 : IMUBase<DIM_S,SCALAR,OPTIONS>(),
   accel_noise(accel_noise),
   gyro_noise(gyro_noise)
{}

template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,6,OPTIONS> OpenLoopIMU<DIM_S,SCALAR,OPTIONS>::
  getMeasurement(const SCALAR                                                                /* time */,
                 const SCALAR                                                                time_step,
                 const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,  OPTIONS>>& truth_state,
                 const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS>>& control_input)
{
  Eigen::Matrix<SCALAR,1,6,OPTIONS> output;

  // Set nominal values
  output = control_input;
  // Add biases
  output.template middleCols<3>(DIM_S::INER_MEAS::GYRO_START_IND)  += truth_state.template middleCols<3>(DIM_S::TRUTH::GYRO_BIAS_START_IND);
  output.template middleCols<3>(DIM_S::INER_MEAS::ACCEL_START_IND) += truth_state.template middleCols<3>(DIM_S::TRUTH::ACCEL_BIAS_START_IND);
  // Add Noise
  output.template middleCols<3>(DIM_S::INER_MEAS::GYRO_START_IND)  += this->gyro_noise-> getContinuousNoise(time_step);
  output.template middleCols<3>(DIM_S::INER_MEAS::ACCEL_START_IND) += this->accel_noise->getContinuousNoise(time_step);

  return output;
}

template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,6,DIM_S::ERROR_DIM,OPTIONS> OpenLoopIMU<DIM_S,SCALAR,OPTIONS>::
  getLinearizedInertialMeasurementMatrix(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& /* ref_nav_state */)
{
  Eigen::Matrix<SCALAR,6,DIM_S::ERROR_DIM,OPTIONS> output;

  output.setZero();
  output.template block<3,3>(DIM_S::INER_MEAS::GYRO_START_IND, DIM_S::ERROR::GYRO_BIAS_START_IND). setIdentity();
  output.template block<3,3>(DIM_S::INER_MEAS::ACCEL_START_IND,DIM_S::ERROR::ACCEL_BIAS_START_IND).setIdentity();

  return output;
}

template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,6,6,OPTIONS> OpenLoopIMU<DIM_S,SCALAR,OPTIONS>::getNoiseCovariance()
{
  Eigen::Matrix<SCALAR,6,6,OPTIONS> output = Eigen::Matrix<SCALAR,6,6,OPTIONS>::Zero();

  output.template block<3,3>(DIM_S::INER_MEAS::GYRO_START_IND, DIM_S::INER_MEAS::GYRO_START_IND)  = this->gyro_noise-> getCovariance();
  output.template block<3,3>(DIM_S::INER_MEAS::ACCEL_START_IND,DIM_S::INER_MEAS::ACCEL_START_IND) = this->accel_noise->getCovariance();

  return output;
}
} // sensors
} // kf

#endif
/* open_loop_imu.hpp */
