/**
 * @File: absolute_pressure.hpp
 * @Date: September 2022
 * @Author: James Swedeen
 *
 * @brief
 * A class for simulating absolute pressure measurement which give you information about altitude.
 **/

#ifndef KALMAN_FILTER_SENSORS_MEASUREMENTS_ABSOLUTE_PRESSURE_HPP
#define KALMAN_FILTER_SENSORS_MEASUREMENTS_ABSOLUTE_PRESSURE_HPP

/* C++ Headers */
#include<cstdint>
#include<memory>

/* Eigen Headers */
#include<Eigen/Dense>

/* ROS Headers */
#include<rclcpp/rclcpp.hpp>

/* Local Headers */
#include<kalman_filter/helpers/dimension_struct.hpp>
#include<kalman_filter/sensors/measurements/measurement_base.hpp>
#include<kalman_filter/noise/noise_base.hpp>
#include<kalman_filter/noise/normal_distribution.hpp>

namespace kf
{
namespace sensors
{
template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
class AbsolutePressure;

template<typename DIM_S, bool ENABLED, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
using AbsolutePressurePtr = std::shared_ptr<AbsolutePressure<DIM_S,ENABLED,SCALAR,OPTIONS>>;

/**
 * @makeAbsolutePressure
 *
 * @brief
 * Helper function that uses information from the ROS parameter server to construct the given object.
 *
 * @templates
 * DIM_S: The type of a Dimensions object or an inheriting object that has information about the size of the state vectors
 * SCALAR: The object type that each dimension will be represented with
 * OPTIONS: Eigen Matrix options
 *
 * @parameters
 * node: The node with the right namespacing to have access to the parameters needed
 * prefix: The prefix of the parameter names
 *
 * @return
 * A fully constructed AbsolutePressure.
 **/
template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
MeasurementBasePtr<1,DIM_S,SCALAR,OPTIONS> makeAbsolutePressure(const rclcpp::Node::SharedPtr& node, const std::string& prefix);

/**
 * @DIM_S
 * The type of a Dimensions object or an inheriting object that has information about the size of the state vectors.
 *
 * @SCALAR
 * The object type that each dimension will be represented with.
 *
 * @ENABLED
 * False if and only if you want this measurement source to be turned off.
 *
 * @OPTIONS
 * Eigen Matrix options.
 **/
template<typename DIM_S, bool ENABLED, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
class AbsolutePressure
 : public MeasurementBase<1,DIM_S,SCALAR,OPTIONS>
{
public:
  /**
   * @Default Constructor
   **/
  AbsolutePressure() = delete;
  /**
   * @Copy Constructor
   **/
  AbsolutePressure(const AbsolutePressure&) = default;
  /**
   * @Move Constructor
   **/
  AbsolutePressure(AbsolutePressure&&) = default;
  /**
   * @Constructor
   *
   * @brief
   * Initializes the object for use.
   *
   * @parameters
   * noise: The additive noise for this measurement
   * measurement_period: The time between this measurement's updates
   * gravity_magnitude: The magnitude of the acceleration from gravity
   * air_density: The density of the air at the flight location
   **/
  AbsolutePressure(const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& noise,
                   const SCALAR                                 measurement_period,
                   const SCALAR                                 gravity_magnitude,
                   const SCALAR                                 air_density);
  /**
   * @Deconstructor
   **/
  ~AbsolutePressure() override = default;
  /**
   * @Assignment Operators
   **/
  AbsolutePressure& operator=(const AbsolutePressure&)  = default;
  AbsolutePressure& operator=(      AbsolutePressure&&) = default;
  /**
   * @getMeasurement
   *
   * @brief
   * Used to synthesize a measurement.
   *
   * @parameters
   * truth_state: The current truth state vector
   *
   * @return
   * The vector of measurements.
   **/
  inline Eigen::Matrix<SCALAR,1,1,OPTIONS>
    getMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state) override;
  /**
   * @estimateMeasurement
   *
   * @brief
   * Used to estimate a measurement given the navigation state.
   *
   * @parameters
   * nav_state: The current navigation state vector
   *
   * @return
   * The vector of measurements.
   **/
  inline Eigen::Matrix<SCALAR,1,1,OPTIONS>
    estimateMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state) override;
  /**
   * @getMeasurementMatrixNav
   *
   * @brief
   * Finds the linearized form of "estimateMeasurement" around the error state.
   *
   * @parameters
   * nav_state: The current navigation state vector
   *
   * @return
   * The measurement matrix.
   **/
  inline Eigen::Matrix<SCALAR,1,DIM_S::ERROR_DIM,OPTIONS>
    getMeasurementMatrixNav(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state) override;
  /**
   * @getMeasurementMatrixTruth
   *
   * @brief
   * Finds the linearized form of "getMeasurement" around the error state.
   *
   * @parameters
   * truth_state: The current truth state vector
   *
   * @return
   * The measurement matrix.
   **/
  inline Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DISP_DIM,OPTIONS>
    getMeasurementMatrixTruth(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state) override;
  /**
   * @getNoiseCovariance
   *
   * @brief
   * Gets the covariance matrix of the additive noise.
   *
   * @return
   * The covariance matrix of the additive noise.
   **/
  inline Eigen::Matrix<SCALAR,1,1,OPTIONS> getNoiseCovariance() override;
  /**
   * @applyMeasurement
   *
   * @brief
   * Returns true if and only if it is time to use a measurement.
   *
   * @parameters
   * time: The current simulation time
   * next_measurement_time: The time that the next measurement will be ready to use
   * truth_state: The current truth state vector
   *
   * @return
   * True if and only if it is time to use a measurement.
   **/
  inline bool applyMeasurement(const SCALAR                                                              time,
                               const SCALAR                                                              next_measurement_time,
                               const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state) override;
  /**
   * @updateNextMeasurementTime
   *
   * @brief
   * Finds the next time that a measurement should be applied.
   *
   * @parameters
   * time: The current simulation time
   * next_measurement_time: The old time that the next measurement will be ready to use
   *
   * @return
   * The new time that the next measurement will be ready to use
   **/
  inline SCALAR updateNextMeasurementTime(const SCALAR time, const SCALAR next_measurement_time) override;
private:
  noise::NoiseBasePtr<1,SCALAR,OPTIONS> noise;
  SCALAR                                measurement_period;
  SCALAR                                gravity_magnitude;
  SCALAR                                air_density;
};

template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
MeasurementBasePtr<1,DIM_S,SCALAR,OPTIONS>
  makeAbsolutePressure(const rclcpp::Node::SharedPtr& node, const std::string& prefix)
{
  node->declare_parameter(prefix + ".enabled",            rclcpp::PARAMETER_BOOL);
  node->declare_parameter(prefix + ".measurement_period", rclcpp::PARAMETER_DOUBLE);
  node->declare_parameter(prefix + ".gravity_magnitude",  rclcpp::PARAMETER_DOUBLE);
  node->declare_parameter(prefix + ".air_density",        rclcpp::PARAMETER_DOUBLE);

  const bool   enabled            = node->get_parameter(prefix + ".enabled").as_bool();
  const SCALAR measurement_period = node->get_parameter(prefix + ".measurement_period").as_double();
  const SCALAR gravity_magnitude  = node->get_parameter(prefix + ".gravity_magnitude").as_double();
  const SCALAR air_density        = node->get_parameter(prefix + ".air_density").as_double();

  if(enabled)
  {
    return std::make_shared<AbsolutePressure<DIM_S,true,SCALAR,OPTIONS>>(
             noise::makeNormalDistribution<1,SCALAR,OPTIONS>(node, prefix + ".noise"),
             measurement_period,
             gravity_magnitude,
             air_density);
  }
  else // Not enabled
  {
    return std::make_shared<AbsolutePressure<DIM_S,false,SCALAR,OPTIONS>>(
             noise::makeNormalDistribution<1,SCALAR,OPTIONS>(node, prefix + ".noise"),
             measurement_period,
             gravity_magnitude,
             air_density);
  }
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
AbsolutePressure<DIM_S,ENABLED,SCALAR,OPTIONS>::
  AbsolutePressure(const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& noise,
                   const SCALAR                                 measurement_period,
                   const SCALAR                                 gravity_magnitude,
                   const SCALAR                                 air_density)
 : MeasurementBase<1,DIM_S,SCALAR,OPTIONS>(),
   noise(noise),
   measurement_period(measurement_period),
   gravity_magnitude(gravity_magnitude),
   air_density(air_density)
{}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,1,OPTIONS> AbsolutePressure<DIM_S,ENABLED,SCALAR,OPTIONS>::
  getMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state)
{
  return (-truth_state.template middleCols<1>(DIM_S::TRUTH::DOWN_IND) * this->gravity_magnitude * this->air_density) +
         truth_state.template middleCols<1>(DIM_S::TRUTH::ABS_PRESSURE_BIAS_IND) +
         this->noise->getNoise();
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,1,OPTIONS> AbsolutePressure<DIM_S,ENABLED,SCALAR,OPTIONS>::
  estimateMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state)
{
  return (-nav_state.template middleCols<1>(DIM_S::NAV::DOWN_IND) * this->gravity_magnitude * this->air_density) +
         nav_state.template middleCols<1>(DIM_S::NAV::ABS_PRESSURE_BIAS_IND);
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,DIM_S::ERROR_DIM,OPTIONS> AbsolutePressure<DIM_S,ENABLED,SCALAR,OPTIONS>::
  getMeasurementMatrixNav(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& /* nav_state */)
{
  Eigen::Matrix<SCALAR,1,DIM_S::ERROR_DIM,OPTIONS> output = Eigen::Matrix<SCALAR,1,DIM_S::ERROR_DIM,OPTIONS>::Zero();

  output[DIM_S::ERROR::DOWN_IND]              = -this->gravity_magnitude * this->air_density;
  output[DIM_S::ERROR::ABS_PRESSURE_BIAS_IND] = 1;

  return output;
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DISP_DIM,OPTIONS> AbsolutePressure<DIM_S,ENABLED,SCALAR,OPTIONS>::
  getMeasurementMatrixTruth(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& /* truth_state */)
{
  Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DISP_DIM,OPTIONS> output =
    Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DISP_DIM,OPTIONS>::Zero();

  output[DIM_S::TRUTH_DISP::DOWN_IND]         = -this->gravity_magnitude * this->air_density;
  output[DIM_S::TRUTH::ABS_PRESSURE_BIAS_IND] = 1;

  return output;
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,1,OPTIONS> AbsolutePressure<DIM_S,ENABLED,SCALAR,OPTIONS>::getNoiseCovariance()
{
  return this->noise->getCovariance();
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline bool AbsolutePressure<DIM_S,ENABLED,SCALAR,OPTIONS>::
  applyMeasurement(const SCALAR                                                              time,
                   const SCALAR                                                              next_measurement_time,
                   const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& /* truth_state */)
{
  if constexpr(ENABLED)
  {
    if(time >= next_measurement_time)
    {
      return true;
    }
  }
  return false;
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline SCALAR AbsolutePressure<DIM_S,ENABLED,SCALAR,OPTIONS>::
  updateNextMeasurementTime(const SCALAR time, const SCALAR next_measurement_time)
{
  if(time >= next_measurement_time)
  {
    return next_measurement_time + this->measurement_period;
  }
  return next_measurement_time;
}
} // sensors
} // kf

#endif
/* absolute_pressure.hpp */
