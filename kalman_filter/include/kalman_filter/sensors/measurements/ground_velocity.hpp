/**
 * @File: ground_velocity.hpp
 * @Date: September 2022
 * @Author: James Swedeen
 *
 * @brief
 * A class for simulating ground velocity measurements.
 **/

#ifndef KALMAN_FILTER_SENSORS_MEASUREMENTS_GROUND_VELOCITY_HPP
#define KALMAN_FILTER_SENSORS_MEASUREMENTS_GROUND_VELOCITY_HPP

/* C++ Headers */
#include<cstdint>
#include<memory>

/* Eigen Headers */
#include<Eigen/Dense>

/* ROS Headers */
#include<rclcpp/rclcpp.hpp>

/* Local Headers */
#include<kalman_filter/sensors/measurements/measurement_base.hpp>
#include<kalman_filter/noise/noise_base.hpp>
#include<kalman_filter/noise/normal_distribution.hpp>

namespace kf
{
namespace sensors
{
template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
class GroundVelocity;

template<typename DIM_S, bool ENABLED, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
using GroundVelocityPtr = std::shared_ptr<GroundVelocity<DIM_S,ENABLED,SCALAR,OPTIONS>>;

/**
 * @makeGroundVelocity
 *
 * @brief
 * Helper function that uses information from the ROS parameter server to construct the given object.
 *
 * @templates
 * DIM_S: The type of a Dimensions object or an inheriting object that has information about the size of the state vectors
 * SCALAR: The object type that each dimension will be represented with
 * OPTIONS: Eigen Matrix options
 *
 * @parameters
 * node: The node with the right namespacing to have access to the parameters needed
 * prefix: The prefix of the parameter names
 *
 * @return
 * A fully constructed GroundVelocity.
 **/
template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
MeasurementBasePtr<1,DIM_S,SCALAR,OPTIONS> makeGroundVelocity(const rclcpp::Node::SharedPtr& node, const std::string& prefix);

/**
 * @DIM_S
 * The type of a Dimensions object or an inheriting object that has information about the size of the state vectors.
 *
 * @SCALAR
 * The object type that each dimension will be represented with.
 *
 * @ENABLED
 * False if and only if you want this measurement source to be turned off.
 *
 * @OPTIONS
 * Eigen Matrix options.
 **/
template<typename DIM_S, bool ENABLED, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
class GroundVelocity
 : public MeasurementBase<1,DIM_S,SCALAR,OPTIONS>
{
public:
  /**
   * @Default Constructor
   **/
  GroundVelocity() = delete;
  /**
   * @Copy Constructor
   **/
  GroundVelocity(const GroundVelocity&) = default;
  /**
   * @Move Constructor
   **/
  GroundVelocity(GroundVelocity&&) = default;
  /**
   * @Constructor
   *
   * @brief
   * Initializes the object for use.
   *
   * @parameters
   * noise: The additive noise for this measurement
   * measurement_period: The time between this measurement's updates
   **/
  GroundVelocity(const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& noise,
                 const SCALAR                                 measurement_period);
  /**
   * @Deconstructor
   **/
  ~GroundVelocity() override = default;
  /**
   * @Assignment Operators
   **/
  GroundVelocity& operator=(const GroundVelocity&)  = default;
  GroundVelocity& operator=(      GroundVelocity&&) = default;
  /**
   * @getMeasurement
   *
   * @brief
   * Used to synthesize a measurement.
   *
   * @parameters
   * truth_state: The current truth state vector
   *
   * @return
   * The vector of measurements.
   **/
  inline Eigen::Matrix<SCALAR,1,1,OPTIONS>
    getMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state) override;
  /**
   * @estimateMeasurement
   *
   * @brief
   * Used to estimate a measurement given the navigation state.
   *
   * @parameters
   * nav_state: The current navigation state vector
   *
   * @return
   * The vector of measurements.
   **/
  inline Eigen::Matrix<SCALAR,1,1,OPTIONS>
    estimateMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state) override;
  /**
   * @getMeasurementMatrixNav
   *
   * @brief
   * Finds the linearized form of "estimateMeasurement" around the error state.
   *
   * @parameters
   * nav_state: The current navigation state vector
   *
   * @return
   * The measurement matrix.
   **/
  inline Eigen::Matrix<SCALAR,1,DIM_S::ERROR_DIM,OPTIONS>
    getMeasurementMatrixNav(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state) override;
  /**
   * @getMeasurementMatrixTruth
   *
   * @brief
   * Finds the linearized form of "getMeasurement" around the error state.
   *
   * @parameters
   * truth_state: The current truth state vector
   *
   * @return
   * The measurement matrix.
   **/
  inline Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DISP_DIM,OPTIONS>
    getMeasurementMatrixTruth(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state) override;
  /**
   * @getNoiseCovariance
   *
   * @brief
   * Gets the covariance matrix of the additive noise.
   *
   * @return
   * The covariance matrix of the additive noise.
   **/
  inline Eigen::Matrix<SCALAR,1,1,OPTIONS> getNoiseCovariance() override;
  /**
   * @applyMeasurement
   *
   * @brief
   * Returns true if and only if it is time to use a measurement.
   *
   * @parameters
   * time: The current simulation time
   * next_measurement_time: The time that the next measurement will be ready to use
   * truth_state: The current truth state vector
   *
   * @return
   * True if and only if it is time to use a measurement.
   **/
  inline bool applyMeasurement(const SCALAR                                                              time,
                               const SCALAR                                                              next_measurement_time,
                               const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state) override;
  /**
   * @updateNextMeasurementTime
   *
   * @brief
   * Finds the next time that a measurement should be applied.
   *
   * @parameters
   * time: The current simulation time
   * next_measurement_time: The old time that the next measurement will be ready to use
   *
   * @return
   * The new time that the next measurement will be ready to use
   **/
  inline SCALAR updateNextMeasurementTime(const SCALAR time, const SCALAR next_measurement_time) override;
private:
  noise::NoiseBasePtr<1,SCALAR,OPTIONS> noise;
  SCALAR                                measurement_period;
};

template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
MeasurementBasePtr<1,DIM_S,SCALAR,OPTIONS> makeGroundVelocity(const rclcpp::Node::SharedPtr& node, const std::string& prefix)
{
  node->declare_parameter(prefix + ".enabled",            rclcpp::PARAMETER_BOOL);
  node->declare_parameter(prefix + ".measurement_period", rclcpp::PARAMETER_DOUBLE);

  const bool   enabled            = node->get_parameter(prefix + ".enabled").as_bool();
  const SCALAR measurement_period = node->get_parameter(prefix + ".measurement_period").as_double();

  if(enabled)
  {
    return std::make_shared<GroundVelocity<DIM_S,true,SCALAR,OPTIONS>>(
             noise::makeNormalDistribution<1,SCALAR,OPTIONS>(node, prefix + ".noise"),
             measurement_period);
  }
  else // Not enabled
  {
    return std::make_shared<GroundVelocity<DIM_S,false,SCALAR,OPTIONS>>(
             noise::makeNormalDistribution<1,SCALAR,OPTIONS>(node, prefix + ".noise"),
             measurement_period);
  }
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
GroundVelocity<DIM_S,ENABLED,SCALAR,OPTIONS>::
  GroundVelocity(const noise::NoiseBasePtr<1,SCALAR,OPTIONS>& noise,
          const SCALAR                                 measurement_period)
 : MeasurementBase<1,DIM_S,SCALAR,OPTIONS>(),
   noise(noise),
   measurement_period(measurement_period)
{}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,1,OPTIONS> GroundVelocity<DIM_S,ENABLED,SCALAR,OPTIONS>::
  getMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state)
{
  return Eigen::Matrix<SCALAR,1,1,OPTIONS>(truth_state.template middleCols<2>(DIM_S::TRUTH::NORTH_VEL_IND).norm()) +
         this->noise->getNoise();
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,1,OPTIONS> GroundVelocity<DIM_S,ENABLED,SCALAR,OPTIONS>::
  estimateMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state)
{
  return Eigen::Matrix<SCALAR,1,1,OPTIONS>(nav_state.template middleCols<2>(DIM_S::NAV::NORTH_VEL_IND).norm());
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,DIM_S::ERROR_DIM,OPTIONS> GroundVelocity<DIM_S,ENABLED,SCALAR,OPTIONS>::
  getMeasurementMatrixNav(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state)
{
  Eigen::Matrix<SCALAR,1,DIM_S::ERROR_DIM,OPTIONS> output = Eigen::Matrix<SCALAR,1,DIM_S::ERROR_DIM,OPTIONS>::Zero();

  output.template middleCols<2>(DIM_S::ERROR::NORTH_VEL_IND) =
    nav_state.template middleCols<2>(DIM_S::NAV::NORTH_VEL_IND).array() / nav_state.template middleCols<2>(DIM_S::NAV::NORTH_VEL_IND).norm();

  return output;
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DISP_DIM,OPTIONS> GroundVelocity<DIM_S,ENABLED,SCALAR,OPTIONS>::
  getMeasurementMatrixTruth(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state)
{
  Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DISP_DIM,OPTIONS> output = Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DISP_DIM,OPTIONS>::Zero();

  output.template middleCols<2>(DIM_S::TRUTH_DISP::NORTH_VEL_IND) =
    truth_state.template middleCols<2>(DIM_S::TRUTH::NORTH_VEL_IND).array() / truth_state.template middleCols<2>(DIM_S::TRUTH::NORTH_VEL_IND).norm();

  return output;
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,1,OPTIONS> GroundVelocity<DIM_S,ENABLED,SCALAR,OPTIONS>::getNoiseCovariance()
{
  return this->noise->getCovariance();
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline bool GroundVelocity<DIM_S,ENABLED,SCALAR,OPTIONS>::
  applyMeasurement(const SCALAR                                                              time,
                   const SCALAR                                                              next_measurement_time,
                   const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& /* truth_state */)
{
  if constexpr(ENABLED)
  {
    if(time >= next_measurement_time)
    {
      return true;
    }
  }
  return false;
}

template<typename DIM_S, bool ENABLED, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline SCALAR GroundVelocity<DIM_S,ENABLED,SCALAR,OPTIONS>::
  updateNextMeasurementTime(const SCALAR time, const SCALAR next_measurement_time)
{
  if(time >= next_measurement_time)
  {
    return next_measurement_time + this->measurement_period;
  }
  return next_measurement_time;
}
} // sensors
} // kf

#endif
/* ground_velocity.hpp */
