/**
 * @File: measurement_base.hpp
 * @Date: April 2022
 * @Author: James Swedeen
 *
 * @brief
 * A base class for non-inertial measurements.
 **/

#ifndef KALMAN_FILTER_SENSORS_MEASUREMENTS_MEASUREMENT_BASE_HPP
#define KALMAN_FILTER_SENSORS_MEASUREMENTS_MEASUREMENT_BASE_HPP

/* C++ Headers */
#include<cstdint>
#include<memory>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */

namespace kf
{
namespace sensors
{
template<Eigen::Index DIM, typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
class MeasurementBase;

template<Eigen::Index DIM, typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
using MeasurementBasePtr = std::shared_ptr<MeasurementBase<DIM,DIM_S,SCALAR,OPTIONS>>;

/**
 * @DIM
 * The size of the actual measurement values.
 *
 * @DIM_S
 * The type of a Dimensions object or an inheriting object that has information about the size of the state vectors.
 *
 * @SCALAR
 * The object type that each dimension will be represented with.
 *
 * @OPTIONS
 * Eigen Matrix options.
 **/
template<Eigen::Index DIM, typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
class MeasurementBase
{
public:
  /**
   * @Default Constructor
   **/
  MeasurementBase() = default;
  /**
   * @Copy Constructor
   **/
  MeasurementBase(const MeasurementBase&) = default;
  /**
   * @Move Constructor
   **/
  MeasurementBase(MeasurementBase&&) = default;
  /**
   * @Deconstructor
   **/
  virtual ~MeasurementBase() = default;
  /**
   * @Assignment Operators
   **/
  MeasurementBase& operator=(const MeasurementBase&)  = default;
  MeasurementBase& operator=(      MeasurementBase&&) = default;
  /**
   * @getMeasurement
   *
   * @brief
   * Used to synthesize a measurement.
   *
   * @parameters
   * truth_state: The current truth state vector
   *
   * @return
   * The vector of measurements.
   **/
  inline virtual Eigen::Matrix<SCALAR,1,DIM,OPTIONS>
    getMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state) = 0;
  /**
   * @estimateMeasurement
   *
   * @brief
   * Used to estimate a measurement given the navigation state.
   *
   * @parameters
   * nav_state: The current navigation state vector
   *
   * @return
   * The vector of measurements.
   **/
  inline virtual Eigen::Matrix<SCALAR,1,DIM,OPTIONS>
    estimateMeasurement(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state) = 0;
  /**
   * @getMeasurementMatrixNav
   *
   * @brief
   * Finds the linearized form of "estimateMeasurement" around the error state.
   *
   * @parameters
   * nav_state: The current navigation state vector
   *
   * @return
   * The measurement matrix.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM,DIM_S::ERROR_DIM,OPTIONS>
    getMeasurementMatrixNav(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state) = 0;
  /**
   * @getMeasurementMatrixTruth
   *
   * @brief
   * Finds the linearized form of "getMeasurement" around the error state.
   *
   * @parameters
   * truth_state: The current truth state vector
   *
   * @return
   * The measurement matrix.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM,DIM_S::TRUTH_DISP_DIM,OPTIONS>
    getMeasurementMatrixTruth(const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state) = 0;
  /**
   * @getNoiseCovariance
   *
   * @brief
   * Gets the covariance matrix of the additive noise.
   *
   * @return
   * The covariance matrix of the additive noise.
   **/
  inline virtual Eigen::Matrix<SCALAR,DIM,DIM,OPTIONS> getNoiseCovariance() = 0;
  /**
   * @applyMeasurement
   *
   * @brief
   * Returns true if and only if it is time to use a measurement.
   *
   * @parameters
   * time: The current simulation time
   * next_measurement_time: The time that the next measurement will be ready to use
   * truth_state: The current truth state vector
   *
   * @return
   * True if and only if it is time to use a measurement.
   **/
  inline virtual bool applyMeasurement(const SCALAR                                                              time,
                                       const SCALAR                                                              next_measurement_time,
                                       const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS>>& truth_state) = 0;
  /**
   * @updateNextMeasurementTime
   *
   * @brief
   * Finds the next time that a measurement should be applied.
   *
   * @parameters
   * time: The current simulation time
   * next_measurement_time: The old time that the next measurement will be ready to use
   *
   * @return
   * The new time that the next measurement will be ready to use
   **/
  inline virtual SCALAR updateNextMeasurementTime(const SCALAR time, const SCALAR next_measurement_time) = 0;
};
} // sensors
} // kf

#endif
/* measurement_base.hpp */
