from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
import os
from enum import IntEnum
import multiprocessing

class PlannerConfig(IntEnum):
    NULL_PLANNECONFIG = 0
    RRT = 1
    RRT_STAR = 2
    RRT_STAR_SMART = 3
    BIT = 4

def generate_launch_description():
    # Create the package directory
    uav_launch_dir = get_package_share_directory('uav_launch')

    # Define the urdf file for visualizing the uav
    urdf_file_name = 'fixed_wing_uav.urdf'
    urdf = os.path.join(
        get_package_share_directory('uav_launch'),
        'urdf',
        urdf_file_name)

    # Flag for enabling/disabling use of simulation time instead of wall clock
    use_sim_time = True

    min_north = -9700.0
    min_east  = -4650.0
    north_len = abs(min_north) + 1000.0
    east_len  = abs(min_east)  + 1000.0
    nominal_altitude = 10.0

    return LaunchDescription([

        ################ Dynamics and kinematics #####################################
        Node(
            package='uav_launch',
            executable='transforms',
            name='transforms',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            arguments=[urdf],
            parameters=[{
                'use_sim_time': use_sim_time
            }]),
        # Dynamics combined with autopilot
        Node(
            package='uav_launch',
            executable='ch06_autopilot_dynamics',
            name='autopilot_dynamic_mav',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch04_wind',
            name='wind',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch07_sensors',
            name="sensors",
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        ################# UAV Control Software #######################################
        # State Estimator
        Node(
            package='topic_tools',
            executable='relay',
            name="state_estimator_relay",
            parameters=[
                {"input_topic": "uav_state"},
                {"output_topic": "uav_state_estimate"},
                {"use_sim_time": use_sim_time}
            ]
        ),

        # Path follower
        Node(
            package='uav_launch',
            executable='ch10_path_follower',
            name='path_follower',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        # Path follower
        Node(
            package='uav_launch',
            executable='ch11_path_manager',
            name='path_manager',
            #emulate_tty=True,
            #output="screen",
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        # Path planner node
        Node(
            package='rrt_search',
            executable='waypoint_path_planner',
            name='path_planner',
            output="screen",
            #prefix=['xterm -e gdb -ex run --args'],
            parameters=[{
                'use_sim_time': use_sim_time,
                'planner_type': PlannerConfig.BIT.value,
                'batch_size': 1500,
                'num_target_samples': 1,
                'turning_radius': 200.0,
                'nominal_altitude': nominal_altitude,
                'nominal_airspeed': 25.0,
                'uav_state_topic': 'uav_state_estimate',
                'goal_pose_topic': '/wp_panel/goal_pose',
                'waypoints_topic': 'waypoints',
                'replan_topic': 'replan',
                'occupancy_grid':{
                    'line_width': -1.0,
                    'resolution': 1.0,
                    'origin': [min_north, min_east],
                    'width': north_len,
                    'height': east_len,
                    'nominal_altitude': nominal_altitude,
                    'building_data_file': os.path.join(get_package_share_directory('occupancy_grid'), 'config', 'new_york_buildings_manhattan.csv'),
                    },
                'sampler':{
                    'target_radius': 1.0,
                    'check_target_ratio': 100,
                    },
                'nns':{
                    'leaf_size': 1000,
                    'num_threads': multiprocessing.cpu_count(),
                    },
                'steer':{
                    'near_radius': 1000.0,
                    },
                'problem':{
                    'target_radius': 1.0,
                    'max_iteration': 0,
                    'max_duration': 30.0,
                    'target_cost': 100000.0,
                    },
                'edge':{
                    'resolution': 1.0,
                    },
            }]
        ),
        ## Occupancy Grid Plotter
        ## TODO: Remove when not needed
        Node(
            package='occupancy_grid',
            executable='building_occupancy_grid_node',
            name='occupancy_grid_pub',
            output='screen',
            #prefix=['xterm -e gdb -ex run --args'],
            parameters=[{
                'use_sim_time': use_sim_time,
                'occupancy_grid':{
                    'line_width': -1.0,
                    'resolution': 1.0,
                    'origin': [min_north, min_east],
                    'width': north_len,
                    'height': east_len,
                    'nominal_altitude': nominal_altitude,
                    'building_data_file': os.path.join(get_package_share_directory('occupancy_grid'), 'config', 'new_york_buildings_manhattan.csv'),
                    },
                }],
            ),
        # World manager
        Node(
            package='occupancy_grid',
            executable='data_world_plotter_node.py',
            name='world_plotter',
            parameters=[{
                'use_sim_time': use_sim_time,
                'ts': 1.0,
                'min_north': min_north,
                'min_east': min_east,
                'max_north': min_north + north_len,
                'max_east': min_east + east_len,
                'data_file': os.path.join(get_package_share_directory('occupancy_grid'), 'config', 'new_york_buildings_manhattan.csv'),
            }]
        ),

        ################# Tools for Interacting with the Sim #########################
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            emulate_tty=True,
            output="screen",
            arguments=['-d', [os.path.join(get_package_share_directory('occupancy_grid'), 'rviz', 'manhattan_path_planner.rviz')]],
            parameters=[{
                'use_sim_time': False
            }]
        ),

        Node(
            package='rqt_robot_monitor',
            executable='rqt_robot_monitor',
            name='rqt_robot_monitor',
            parameters=[{
                'use_sim_time': False
            }]
        ),

        Node(
            package='uav_launch',
            executable='diagnotic_aggregator',
            name='diagnotic_aggregator',
            parameters=[{
                'use_sim_time': False
            }]
        ),

        Node(
            package='uav_launch',
            executable='uav_plotter',
            name='uav_plotter',
            parameters=[{
                'use_sim_time': True,
                't_horizon': 100.,
                'plot_sensors': True
            }]
        )
    ])
