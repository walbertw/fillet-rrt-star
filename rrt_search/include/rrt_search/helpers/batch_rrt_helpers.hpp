/**
 * @File: batch_rrt_helpers.hpp
 * @Date: June 2022
 * @Author: James Swedeen
 *
 * @brief
 * Header that defines some needed functions in order to use Batch RRT based algorithms.
 **/

#ifndef RRT_SEARCH_HELPERS_BATCH_RRT_HELPERS_HPP
#define RRT_SEARCH_HELPERS_BATCH_RRT_HELPERS_HPP

/* C++ Headers */
#include<cstdint>
#include<memory>
#include<list>
#include<vector>
#include<algorithm>
#include<execution>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */
#include<rrt_search/helpers/batch_queues.hpp>
#include<rrt_search/helpers/rrt_versions.hpp>
#include<rrt_search/tree/node.hpp>
#include<rrt_search/tree/rrt_tree.hpp>
#include<rrt_search/steering_functions/steering_function.hpp>
#include<rrt_search/obstacle_checkers/obstacle_checker.hpp>
#include<rrt_search/cost_functions/fillet_cost_function.hpp>
#include<rrt_search/helpers/fillet_cost_function.hpp>

namespace rrt
{
namespace search
{
namespace batch
{
/**
 * @expandNextVertex
 *
 * @brief
 * Used to process the next promising vertex into edges.
 *
 * @templates
 * DIM: The number of dimensions the problem has in total
 * SCALAR: The object type that each dimension will be represented with
 * VERSION: Specifies different variations on the RRT algorithm
 * OPTIONS: Eigen Matrix options
 *
 * @parameters
 * queues: The current queues of vertices and edges
 * tree: The search tree
 * search_radius: The nearest neighbor search radius
 * cost_function: Helper function used to calculate the cost of edges
 **/
template<Eigen::Index DIM, RRTVersions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void expandNextVertex(      QueueHolder<DIM,false,SCALAR,OPTIONS>&     queues,
                                   tree::RRTTree<DIM,VERSION,SCALAR,OPTIONS>& tree,
                             const SCALAR                                     search_radius,
                             const cost::CostFunctionPtr<DIM,SCALAR,OPTIONS>& cost_function);
/**
 * @expandNextVertexFillet
 *
 * @brief
 * Used to process the next promising vertex into edges in FB-BIT*.
 *
 * @templates
 * DIM: The number of dimensions the problem has in total
 * SCALAR: The object type that each dimension will be represented with
 * VERSION: Specifies different variations on the RRT algorithm
 * OPTIONS: Eigen Matrix options
 *
 * @parameters
 * queues: The current queues of vertices and edges
 * tree: The search tree
 * search_radius: The nearest neighbor search radius
 * edge_generator: Used to make valid edges
 * cost_function: Helper function used to calculate the cost of edges
 **/
template<Eigen::Index DIM, RRTVersions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void expandNextVertexFillet(      QueueHolder<DIM,true,SCALAR,OPTIONS>&             queues,
                                         tree::RRTTree<DIM,VERSION,SCALAR,OPTIONS>&        tree,
                                   const SCALAR                                            search_radius,
                                   const edge::FilletEdgeGeneratorPtr<DIM,SCALAR,OPTIONS>& edge_generator,
                                   const cost::FilletCostFunctionPtr< DIM,SCALAR,OPTIONS>& cost_function);
/**
 * @expandNextEdge
 *
 * @brief
 * Used to process the next promising edge into a tree branch.
 *
 * @templates
 * DIM: The number of dimensions the problem has in total
 * SCALAR: The object type that each dimension will be represented with
 * VERSION: Specifies different variations on the RRT algorithm
 * OPTIONS: Eigen Matrix options
 *
 * @parameters
 * queues: The current queues of vertices and edges
 * tree: The search tree
 * edge_generator: Used to make valid edges
 * obstacle_checker: Used to check for obstacles
 * cost_function: Helper function used to calculate the cost of edges
 **/
template<Eigen::Index DIM, RRTVersions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void expandNextEdge(      QueueHolder<            DIM,false,  SCALAR,OPTIONS>& queues,
                                 tree::RRTTree<          DIM,VERSION,SCALAR,OPTIONS>& tree,
                           const edge::EdgeGeneratorPtr< DIM,        SCALAR,OPTIONS>& edge_generator,
                           const obs::ObstacleCheckerPtr<DIM,        SCALAR,OPTIONS>& obstacle_checker,
                           const cost::CostFunctionPtr<  DIM,        SCALAR,OPTIONS>& cost_function);
/**
 * @expandNextEdgeFillet
 *
 * @brief
 * Used to process the next promising edge into a tree branch for FB-BIT*.
 *
 * @templates
 * DIM: The number of dimensions the problem has in total
 * SCALAR: The object type that each dimension will be represented with
 * VERSION: Specifies different variations on the RRT algorithm
 * OPTIONS: Eigen Matrix options
 *
 * @parameters
 * queues: The current queues of vertices and edges
 * tree: The search tree
 * edge_generator: Used to make valid edges
 * obstacle_checker: Used to check for obstacles
 * cost_function: Helper function used to calculate the cost of edges
 **/
template<Eigen::Index DIM, RRTVersions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void expandNextEdgeFillet(      QueueHolder<                 DIM,true,   SCALAR,OPTIONS>& queues,
                                       tree::RRTTree<               DIM,VERSION,SCALAR,OPTIONS>& tree,
                                 const edge::FilletEdgeGeneratorPtr<DIM,        SCALAR,OPTIONS>& edge_generator,
                                 const obs::ObstacleCheckerPtr<     DIM,        SCALAR,OPTIONS>& obstacle_checker,
                                 const cost::FilletCostFunctionPtr< DIM,        SCALAR,OPTIONS>& cost_function);
} // batch


template<Eigen::Index DIM, RRTVersions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void batch::expandNextVertex(      QueueHolder<DIM,false,SCALAR,OPTIONS>&     queues,
                                          tree::RRTTree<DIM,VERSION,SCALAR,OPTIONS>& tree,
                                    const SCALAR                                     search_radius,
                                    const cost::CostFunctionPtr<DIM,SCALAR,OPTIONS>& cost_function)
{
  std::vector<Vertex<DIM,SCALAR,OPTIONS>*> near_unconnected_verts;
  const SCALAR                             cur_sol_cost = queues.cgetSolutionCost();

  // Pop the best vertex in the queue
  Vertex<DIM,SCALAR,OPTIONS>* best_vertex             = queues.popBestVertex();
  const bool                  perform_rewiring_checks = best_vertex->rewireDelayed() and queues.hasSolution();
  // Add all promising edges
  if(best_vertex->unexpanded())
  {
    queues.reserveExtraEdgeSpace(queues.numberUnconnectedVertices() + tree.size());
    // Find near unconnected set
    near_unconnected_verts = queues.nearUnconnectedVertices(best_vertex, search_radius);
    best_vertex->setExpanded();
  }
  else // Has been expanded
  {
    queues.reserveExtraEdgeSpace(queues.numberUnconnectedVertices() + ((perform_rewiring_checks) ? tree.size() : 0));
    // Find near new unconnected set
    near_unconnected_verts = queues.nearNewUnconnectedVertices(best_vertex, search_radius);
  }
  // Add edges to unconnected vertices
  const auto near_unconnected_verts_end = near_unconnected_verts.end();
  for(auto near_it = near_unconnected_verts.begin(); near_it < near_unconnected_verts_end; ++near_it)
  {
    const SCALAR edge_cost_est = cost_function->costEstimate(best_vertex->cgetNode()->cgetPoint(),
                                                             (*near_it)->             cgetPoint());
    // If the edge has the potential to help the solution add it to the edge queue
    if((best_vertex->cgetCostToComeEst() + edge_cost_est + (*near_it)->cgetCostToGoEst()) < cur_sol_cost)
    {
      queues.addEdge(best_vertex, *near_it, edge_cost_est, (*near_it)->cgetCostToGoEst());
    }
  }
  // Add edges to connected vertices
  if(perform_rewiring_checks)
  {
    const std::vector<tree::Node<DIM,SCALAR,OPTIONS>*> neighbors =
      tree.findInRadius(best_vertex->cgetNode()->cgetPoint(), search_radius, std::numeric_limits<size_t>::max(), false);
    const size_t num_neighbors = neighbors.size();
    for(size_t neighbor_it = 0; neighbor_it < num_neighbors; ++neighbor_it)
    {
      // Check that the edge isn't already in the tree
      if(best_vertex->cgetNode() != neighbors[neighbor_it]->cgetParent())
      {
        const SCALAR edge_cost_est = cost_function->costEstimate(best_vertex->cgetNode()->cgetPoint(),
                                                                 neighbors[neighbor_it]-> cgetPoint());
        const SCALAR est_new_cost_node = best_vertex->cgetCostToComeEst() + edge_cost_est;
        // If this edge can improve the cost of the neighbor
        if(est_new_cost_node < neighbors[neighbor_it]->cgetCost())
        {
          Vertex<DIM,SCALAR,OPTIONS>* neighbor_vertex = queues.getConnectedVertex(neighbors[neighbor_it]->cgetIndex());
          // If this edge can improve the solution
          if((est_new_cost_node + neighbor_vertex->cgetCostToGoEst()) < cur_sol_cost)
          {
            queues.addEdge(best_vertex, neighbor_vertex, edge_cost_est, neighbor_vertex->cgetCostToGoEst());
          }
        }
      }
    }
    best_vertex->setRewired();
  }
}

template<Eigen::Index DIM, RRTVersions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void batch::expandNextVertexFillet(      QueueHolder<DIM,true,SCALAR,OPTIONS>&             queues,
                                                tree::RRTTree<DIM,VERSION,SCALAR,OPTIONS>&        tree,
                                          const SCALAR                                            search_radius,
                                          const edge::FilletEdgeGeneratorPtr<DIM,SCALAR,OPTIONS>& edge_generator,
                                          const cost::FilletCostFunctionPtr< DIM,SCALAR,OPTIONS>& cost_function)
{
  std::vector<Vertex<DIM,SCALAR,OPTIONS>*>                    near_unconnected_verts;
  const SCALAR                                                cur_sol_cost = queues.cgetSolutionCost();
  const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM,OPTIONS>> cur_target   = queues.cgetTarget();

  // Pop the best vertex in the queue
  Vertex<DIM,SCALAR,OPTIONS>* best_vertex             = queues.popBestVertex();
  const bool                  perform_rewiring_checks = best_vertex->rewireDelayed() and queues.hasSolution();
  // Add all promising edges
  if(best_vertex->unexpanded())
  {
    queues.reserveExtraEdgeSpace(queues.numberUnconnectedVertices() + tree.size());
    // Find near unconnected set
    near_unconnected_verts = queues.nearUnconnectedVertices(best_vertex, search_radius);
    best_vertex->setExpanded();
  }
  else // Has been expanded
  {
    queues.reserveExtraEdgeSpace(queues.numberUnconnectedVertices() + ((perform_rewiring_checks) ? tree.size() : 0));
    // Find near new unconnected set
    near_unconnected_verts = queues.nearNewUnconnectedVertices(best_vertex, search_radius);
  }
  // Add edges to unconnected vertices
  const auto near_unconnected_verts_end = near_unconnected_verts.end();
  for(auto near_it = near_unconnected_verts.begin(); near_it < near_unconnected_verts_end; ++near_it)
  {
    const Eigen::Matrix<SCALAR,1,DIM,OPTIONS> steered_point =
      edge_generator->setOrientation((*near_it)->cgetPoint(), best_vertex->cgetNode()->cgetPoint());
    const SCALAR edge_cost_est = cost_function->costEstimate(best_vertex->cgetNode()->cgetPoint(), steered_point);
    const SCALAR edge_cost_to_go_est = cost_function->filletCostToGoEstimate(steered_point,
                                                                             best_vertex->cgetNode()->cgetPoint(),
                                                                             cur_target);
    // If the edge has the potential to help the solution add it to the edge queue
    if((best_vertex->cgetCostToComeEst() + edge_cost_est + edge_cost_to_go_est) < cur_sol_cost)
    {
      queues.addEdge(best_vertex, *near_it, edge_cost_est, edge_cost_to_go_est);
    }
  }
  // Add edges to connected vertices
  if(perform_rewiring_checks)
  {
    const std::vector<tree::Node<DIM,SCALAR,OPTIONS>*> neighbors =
      tree.findInRadius(best_vertex->cgetNode()->cgetPoint(), search_radius, std::numeric_limits<size_t>::max(), false);
    const size_t num_neighbors = neighbors.size();
    for(size_t neighbor_it = 0; neighbor_it < num_neighbors; ++neighbor_it)
    {
      // Check that the edge isn't already in the tree
      if(best_vertex->cgetNode() != neighbors[neighbor_it]->cgetParent())
      {
        const Eigen::Matrix<SCALAR,1,DIM,OPTIONS> steered_point =
          edge_generator->setOrientation(neighbors[neighbor_it]->cgetPoint(), best_vertex->cgetNode()->cgetPoint());
        const SCALAR edge_cost_est = cost_function->costEstimate(best_vertex->cgetNode()->cgetPoint(), steered_point);
        const SCALAR est_new_cost_node = best_vertex->cgetCostToComeEst() + edge_cost_est;
        // If this edge can improve the cost of the neighbor
        if(est_new_cost_node < neighbors[neighbor_it]->cgetCost())
        {
          Vertex<DIM,SCALAR,OPTIONS>* neighbor_vertex = queues.getConnectedVertex(neighbors[neighbor_it]->cgetIndex());

          const SCALAR edge_cost_to_go_est = cost_function->filletCostToGoEstimate(steered_point,
                                                                                   best_vertex->cgetNode()->cgetPoint(),
                                                                                   cur_target);

          // If this edge can improve the solution
          if((est_new_cost_node + edge_cost_to_go_est) < cur_sol_cost)
          {
            queues.addEdge(best_vertex, neighbor_vertex, edge_cost_est, edge_cost_to_go_est);
          }
        }
      }
    }
    best_vertex->setRewired();
  }
}

template<Eigen::Index DIM, RRTVersions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void batch::expandNextEdge(      QueueHolder<            DIM,false,  SCALAR,OPTIONS>& queues,
                                        tree::RRTTree<          DIM,VERSION,SCALAR,OPTIONS>& tree,
                                  const edge::EdgeGeneratorPtr< DIM,        SCALAR,OPTIONS>& edge_generator,
                                  const obs::ObstacleCheckerPtr<DIM,        SCALAR,OPTIONS>& obstacle_checker,
                                  const cost::CostFunctionPtr<  DIM,        SCALAR,OPTIONS>& cost_function)
{
  const SCALAR cur_sol_cost = queues.cgetSolutionCost();
  // Pop best edge in queue
  const std::unique_ptr<batch::Edge<DIM,SCALAR,OPTIONS>> best_edge(queues.popBestEdge());
  // Add edge to tree if it will help
  // If there is not any hope of this edge helping
  if(best_edge->cgetQueueCost() < cur_sol_cost)
  {
    if(best_edge->cgetTargetVertex()->partOfTree()) // Need to perform rewiring checks
    {
      const SCALAR edge_cost_to_come_est =
        best_edge->cgetSourceVertex()->cgetNode()->cgetCost() + best_edge->cgetEdgeCostEst();
      // If the rewiring can help the target node
      if(edge_cost_to_come_est < best_edge->cgetTargetVertex()->cgetNode()->cgetCost())
      {
        Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM,OPTIONS> real_edge;

        // Make potential edge
        // If obstacle free
        if(edge_generator->makeEdge(best_edge->cgetSourceVertex()->cgetNode()->cgetPoint(),
                                    best_edge->cgetTargetVertex()->cgetNode()->cgetPoint(),
                                    real_edge) and
           obstacle_checker->obstacleFree(real_edge))
        {
          const SCALAR edge_cost         = cost_function->cost(real_edge);
          const SCALAR edge_cost_to_come = best_edge->cgetSourceVertex()->cgetNode()->cgetCost() + edge_cost;

          assert(best_edge->cgetEdgeCostEst()                       <= edge_cost);
          assert(best_edge->cgetSourceVertex()->cgetCostToComeEst() <= best_edge->cgetSourceVertex()->cgetNode()->cgetCost());

          // If this edge can help the current tree
          if((edge_cost_to_come + best_edge->cgetEdgeCostToGoEst()) < cur_sol_cost)
          {
            // If the rewiring will help the target node
            if(edge_cost_to_come < best_edge->cgetTargetVertex()->cgetNode()->cgetCost())
            {
              tree.rewire(best_edge->getTargetVertex()->getNode(),
                          best_edge->getSourceVertex()->getNode(),
                          real_edge,
                          edge_cost);
              queues.rewiringOccurred();
            }
          }
        }
      }
    }
    else // Normal extension
    {
      Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM,OPTIONS> real_edge;

      // Make potential edge
      // If obstacle free
      if(edge_generator->makeEdge(best_edge->cgetSourceVertex()->cgetNode()->cgetPoint(),
                                  best_edge->cgetTargetVertex()->cgetPoint(),
                                  real_edge) and
         obstacle_checker->obstacleFree(real_edge))
      {
        const SCALAR edge_cost         = cost_function->cost(real_edge);
        const SCALAR edge_cost_to_come = best_edge->cgetSourceVertex()->cgetNode()->cgetCost() + edge_cost;

        assert(best_edge->cgetEdgeCostEst()                       <= edge_cost);
        assert(best_edge->cgetSourceVertex()->cgetCostToComeEst() <= best_edge->cgetSourceVertex()->cgetNode()->cgetCost());

        // If this edge can help the current tree
        if((edge_cost_to_come + best_edge->cgetEdgeCostToGoEst()) < cur_sol_cost)
        {
          // Add it to the tree
          tree::Node<DIM,SCALAR,OPTIONS>* new_node =
            tree.addEdge(best_edge->getSourceVertex()->getNode(), real_edge, edge_cost);
          best_edge->getTargetVertex()->setNode(new_node);
          // Remove it form the unconnected set and add it to the connected set and the vertex queue
          const size_t new_vert_index = queues.addVertex(best_edge->getTargetVertex());
          assert(new_vert_index == new_node->cgetIndex());
        }
      }
    }
  }
  else // There is no hope of this edge helping
  {
    queues.clearQueues();
  }
}

template<Eigen::Index DIM, RRTVersions VERSION, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline void batch::expandNextEdgeFillet(      QueueHolder<                 DIM,true,   SCALAR,OPTIONS>& queues,
                                              tree::RRTTree<               DIM,VERSION,SCALAR,OPTIONS>& tree,
                                        const edge::FilletEdgeGeneratorPtr<DIM,        SCALAR,OPTIONS>& edge_generator,
                                        const obs::ObstacleCheckerPtr<     DIM,        SCALAR,OPTIONS>& obstacle_checker,
                                        const cost::FilletCostFunctionPtr< DIM,        SCALAR,OPTIONS>& cost_function)
{
  // Pop best edge in queue
  const std::unique_ptr<batch::Edge<DIM,SCALAR,OPTIONS>> best_edge(queues.popBestEdge());
  const SCALAR                                           cur_sol_cost = queues.cgetSolutionCost();

  // Add edge to tree if it will help
  // If there is not any hope of this edge helping
  if(best_edge->cgetQueueCost() < cur_sol_cost)
  {
    if(best_edge->cgetTargetVertex()->partOfTree()) // Need to perform rewiring checks
    {
      const SCALAR edge_cost_to_come_est =
        best_edge->cgetSourceVertex()->cgetNode()->cgetCost() + best_edge->cgetEdgeCostEst();
      // If the rewiring can help the target node
      if(edge_cost_to_come_est < best_edge->cgetTargetVertex()->cgetNode()->cgetCost())
      {
        const Eigen::Matrix<SCALAR,1,DIM,OPTIONS> new_target_point =
          edge_generator->setOrientation(best_edge->cgetTargetVertex()->cgetNode()->cgetPoint(),
                                         best_edge->cgetSourceVertex()->cgetNode()->cgetPoint());
        if(not edge_generator->valid(best_edge->cgetSourceVertex()->cgetNode()->cgetParent()->cgetPoint(),
                                     best_edge->cgetSourceVertex()->cgetNode()->cgetPoint(),
                                     new_target_point)) { return; }
        // Check validity and heuristic cost of children
        const auto children_end = best_edge->cgetTargetVertex()->cgetNode()->cgetChildren().cend();
        for(auto child_it = best_edge->cgetTargetVertex()->cgetNode()->cgetChildren().cbegin();
            child_it != children_end;
            ++child_it)
        {
          const SCALAR child_est_edge_cost = cost_function->costEstimate(new_target_point, (*child_it)->cgetPoint());
          if((edge_cost_to_come_est + child_est_edge_cost) > (*child_it)->cgetCost()) { return; }
          if(not edge_generator->valid(best_edge->cgetSourceVertex()->cgetNode()->cgetPoint(),
                                       new_target_point,
                                       (*child_it)->cgetPoint())) { return; }
          const auto grandchildren_end = (*child_it)->cgetChildren().cend();
          for(auto grandchild_it = (*child_it)->cgetChildren().cbegin();
              grandchild_it != grandchildren_end;
              ++grandchild_it)
          {
            if(not edge_generator->valid(new_target_point,
                                         (*child_it)->cgetPoint(),
                                         (*grandchild_it)->cgetPoint())) { return; }
          }
        }

        // Check real costs
        Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM,OPTIONS> real_edge;
        Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM,OPTIONS> real_fillet;

        // Make potential edge
        if constexpr(edgeGeneratorUsesPreviousEdge(VERSION))
        {
          edge_generator->previousEdge(best_edge->cgetSourceVertex()->cgetNode()->cgetEdge());
        }
        const bool curve_made = edge_generator->makeEdge(best_edge->cgetSourceVertex()->cgetNode()->cgetFilletEnd(),
                                                         best_edge->cgetSourceVertex()->cgetNode()->cgetPoint(),
                                                         new_target_point,
                                                         real_fillet);
        // If turn is not obstacle free
        if(not (curve_made and obstacle_checker->obstacleFree(real_fillet))) { return; }
        // Make connecting edge
        const bool edge_made = edge_generator->makeEdge(real_fillet.template bottomRows<1>(),
                                                        new_target_point,
                                                        real_edge);
        // If obstacle free
        if(not (edge_made and obstacle_checker->obstacleFree(real_edge))) { return; }

        const SCALAR edge_cost =
          fillet::findNodeLocalCost<DIM,SCALAR,OPTIONS>(best_edge->cgetSourceVertex()->cgetNode()->cgetPoint(),
                                                        real_edge,
                                                        real_fillet,
                                                        edge_generator,
                                                        cost_function);
        const SCALAR edge_cost_to_come = best_edge->cgetSourceVertex()->cgetNode()->cgetCost() + edge_cost;

        assert((best_edge->cgetEdgeCostEst() - edge_cost) < ASSERT_INTEGRATION_EPS);
        assert((best_edge->cgetSourceVertex()->cgetCostToComeEst() - best_edge->cgetSourceVertex()->cgetNode()->cgetCost()) < ASSERT_INTEGRATION_EPS);

        // If this edge can help the current tree
        if((edge_cost_to_come + best_edge->cgetEdgeCostToGoEst()) < cur_sol_cost)
        {
          // If the rewiring will help the target node
          if(edge_cost_to_come < best_edge->cgetTargetVertex()->cgetNode()->cgetCost())
          {
            std::vector<Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM,OPTIONS>> childrens_new_edges;
            std::vector<Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM,OPTIONS>> childrens_new_fillets;
            std::vector<SCALAR>                                           childrens_new_costs;
            size_t                                                        child_index = 0;

            childrens_new_costs.  resize(best_edge->cgetTargetVertex()->cgetNode()->numberOfChildren());
            childrens_new_edges.  resize(best_edge->cgetTargetVertex()->cgetNode()->numberOfChildren());
            childrens_new_fillets.resize(best_edge->cgetTargetVertex()->cgetNode()->numberOfChildren());

            // If rewiring won't hurt it's children
            for(auto child_it = best_edge->cgetTargetVertex()->cgetNode()->cgetChildren().cbegin();
                child_it != children_end;
                ++child_it, ++child_index)
            {
              // Check that the edge that ends at this child is still valid

              // Make the curve
              if constexpr(edgeGeneratorUsesPreviousEdge(VERSION))
              {
                edge_generator->previousEdge(real_edge);
              }
              const bool child_curve_made = edge_generator->makeEdge(real_fillet.template bottomRows<1>(),
                                                                     new_target_point,
                                                                     (*child_it)->cgetPoint(),
                                                                     childrens_new_fillets[child_index]);
              assert(child_curve_made);
              // If turn is not obstacle free
              //if(not (child_curve_made and obstacle_checker->obstacleFree(childrens_new_fillets[child_index])))
              if(not obstacle_checker->obstacleFree(childrens_new_fillets[child_index])) { return; }

              // Edge that connects this curve to the child node
              if(not (edge_generator->makeEdge(childrens_new_fillets[child_index].template bottomRows<1>(),
                                               (*child_it)->cgetPoint(),
                                               childrens_new_edges[child_index]) and
                      obstacle_checker->obstacleFree(childrens_new_edges[child_index])))
              { return; }

              // Find the cost
              childrens_new_costs[child_index] =
                fillet::findNodeLocalCost<DIM,SCALAR,OPTIONS>(new_target_point,
                                                              childrens_new_edges[child_index],
                                                              childrens_new_fillets[child_index],
                                                              edge_generator,
                                                              cost_function);

              if((edge_cost_to_come + childrens_new_costs[child_index]) > (*child_it)->cgetCost()) { return; }
            }
            // Passed all checks, perform rewiring
            tree.filletRewire(best_edge->getTargetVertex()->getNode(),
                              best_edge->getSourceVertex()->getNode(),
                              real_edge,
                              real_fillet,
                              edge_cost,
                              childrens_new_edges,
                              childrens_new_fillets,
                              childrens_new_costs);
            // Update the cost-to-go of the target vertex with the edge orientation
            best_edge->getTargetVertex()->setCostToGoEst(best_edge->cgetEdgeCostToGoEst());
            // Update effected edges with edge-cost-est info with new orientation
            queues.updateEdgeCostEsts(best_edge->cgetTargetVertex());

            queues.rewiringOccurred();
          }
        }
      }
    }
    else // Normal extension
    {
      Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM,OPTIONS> real_edge;
      Eigen::Matrix<SCALAR,Eigen::Dynamic,DIM,OPTIONS> real_fillet;

      const Eigen::Matrix<SCALAR,1,DIM,OPTIONS> new_target_point =
        edge_generator->setOrientation(best_edge->cgetTargetVertex()->            cgetPoint(),
                                       best_edge->cgetSourceVertex()->cgetNode()->cgetPoint());

      // Make potential edge
      // Make the fillet
      if constexpr(edgeGeneratorUsesPreviousEdge(VERSION))
      {
        edge_generator->previousEdge(best_edge->cgetSourceVertex()->cgetNode()->cgetEdge());
      }
      const bool curve_made = edge_generator->makeEdge(best_edge->cgetSourceVertex()->cgetNode()->cgetFilletEnd(),
                                                       best_edge->cgetSourceVertex()->cgetNode()->cgetPoint(),
                                                       new_target_point,
                                                       real_fillet);
      // If turn is not obstacle free
      if(not (curve_made and obstacle_checker->obstacleFree(real_fillet))) { return; }
      // Make connecting edge
      const bool edge_made = edge_generator->makeEdge(real_fillet.template bottomRows<1>(),
                                                      new_target_point,
                                                      real_edge);
      // If obstacle free
      if(not (edge_made and obstacle_checker->obstacleFree(real_edge))) { return; }

      const SCALAR edge_cost =
        fillet::findNodeLocalCost<DIM,SCALAR,OPTIONS>(best_edge->cgetSourceVertex()->cgetNode()->cgetPoint(),
                                                      real_edge,
                                                      real_fillet,
                                                      edge_generator,
                                                      cost_function);
      const SCALAR edge_cost_to_come = best_edge->cgetSourceVertex()->cgetNode()->cgetCost() + edge_cost;

      assert((best_edge->cgetEdgeCostEst() - edge_cost) < ASSERT_INTEGRATION_EPS);
      assert((best_edge->cgetSourceVertex()->cgetCostToComeEst() - best_edge->cgetSourceVertex()->cgetNode()->cgetCost()) < ASSERT_INTEGRATION_EPS);

      // If this edge can help the current tree
      if((edge_cost_to_come + best_edge->cgetEdgeCostToGoEst()) < cur_sol_cost)
      {
        // Add it to the tree
        tree::Node<DIM,SCALAR,OPTIONS>* new_node =
          tree.addEdge(best_edge->getSourceVertex()->getNode(), real_edge, real_fillet, edge_cost);
        best_edge->getTargetVertex()->setNode(new_node);
        // Update the vertex cost-to-go with the edge orientation
        best_edge->getTargetVertex()->setCostToGoEst(best_edge->cgetEdgeCostToGoEst());
        // Remove it form the unconnected set and add it to the connected set and the vertex queue
        const size_t new_vert_index = queues.addVertex(best_edge->getTargetVertex());
        assert(new_vert_index == new_node->cgetIndex());
      }
    }
  }
  else // There is no hope of this edge helping
  {
    queues.clearQueues();
  }
}
} // search
} // rrt

#endif
/* batch_rrt_helpers.hpp */
