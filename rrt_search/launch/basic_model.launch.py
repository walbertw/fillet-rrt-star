##
# @File: basic_model.launch.py
#

from launch import LaunchDescription
from launch.actions import SetEnvironmentVariable
from launch_ros.actions import Node

def generate_launch_description():
    imu_noise_enabled = True
    bias_noise_enabled = True
    measurement_noise_enabled = True
    range_bearing_enabled = True

    return LaunchDescription([
        SetEnvironmentVariable('ASAN_OPTIONS', 'new_delete_type_mismatch=0'),
        Node(
            package='rrt_search',
            executable='basic_model_node',
            name='radar_planning_node',
            output='screen',
            #prefix=['xterm -e gdb -ex run --args'],
            parameters=[{
                'dt': 0.001, # 0.001
                'num_monte_carlo': 15, # 800
                'lin_cov':{
                    'imu':{
                        'accelerometer':{
                            'noise':{
                                'enabled': False,
                                'zero_mean': True,
                                'standard_deviation': [3.3333333E-04, 3.3333333E-04, 3.3333333E-04],
                                },
                            },
                        'gyroscope':{
                            'noise':{
                                'enabled': False,
                                'zero_mean': True,
                                'standard_deviation': [6.78739154E-06, 6.78739154E-06, 6.78739154E-06],
                                },
                            },
                        },
                    },
                'imu':{
                    'accelerometer':{
                        'noise':{
                            'enabled': imu_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [3.3333333E-04, 3.3333333E-04, 3.3333333E-04],
                            },
                        },
                    'gyroscope':{
                        'noise':{
                            'enabled': imu_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [6.78739154E-06, 6.78739154E-06, 6.78739154E-06],
                            },
                        },
                    },
                'dynamics':{
                    'biases':{
                        'heading':{
                            'noise':{
                                'enabled': bias_noise_enabled,
                                'zero_mean': True,
                                'standard_deviation': [0.0001],
                                'time_constant': [60.0],
                                },
                            },
                        'abs_pressure':{
                            'noise':{
                                'enabled': bias_noise_enabled,
                                'zero_mean': True,
                                'standard_deviation': [0.0001],
                                'time_constant': [60.0],
                                },
                            },
                        'feature_range':{
                            'noise':{
                                'enabled': bias_noise_enabled,
                                'zero_mean': True,
                                'standard_deviation': [0.0001],
                                'time_constant': [600.0],
                                },
                            },
                        'feature_bearing':{
                            'noise':{
                                'enabled': bias_noise_enabled,
                                'zero_mean': True,
                                'standard_deviation': [0.5, 0.5, 0.5],
                                'time_constant': [3600.0, 3600.0, 3600.0],
                                },
                            },
                        'gps_position':{
                            'noise':{
                                'enabled': bias_noise_enabled,
                                'zero_mean': True,
                                'standard_deviation': [2.167948339, 2.167948339, 3.033150178],
                                'time_constant': [60.0, 60.0, 60.0],
                                },
                            },
                        'accelerometer':{
                            'noise':{
                                'enabled': bias_noise_enabled,
                                'zero_mean': True,
                                'standard_deviation': [3.27E-03, 3.27E-03, 3.27E-03],
                                'time_constant': [60.0, 60.0, 60.0],
                                },
                            },
                        'gyroscope':{
                            'noise':{
                                'enabled': bias_noise_enabled,
                                'zero_mean': True,
                                'standard_deviation': [1.6160456E-06, 1.6160456E-06, 1.6160456E-06],
                                'time_constant': [60.0, 60.0, 60.0],
                                },
                            },
                        },
                    },
                'sensors':{
                    'gps':{
                        'enabled': True,
                        'measurement_period': 1.0,
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [3.33333333E-01, 3.33333333E-01, 1],
                            },
                        },
                    'heading':{
                        'enabled': False,
                        'measurement_period': 0.163,
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [5.81776417E-04],
                            },
                        },
                    'altitude':{
                        'enabled': False,
                        'measurement_period': 0.4,
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [3.33333333E-02],
                            },
                        },
                    'abs_pressure':{
                        'enabled': False,
                        'measurement_period': 0.24,
                        'gravity_magnitude': 9.81,
                        'air_density': 1.2682,
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [10.0],
                            },
                        },
                    'ground_velocity':{
                        'enabled': False,
                        'measurement_period': 1.1,
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [0.000000001],
                            },
                        },
                    'feature_range1':{
                        'enabled': range_bearing_enabled,
                        'measurement_period': 1.173,
                        'feature_range': 100.0,
                        'feature_location': [375.0, 375.0, 0.0],
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [0.0000001],
                            },
                        },
                    'feature_range2':{
                        'enabled': range_bearing_enabled,
                        'measurement_period': 1.173,
                        'feature_range': 100.0,
                        'feature_location': [250.0, 0.0, 0.0],
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [0.0000001],
                            },
                        },
                    'feature_range3':{
                        'enabled': range_bearing_enabled,
                        'measurement_period': 1.173,
                        'feature_range': 100.0,
                        'feature_location': [500.0, 750.0, 0.0],
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [0.0000001],
                            },
                        },
                    'feature_bearing1':{
                        'enabled': range_bearing_enabled,
                        'measurement_period': 1.12,
                        'camera_offset': [0.5, 0.0, 0.25],
                        'camera_viewing_angles': [0.0, 0.0, 0.0],
                        'feature_range': 100.0,
                        'feature_location': [375.0, 375.0, 0.0],
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [0.0001, 0.0001],
                            },
                        },
                    'feature_bearing2':{
                        'enabled': range_bearing_enabled,
                        'measurement_period': 1.12,
                        'camera_offset': [0.5, 0.0, 0.25],
                        'camera_viewing_angles': [0.0, 0.0, 0.0],
                        'feature_range': 100.0,
                        'feature_location': [250.0, 0.0, 0.0],
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [0.0001, 0.0001],
                            },
                        },
                    'feature_bearing3':{
                        'enabled': range_bearing_enabled,
                        'measurement_period': 1.12,
                        'camera_offset': [0.5, 0.0, 0.25],
                        'camera_viewing_angles': [0.0, 0.0, 0.0],
                        'feature_range': 100.0,
                        'feature_location': [500.0, 750.0, 0.0],
                        'noise':{
                            'enabled': measurement_noise_enabled,
                            'zero_mean': True,
                            'standard_deviation': [0.0001, 0.0001],
                            },
                        },
                    },
                }],
            ),
        ])

