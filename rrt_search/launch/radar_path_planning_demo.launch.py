##
# @File: radar_path_planning_demo.launch.py
#

from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        Node(
            package='rrt_search',
            executable='radar_path_planning_demo',
            name='radar_planning_node',
            output='screen',
            #prefix=['xterm -e gdb -ex run --args'],
            parameters=[{
                'imu':{
                    'accelerometer':{
                        'noise':{
                            'enabled': False,
                            'zero_mean': True,
                            'standard_deviation': [1.6667e-04, 1.6667e-04, 1.6667e-04],
                            },
                        },
                    'gyroscope':{
                        'noise':{
                            'enabled': False,
                            'zero_mean': True,
                            'standard_deviation': [4.8481e-06, 4.8481e-06, 4.8481e-06],
                            },
                        },
                    },
                'dynamics':{
                    'biases':{
                        'heading':{
                            'noise':{
                                'enabled': True,
                                'zero_mean': True,
                                'standard_deviation': [0.0001],
                                'time_constant': [60.0],
                                },
                            },
                        'abs_pressure':{
                            'noise':{
                                'enabled': True,
                                'zero_mean': True,
                                'standard_deviation': [0.0001],
                                'time_constant': [60.0],
                                },
                            },
                        'accelerometer':{
                            'noise':{
                                'enabled': True,
                                'zero_mean': True,
                                'standard_deviation': [3.27E-04, 3.27E-04, 3.27E-04],
                                'time_constant': [60.0, 60.0, 60.0],
                                },
                            },
                        'gyroscope':{
                            'noise':{
                                'enabled': True,
                                'zero_mean': True,
                                'standard_deviation': [1.6160456E-06, 1.6160456E-06, 1.6160456E-06],
                                'time_constant': [60.0, 60.0, 60.0],
                                },
                            },
                        },
                    },
                'sensors':{
                    'gps':{
                        'enabled': True,
                        'measurement_period': 1,
                        'noise':{
                            'enabled': True,
                            'zero_mean': True,
                            'standard_deviation': [3.33333333E-01, 3.33333333E-01, 1],
                            },
                        },
                    'heading':{
                        'enabled': True,
                        'measurement_period': 1,
                        'noise':{
                            'enabled': True,
                            'zero_mean': True,
                            'standard_deviation': [5.81776417E-04],
                            },
                        },
                    'altitude':{
                        'enabled': True,
                        'measurement_period': 1,
                        'noise':{
                            'enabled': True,
                            'zero_mean': True,
                            'standard_deviation': [3.33333333E-02],
                            },
                        },
                    },
                }]
            ),
        ])

