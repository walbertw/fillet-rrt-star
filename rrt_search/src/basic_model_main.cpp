/**
 * @File: basic_model_main.cpp
 * @Date: May 2022
 * @Author: James Swedeen
 *
 * @brief
 * Demos the Monte Carlo and LinCov abilities of this package.
 **/

/* C++ Headers */

/* Eigen Headers */
#include<Eigen/Dense>

/* ROS Headers */
#include<rclcpp/rclcpp.hpp>

/* Local Headers */
#include<rrt_search/edge_generators/fillets/imu/euler_spiral_imu_edge_generator.hpp>
#include<rrt_search/helpers/connect_waypoints.hpp>
#include<kalman_filter/kalman_filter.hpp>

std::vector<double> toVec(const Eigen::Matrix<double,1,Eigen::Dynamic,Eigen::RowMajor>& input)
{
  std::vector<double> output(input.cols());

  for(Eigen::Index col_it = 0; col_it < input.cols(); ++col_it)
  {
    output[col_it] = input[col_it];
  }

  return output;
}

int main(int argc, char** argv)
{
  rclcpp::init(argc, argv);
  rclcpp::Node::SharedPtr node = std::make_shared<rclcpp::Node>("basic_model_node");

  node->declare_parameter("dt",              rclcpp::PARAMETER_DOUBLE);
  node->declare_parameter("num_monte_carlo", rclcpp::PARAMETER_INTEGER);

  const double simulation_dt   = node->get_parameter("dt").as_double();
  const size_t num_sims        = node->get_parameter("num_monte_carlo").as_int();

  // Make reference trajectory
  const double nominal_velocity   = 35;
  const double max_curvature      = 0.005;
  const double max_curvature_rate = 0.0002;
  const double nominal_pitch      = rrt::math::angleToRadian<double>(4.2);
  const double nominal_down       = -500;
  const double gravity            = 9.81;

  Eigen::Matrix<double,Eigen::Dynamic,15,Eigen::RowMajor> ref_trajectory;
  {
    rrt::edge::IMUSignalEdgeGeneratorPtrd edge_gen =
      std::make_shared<rrt::edge::EulerSpiralIMUEdgeGeneratord>(nominal_velocity*simulation_dt,
                                                                max_curvature,
                                                                max_curvature_rate,
                                                                nominal_velocity,
                                                                nominal_pitch,
                                                                nominal_down,
                                                                gravity);
      /*std::make_shared<rrt::edge::ArcIMUEdgeGeneratord>(nominal_velocity*simulation_dt,
                                                        double(1)/max_curvature,
                                                        nominal_velocity,
                                                        nominal_pitch,
                                                        nominal_down,
                                                        gravity);*/
    std::list<Eigen::Matrix<double,1,15,Eigen::RowMajor>> waypoints;
    waypoints.emplace_back();
    waypoints.back().setZero();
    waypoints.emplace_back();
    waypoints.back().setZero();
    waypoints.back()[0] = 750;
    waypoints.back()[5] = std::atan2(waypoints.back()[1] - (*std::prev(waypoints.cend(), 2))[1],
                                     waypoints.back()[0] - (*std::prev(waypoints.cend(), 2))[0]);
    waypoints.emplace_back();
    waypoints.back().setZero();
    waypoints.back()[1] = 750;
    waypoints.back()[5] = std::atan2(waypoints.back()[1] - (*std::prev(waypoints.cend(), 2))[1],
                                     waypoints.back()[0] - (*std::prev(waypoints.cend(), 2))[0]);
    waypoints.emplace_back();
    waypoints.back().setZero();
    waypoints.back()[0] = 750;
    waypoints.back()[1] = 750;
    waypoints.back()[5] = std::atan2(waypoints.back()[1] - (*std::prev(waypoints.cend(), 2))[1],
                                     waypoints.back()[0] - (*std::prev(waypoints.cend(), 2))[0]);

    ref_trajectory = rrt::connectWaypointsFillets<15,double,Eigen::RowMajor>(waypoints, edge_gen);
  }

  // double max_dist = -1;
  // double min_dist = 1000000000;
  // for(Eigen::Index row_it = 1; row_it < ref_trajectory.rows(); ++row_it)
  // {
  //   const double temp_dist = (ref_trajectory.row(row_it-1).leftCols<2>() - ref_trajectory.row(row_it).leftCols<2>()).norm();
  //   max_dist = std::max<double>(temp_dist, max_dist);
  //   min_dist = std::min<double>(temp_dist, min_dist);
  //   if( 0 == temp_dist) std::cout << row_it << std::endl;
  // }
  // std::cout << "max_dist: " << max_dist << std::endl;
  // std::cout << "min_dist: " << min_dist << std::endl;
  // std::cout << "nom_dist: " << nominal_velocity*simulation_dt << std::endl;

  // Make IMU
  kf::Tools<kf::dynamics::BasicModelDim,double,Eigen::RowMajor> tools;

  auto inertial_measurements_func =
    std::make_shared<kf::sensors::OpenLoopIMU<kf::dynamics::BasicModelDim,double,Eigen::RowMajor>>(
      kf::noise::makeNormalDistribution<3,double,Eigen::RowMajor>(node, "imu.accelerometer.noise"),
      kf::noise::makeNormalDistribution<3,double,Eigen::RowMajor>(node, "imu.gyroscope.noise"));

  tools.inertial_measurements_func = inertial_measurements_func;

  // Make Dynamics object
  {
    const auto compass_bias_noise         = kf::noise::makeFirstOrderGaussMarkovDrivingNoise<1,double,Eigen::RowMajor>(node, "dynamics.biases.heading.noise");
    const auto abs_pressure_bias_noise    = kf::noise::makeFirstOrderGaussMarkovDrivingNoise<1,double,Eigen::RowMajor>(node, "dynamics.biases.abs_pressure.noise");
    const auto feature_range_bias_noise   = kf::noise::makeFirstOrderGaussMarkovDrivingNoise<1,double,Eigen::RowMajor>(node, "dynamics.biases.feature_range.noise");
    const auto feature_bearing_bias_noise = kf::noise::makeFirstOrderGaussMarkovDrivingNoise<3,double,Eigen::RowMajor>(node, "dynamics.biases.feature_bearing.noise");
    const auto gps_position_bias_noise    = kf::noise::makeFirstOrderGaussMarkovDrivingNoise<3,double,Eigen::RowMajor>(node, "dynamics.biases.gps_position.noise");
    const auto accel_bias_noise           = kf::noise::makeFirstOrderGaussMarkovDrivingNoise<3,double,Eigen::RowMajor>(node, "dynamics.biases.accelerometer.noise");
    const auto gyro_bias_noise            = kf::noise::makeFirstOrderGaussMarkovDrivingNoise<3,double,Eigen::RowMajor>(node, "dynamics.biases.gyroscope.noise");

    tools.dynamics_func = std::make_shared<kf::dynamics::BasicModel<double,Eigen::RowMajor>>(
                            compass_bias_noise,
                            abs_pressure_bias_noise,
                            feature_range_bias_noise,
                            feature_bearing_bias_noise,
                            gps_position_bias_noise,
                            accel_bias_noise,
                            gyro_bias_noise,
                            nominal_velocity,
                            gravity,
                            node->get_parameter("dynamics.biases.heading.noise.time_constant").        as_double_array()[0],
                            node->get_parameter("dynamics.biases.abs_pressure.noise.time_constant").   as_double_array()[0],
                            node->get_parameter("dynamics.biases.feature_range.noise.time_constant").  as_double_array()[0],
                            node->get_parameter("dynamics.biases.feature_bearing.noise.time_constant").as_double_array()[0],
                            node->get_parameter("dynamics.biases.gps_position.noise.time_constant").   as_double_array()[0],
                            node->get_parameter("dynamics.biases.accelerometer.noise.time_constant").  as_double_array()[0],
                            node->get_parameter("dynamics.biases.gyroscope.noise.time_constant").      as_double_array()[0]);
  }

  // Make controller
  tools.controller =
    std::make_shared<kf::control::OpenLoopController<kf::dynamics::BasicModelDim,double,Eigen::RowMajor>>();

  // Make measurement controller
  tools.measurement_controller =
    std::make_shared<kf::sensors::AllSensorsController<kf::dynamics::BasicModelDim,double,Eigen::RowMajor>>(
      kf::sensors::makeGPS<             kf::dynamics::BasicModelDim,double>(node, "sensors.gps"),
      kf::sensors::makeHeading<         kf::dynamics::BasicModelDim,double>(node, "sensors.heading"),
      kf::sensors::makeAltitude<        kf::dynamics::BasicModelDim,double>(node, "sensors.altitude"),
      kf::sensors::makeAbsolutePressure<kf::dynamics::BasicModelDim,double>(node, "sensors.abs_pressure"),
      kf::sensors::makeGroundVelocity<  kf::dynamics::BasicModelDim,double>(node, "sensors.ground_velocity"),
      std::vector<kf::sensors::MeasurementBasePtr<1,kf::dynamics::BasicModelDim,double>>({
        kf::sensors::makeFeatureRange<  kf::dynamics::BasicModelDim,double>(node, "sensors.feature_range1"),
        kf::sensors::makeFeatureRange<  kf::dynamics::BasicModelDim,double>(node, "sensors.feature_range2"),
        kf::sensors::makeFeatureRange<  kf::dynamics::BasicModelDim,double>(node, "sensors.feature_range3")
        }),
      std::vector<kf::sensors::MeasurementBasePtr<2,kf::dynamics::BasicModelDim,double>>({
        kf::sensors::makeFeatureBearing<kf::dynamics::BasicModelDim,double>(node, "sensors.feature_bearing1"),
        kf::sensors::makeFeatureBearing<kf::dynamics::BasicModelDim,double>(node, "sensors.feature_bearing2"),
        kf::sensors::makeFeatureBearing<kf::dynamics::BasicModelDim,double>(node, "sensors.feature_bearing3")
        }));

  // Make mapping object
  tools.mappings = std::make_shared<kf::map::SimpleMapping<kf::dynamics::BasicModelDim,double,Eigen::RowMajor>>();

  // Make state vector
  Eigen::Matrix<double,1,kf::dynamics::BasicModelDim::MC::FULL_STATE_LEN,Eigen::RowMajor> init_sim_vec;
  init_sim_vec.setZero();
  // Set the reference trajectory
  init_sim_vec.middleCols<kf::dynamics::BasicModelDim::REF_DIM>(kf::dynamics::BasicModelDim::REF_START_IND) =
    ref_trajectory.leftCols<kf::dynamics::BasicModelDim::REF_DIM>().row(0);
  // Set the time
  init_sim_vec(0, kf::dynamics::BasicModelDim::TIME_IND) = 0;
  // Set the starting state
  init_sim_vec.row(0).middleCols<3>(kf::dynamics::BasicModelDim::MC::TRUTH_START_IND +
                               kf::dynamics::BasicModelDim::TRUTH::POS_START_IND) = init_sim_vec.row(0).middleCols<3>(kf::dynamics::BasicModelDim::REF_START_IND +
                                                                                                                 kf::dynamics::BasicModelDim::REF::POS_START_IND);
  init_sim_vec.row(0).middleCols<4>(kf::dynamics::BasicModelDim::MC::TRUTH_START_IND + kf::dynamics::BasicModelDim::TRUTH::QUAT_START_IND) = kf::math::quat::rollPitchYawToQuaternion(init_sim_vec.row(0).middleCols<3>(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::EULER_START_IND));
  init_sim_vec.row(0).middleCols<3>(kf::dynamics::BasicModelDim::MC::TRUTH_START_IND +
                               kf::dynamics::BasicModelDim::TRUTH::VEL_START_IND) = init_sim_vec.row(0).middleCols<3>(kf::dynamics::BasicModelDim::REF_START_IND +
                                                                                                                 kf::dynamics::BasicModelDim::REF::VEL_START_IND);
  init_sim_vec.row(0).middleCols<6>(kf::dynamics::BasicModelDim::MC::TRUTH_START_IND + kf::dynamics::BasicModelDim::TRUTH::GYRO_BIAS_START_IND).setZero();

  init_sim_vec.row(0).middleCols<kf::dynamics::BasicModelDim::NAV_DIM>(kf::dynamics::BasicModelDim::MC::NAV_START_IND) =
    init_sim_vec.row(0).middleCols<kf::dynamics::BasicModelDim::TRUTH_DIM>(kf::dynamics::BasicModelDim::MC::TRUTH_START_IND);
  Eigen::Map<Eigen::Matrix<double,kf::dynamics::BasicModelDim::ERROR_DIM,kf::dynamics::BasicModelDim::ERROR_DIM,Eigen::RowMajor>> error_covariance(
        init_sim_vec.block<1,kf::dynamics::BasicModelDim::ERROR_COV_LEN>(0, kf::dynamics::BasicModelDim::ERROR_COV_START_IND).data());
  error_covariance.setZero();
/*  error_covariance(0,0) = std::pow(3.33333333E-01, 2);
  error_covariance(1,1) = std::pow(3.33333333E-01, 2);
  error_covariance(2,2) = std::pow(1, 2);
  error_covariance(3,3) = std::pow(3.33333333E-02, 2);
  error_covariance(4,4) = std::pow(3.33333333E-02, 2);
  error_covariance(5,5) = std::pow(3.33333333E-02, 2);
  error_covariance(6,6) = std::pow(3.33333333E-02, 2);
  error_covariance(7,7) = std::pow(3.33333333E-02, 2);
  error_covariance(8,8) = std::pow(3.33333333E-02, 2);
  error_covariance(9,9) = std::pow(1.6160456E-06, 2);
  error_covariance(10,10) = std::pow(1.6160456E-06, 2);
  error_covariance(11,11) = std::pow(1.6160456E-06, 2);
  error_covariance(12,12) = std::pow(3.27E-03, 2);
  error_covariance(13,13) = std::pow(3.27E-03, 2);
  error_covariance(14,14) = std::pow(3.27E-03, 2);*/

  // Noise adding function for the starting state
  const auto init_noise_func = [] (Eigen::Ref<Eigen::Matrix<double,1,kf::dynamics::BasicModelDim::TRUTH_DIM,Eigen::RowMajor>> truth_state,
                                   Eigen::Ref<Eigen::Matrix<double,1,kf::dynamics::BasicModelDim::NAV_DIM,  Eigen::RowMajor>> /* nav_state */)
  {
    kf::noise::NormalDistribution<3,true,true,true> noise;

    truth_state.middleCols<3>(kf::dynamics::BasicModelDim::TRUTH::GYRO_BIAS_START_IND) +=
      (Eigen::Matrix<double,3,3,Eigen::RowMajor>::Identity().array() * 1.616E-06).matrix() * noise.getNoise().transpose();
    truth_state.middleCols<3>(kf::dynamics::BasicModelDim::TRUTH::ACCEL_BIAS_START_IND) +=
      (Eigen::Matrix<double,3,3,Eigen::RowMajor>::Identity().array() * 0.0033).matrix() * noise.getNoise().transpose();
  };

  // Make LinCov State vector
  Eigen::Matrix<double,Eigen::Dynamic,kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,Eigen::RowMajor> lincov_sim_vec;
  lincov_sim_vec.resize(ref_trajectory.rows(), Eigen::NoChange);
  lincov_sim_vec.setConstant(std::numeric_limits<double>::quiet_NaN());
  // Set time
  lincov_sim_vec(0,0) = 0;
  lincov_sim_vec.middleCols<kf::dynamics::BasicModelDim::NUM_MEAS_DIM>(kf::dynamics::BasicModelDim::NUM_MEAS_START_IND).setZero();
  // Set reference trajectory
  lincov_sim_vec.middleCols<kf::dynamics::BasicModelDim::REF_DIM>(kf::dynamics::BasicModelDim::REF_START_IND) = ref_trajectory.leftCols<kf::dynamics::BasicModelDim::REF_DIM>();
  // Set starting covariance
  lincov_sim_vec.row(0).block<1,kf::dynamics::BasicModelDim::ERROR_COV_LEN>(0, kf::dynamics::BasicModelDim::ERROR_COV_START_IND) =
    init_sim_vec.block<1,kf::dynamics::BasicModelDim::ERROR_COV_LEN>(0, kf::dynamics::BasicModelDim::ERROR_COV_START_IND);

  Eigen::Map<Eigen::Matrix<double,kf::dynamics::BasicModelDim::LINCOV::AUG_DIM,kf::dynamics::BasicModelDim::LINCOV::AUG_DIM,Eigen::RowMajor>> init_aug_covariance(
    lincov_sim_vec.template block<1,kf::dynamics::BasicModelDim::LINCOV::AUG_COV_LEN>(0, kf::dynamics::BasicModelDim::LINCOV::AUG_COV_START_IND).data());
  init_aug_covariance.setZero();

  init_aug_covariance.block<3,3>(kf::dynamics::BasicModelDim::ERROR::GYRO_BIAS_START_IND,kf::dynamics::BasicModelDim::ERROR::GYRO_BIAS_START_IND) =
    (Eigen::Matrix<double,3,3,Eigen::RowMajor>::Identity().array() * std::pow(1.616E-06, 2)).matrix();
  init_aug_covariance.block<3,3>(kf::dynamics::BasicModelDim::ERROR::ACCEL_BIAS_START_IND,kf::dynamics::BasicModelDim::ERROR::ACCEL_BIAS_START_IND) =
    (Eigen::Matrix<double,3,3,Eigen::RowMajor>::Identity().array() * std::pow(0.0033, 2)).matrix();

  init_aug_covariance.block<kf::dynamics::BasicModelDim::ERROR_DIM,kf::dynamics::BasicModelDim::ERROR_DIM>(kf::dynamics::BasicModelDim::ERROR_DIM,kf::dynamics::BasicModelDim::ERROR_DIM) =
    error_covariance;

  // Add bounds to the GPS bias as a test
  tools.bounded_indexes.resize(3);
  tools.upper_bound.resize(3);
  tools.lower_bound.resize(3);
  for(Eigen::Index state_it = 0; state_it < 3; ++state_it)
  {
    tools.bounded_indexes[state_it] = state_it + kf::dynamics::BasicModelDim::NAV::GPS_POS_BIAS_START_IND;
    tools.upper_bound[state_it] = 0.0001;
    tools.lower_bound[state_it] = -0.0001;
  }

  // Run MC simulation
  std::vector<Eigen::Matrix<double,Eigen::Dynamic,kf::dynamics::BasicModelDim::MC::FULL_STATE_LEN,Eigen::RowMajor>> output;

  std::chrono::high_resolution_clock::time_point start_time = std::chrono::high_resolution_clock::now();

  kf::runMonteCarloSims<kf::dynamics::BasicModelDim,kf::Versions::NULL_VERSION,double,Eigen::RowMajor>(
    ref_trajectory.leftCols<kf::dynamics::BasicModelDim::REF_DIM>(),
    init_sim_vec,
    num_sims,
    tools,
    init_noise_func,
    output);

  std::cout << "MC simulation took " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_time).count() << " milliseconds" << std::endl;

  // Run LinCov simulation
  tools.inertial_measurements_func =
    std::make_shared<kf::sensors::OpenLoopIMU<kf::dynamics::BasicModelDim,double,Eigen::RowMajor>>(
      kf::noise::makeNormalDistribution<3,double,Eigen::RowMajor>(node, "lin_cov.imu.accelerometer.noise"),
      kf::noise::makeNormalDistribution<3,double,Eigen::RowMajor>(node, "lin_cov.imu.gyroscope.noise"));

  start_time = std::chrono::high_resolution_clock::now();

  kf::runLinCov<kf::dynamics::BasicModelDim,kf::Versions::NULL_VERSION,double,Eigen::RowMajor>(
    lincov_sim_vec,
    tools);

  std::cout << "LC simulation took " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_time).count() << " milliseconds" << std::endl;

  const auto temp_func = [](const Eigen::Matrix<double,Eigen::Dynamic,kf::dynamics::BasicModelDim::TRUTH_DIM,Eigen::RowMajor>& input) -> Eigen::Matrix<double,Eigen::Dynamic,kf::dynamics::BasicModelDim::TRUTH_DISP_DIM,Eigen::RowMajor>
  {
    Eigen::Matrix<double,Eigen::Dynamic,kf::dynamics::BasicModelDim::TRUTH_DISP_DIM,Eigen::RowMajor> output;
    output.resize(input.rows(), Eigen::NoChange);

    // Deal with all of the non quaternion stuff
    output.template leftCols<kf::dynamics::BasicModelDim::TRUTH_DISP::EULER_START_IND>() = input.template leftCols<kf::dynamics::BasicModelDim::TRUTH::QUAT_START_IND>();
    output.template rightCols<kf::dynamics::BasicModelDim::TRUTH_DISP_DIM-kf::dynamics::BasicModelDim::TRUTH_DISP::EULER_END_IND-1>() = input.template rightCols<kf::dynamics::BasicModelDim::TRUTH_DIM-kf::dynamics::BasicModelDim::TRUTH::QUAT_END_IND-1>();
    // Deal with the quaternion
    output.template middleCols<3>(kf::dynamics::BasicModelDim::TRUTH_DISP::EULER_START_IND) =
      kf::math::quat::quaternionToRollPitchYaw(input.template middleCols<4>(kf::dynamics::BasicModelDim::TRUTH::QUAT_START_IND));

    return output;
  };

//  std::cout << Eigen::Map<Eigen::Matrix<double,kf::dynamics::BasicModelDim::LINCOV::AUG_DIM,kf::dynamics::BasicModelDim::LINCOV::AUG_DIM,Eigen::RowMajor>>(
//      lincov_sim_vec.template block<1,kf::dynamics::BasicModelDim::LINCOV::AUG_COV_LEN>(lincov_sim_vec.rows()-1, kf::dynamics::BasicModelDim::LINCOV::AUG_COV_START_IND).data()).rightCols(kf::dynamics::BasicModelDim::NAV_DIM).bottomRows(kf::dynamics::BasicModelDim::NAV_DIM) << std::endl;

  kf::plot::plotAllStatistics<kf::dynamics::BasicModelDim,double,Eigen::RowMajor>(
    output,
    lincov_sim_vec,
    tools.mappings,
    temp_func,
    temp_func,
    {
      std::make_pair("Position", std::make_tuple(3,
                                                 kf::dynamics::BasicModelDim::REF::POS_START_IND,
                                                 kf::dynamics::BasicModelDim::TRUTH_DISP::POS_START_IND,
                                                 kf::dynamics::BasicModelDim::ERROR::POS_START_IND)),
      std::make_pair("Euler Angles", std::make_tuple(3,
                                                     kf::dynamics::BasicModelDim::REF::EULER_START_IND,
                                                     kf::dynamics::BasicModelDim::TRUTH_DISP::EULER_START_IND,
                                                     kf::dynamics::BasicModelDim::ERROR::EULER_START_IND)),
      std::make_pair("NED Velocity", std::make_tuple(3,
                                                     kf::dynamics::BasicModelDim::REF::VEL_START_IND,
                                                     kf::dynamics::BasicModelDim::TRUTH_DISP::VEL_START_IND,
                                                     kf::dynamics::BasicModelDim::ERROR::VEL_START_IND)),
      std::make_pair("Heading Bias", std::make_tuple(1,
                                                     -1,
                                                     kf::dynamics::BasicModelDim::TRUTH::HEADING_BIAS_IND,
                                                     kf::dynamics::BasicModelDim::TRUTH_DISP::HEADING_BIAS_IND)),
      std::make_pair("Abs Pressure Bias", std::make_tuple(1,
                                                          -1,
                                                          kf::dynamics::BasicModelDim::TRUTH::ABS_PRESSURE_BIAS_IND,
                                                          kf::dynamics::BasicModelDim::TRUTH_DISP::ABS_PRESSURE_BIAS_IND)),
      std::make_pair("Feature Range Bias", std::make_tuple(1,
                                                           -1,
                                                           kf::dynamics::BasicModelDim::TRUTH::FEATURE_RANGE_BIAS_IND,
                                                           kf::dynamics::BasicModelDim::TRUTH_DISP::FEATURE_RANGE_BIAS_IND)),
      std::make_pair("Feature Bearing Bias", std::make_tuple(3,
                                                             -1,
                                                             kf::dynamics::BasicModelDim::TRUTH::FEATURE_BEARING_BIAS_START_IND,
                                                             kf::dynamics::BasicModelDim::TRUTH_DISP::FEATURE_BEARING_BIAS_START_IND)),
      std::make_pair("GPS Position Bias", std::make_tuple(3,
                                                          -1,
                                                          kf::dynamics::BasicModelDim::TRUTH::GPS_POS_BIAS_START_IND,
                                                          kf::dynamics::BasicModelDim::TRUTH_DISP::GPS_POS_BIAS_START_IND)),
      std::make_pair("Gyro Bias", std::make_tuple(3,
                                                  -1,
                                                  kf::dynamics::BasicModelDim::TRUTH::GYRO_BIAS_START_IND,
                                                  kf::dynamics::BasicModelDim::TRUTH_DISP::GYRO_BIAS_START_IND)),
      std::make_pair("Accel Bias", std::make_tuple(3,
                                                   -1,
                                                   kf::dynamics::BasicModelDim::TRUTH_DISP::ACCEL_BIAS_START_IND,
                                                   kf::dynamics::BasicModelDim::ERROR::ACCEL_BIAS_START_IND)),
    },
    {true,false,false,true,false,false},
    1,
    false);

  // Euler Vel
/*  matplotlibcpp::figure();
  matplotlibcpp::subplot(3, 1, 1);
  matplotlibcpp::xlabel("Time");
  matplotlibcpp::ylabel("Roll Vel");
  matplotlibcpp::named_plot<double,double>("Reference", toVec(lincov_sim_vec.col(kf::dynamics::BasicModelDim::TIME_IND)), toVec(ref_trajectory.col(9)), "g");
  matplotlibcpp::legend();
  matplotlibcpp::title("Gyro Measurements Over Time");

  matplotlibcpp::subplot(3, 1, 2);
  matplotlibcpp::xlabel("Time");
  matplotlibcpp::ylabel("Pitch Vel");
  matplotlibcpp::named_plot<double,double>("Reference", toVec(lincov_sim_vec.col(kf::dynamics::BasicModelDim::TIME_IND)), toVec(ref_trajectory.col(10)), "g");

  matplotlibcpp::subplot(3, 1, 3);
  matplotlibcpp::xlabel("Time");
  matplotlibcpp::ylabel("Yaw Vel");
  matplotlibcpp::named_plot<double,double>("Reference", toVec(lincov_sim_vec.col(kf::dynamics::BasicModelDim::TIME_IND)), toVec(ref_trajectory.col(11)), "g");
*/
  // X Y Plot
  const Eigen::Matrix<double,Eigen::Dynamic,kf::dynamics::BasicModelDim::TRUTH_DIM,Eigen::RowMajor> avg_truth_state =
    kf::math::approxMeanTruthStateTrajectory<kf::dynamics::BasicModelDim,double,Eigen::RowMajor>(output, tools.mappings);
  const Eigen::Matrix<double,Eigen::Dynamic,kf::dynamics::BasicModelDim::NAV_DIM,Eigen::RowMajor> avg_nav_state =
    kf::math::approxMeanNavStateTrajectory<kf::dynamics::BasicModelDim,double,Eigen::RowMajor>(output, tools.mappings);

  matplotlibcpp::figure();
  matplotlibcpp::xlabel("North (m)");
  matplotlibcpp::ylabel("East (m)");
  matplotlibcpp::set_aspect_equal();
  matplotlibcpp::named_plot<double,double>("Reference",       toVec(ref_trajectory. col(kf::dynamics::BasicModelDim::REF::NORTH_IND)),   toVec(ref_trajectory. col(kf::dynamics::BasicModelDim::REF::EAST_IND)), "g");
  matplotlibcpp::named_plot<double,double>("Avg. Truth",      toVec(avg_truth_state.col(kf::dynamics::BasicModelDim::TRUTH::NORTH_IND)), toVec(avg_truth_state.col(kf::dynamics::BasicModelDim::REF::EAST_IND)), "b");
  matplotlibcpp::named_plot<double,double>("Avg. Navigation", toVec(avg_nav_state.  col(kf::dynamics::BasicModelDim::NAV::NORTH_IND)),   toVec(avg_nav_state.  col(kf::dynamics::BasicModelDim::REF::EAST_IND)), "r");
  matplotlibcpp::named_plot<double,double>("Buoy", {375.0, 250.0, 500.0}, {375.0, 0.0, 750.0}, "k*");
  matplotlibcpp::legend();
  matplotlibcpp::title("Birds Eye View");

  matplotlibcpp::show();
  exit(EXIT_SUCCESS);
}

/* basic_model_main.cpp */
