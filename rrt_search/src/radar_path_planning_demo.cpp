/**
 * @File: radar_path_planning_demo.cpp
 * @Date: June 2022
 * @Author: James Swedeen
 *
 * @brief
 * Demos the path planning under probability of detection of this repository.
 **/

/* C++ Headers */

/* Eigen Headers */
#include<Eigen/Dense>

/* ROS Headers */
#include<rclcpp/rclcpp.hpp>

/* Local Headers */
#include<rrt_search/rrt_search.hpp>
#include<rrt_search/search_functions/radar_visibility_graph.hpp>
#include<kalman_filter/kalman_filter.hpp>
#include<radar_detection/radar_detection.hpp>

using scalar = double;

std::vector<double> toVec(const Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor>& input)
{
  std::vector<double> output(input.cols());

  for(Eigen::Index col_it = 0; col_it < input.cols(); ++col_it)
  {
    output[col_it] = input[col_it];
  }

  return output;
}

template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
struct ReferenceDistanceFunc
{
  /**
   * Calculates the distance.
   **/
  template<typename DERIVED1, typename DERIVED2>
  inline SCALAR operator()(const Eigen::MatrixBase<DERIVED1>& starting_point,
                           const Eigen::MatrixBase<DERIVED2>& ending_point)
  {
    return (starting_point.template middleCols<2>(DIM_S::REF_START_IND + DIM_S::REF::POS_START_IND) -
            ending_point.  template middleCols<2>(DIM_S::REF_START_IND + DIM_S::REF::POS_START_IND)).norm();
  }
  /**
   * @InternalDim
   *
   * @brief
   * The size of the internally used state vectors.
   **/
  inline constexpr static const Eigen::Index InternalDim = 2;
  /**
   * Presets the state for use.
   **/
  template<typename DERIVED>
  inline Eigen::Matrix<SCALAR,1,InternalDim,OPTIONS>
    to_internal(const Eigen::MatrixBase<DERIVED>& input)
  {
    return input.template middleCols<2>(DIM_S::REF_START_IND + DIM_S::REF::POS_START_IND);
  }
  /**
   * @findDist
   **/
  template<typename DERIVED1, typename DERIVED2>
  inline SCALAR findDist(const Eigen::MatrixBase<DERIVED1>& starting_point,
                         const Eigen::MatrixBase<DERIVED2>& ending_point)
  {
    return (starting_point - ending_point).norm();
  }
};

int main(int argc, char** argv)
{
  rclcpp::init(argc, argv);
  rclcpp::Node::SharedPtr node = std::make_shared<rclcpp::Node>("radar_path_planning_node");

  //std::cin.get();

  const double fillet_dt          = 0.01;
  const double line_dt            = 1; //1;
  const double nominal_velocity   = 80;
  const double max_curvature      = 0.0002;
  const double max_curvature_rate = 0.00001;
  const double nominal_pitch      = rrt::math::angleToRadian<double>(4.2);
  const double nominal_down       = -3500;
  const double gravity            = 9.81;

  const Eigen::Matrix<scalar,1,2,Eigen::RowMajor> target_point = (Eigen::Matrix<scalar,2,1>() << -200e3, 0).finished();
  const Eigen::Matrix<scalar,1,2,Eigen::RowMajor> start_point  = (Eigen::Matrix<scalar,2,1>() << -1500e3, 1400e3).finished();
  //const Eigen::Matrix<scalar,1,2,Eigen::RowMajor> target_point = (Eigen::Matrix<scalar,2,1>() << -500e3, 500e3).finished();

  /// Starting point
  Eigen::Matrix<scalar,1,kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,Eigen::RowMajor> starting_point;
  starting_point.setConstant(std::numeric_limits<scalar>::quiet_NaN());
  // Set time
  starting_point[0] = 0;
  starting_point.middleCols<kf::dynamics::BasicModelDim::NUM_MEAS_DIM>(kf::dynamics::BasicModelDim::NUM_MEAS_START_IND).setZero();
  // Set reference trajectory
  starting_point.middleCols<kf::dynamics::BasicModelDim::REF_DIM>(kf::dynamics::BasicModelDim::REF_START_IND).setZero();
  starting_point.middleCols<2>(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::POS_START_IND) = start_point;
  starting_point[kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::DOWN_IND]  = nominal_down;
  starting_point[kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::PITCH_IND] = nominal_pitch;
  starting_point[kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::YAW_IND]   = rrt::math::angleToRadian<scalar>(120);
  // Set starting covariance
  //starting_point.block<1,kf::dynamics::BasicModelDim::ERROR_COV_LEN>(0, kf::dynamics::BasicModelDim::ERROR_COV_START_IND).setZero();
  Eigen::Map<Eigen::Matrix<double,kf::dynamics::BasicModelDim::ERROR_DIM,kf::dynamics::BasicModelDim::ERROR_DIM,Eigen::RowMajor>> error_covariance(
    starting_point.block<1,kf::dynamics::BasicModelDim::ERROR_COV_LEN>(0, kf::dynamics::BasicModelDim::ERROR_COV_START_IND).data());
  error_covariance.setZero();
/*  error_covariance(0,0) = std::pow(3.33333333E-01, 2);
  error_covariance(1,1) = std::pow(3.33333333E-01, 2);
  error_covariance(2,2) = std::pow(1, 2);
  error_covariance(3,3) = std::pow(3.33333333E-02, 2);
  error_covariance(4,4) = std::pow(3.33333333E-02, 2);
  error_covariance(5,5) = std::pow(3.33333333E-02, 2);
  error_covariance(6,6) = std::pow(3.33333333E-02, 2);
  error_covariance(7,7) = std::pow(3.33333333E-02, 2);
  error_covariance(8,8) = std::pow(3.33333333E-02, 2);
  error_covariance(9,9) = std::pow(1.6160456E-06, 2);
  error_covariance(10,10) = std::pow(1.6160456E-06, 2);
  error_covariance(11,11) = std::pow(1.6160456E-06, 2);
  error_covariance(12,12) = std::pow(3.27E-03, 2);
  error_covariance(13,13) = std::pow(3.27E-03, 2);
  error_covariance(14,14) = std::pow(3.27E-03, 2);*/

  Eigen::Map<Eigen::Matrix<scalar,kf::dynamics::BasicModelDim::LINCOV::AUG_DIM,kf::dynamics::BasicModelDim::LINCOV::AUG_DIM,Eigen::RowMajor>> init_aug_covariance(
    starting_point.block<1,kf::dynamics::BasicModelDim::LINCOV::AUG_COV_LEN>(0, kf::dynamics::BasicModelDim::LINCOV::AUG_COV_START_IND).data());
  init_aug_covariance.setZero();
  init_aug_covariance.block<3,3>(kf::dynamics::BasicModelDim::ERROR::GYRO_BIAS_START_IND,kf::dynamics::BasicModelDim::ERROR::GYRO_BIAS_START_IND) =
    (Eigen::Matrix<scalar,3,3,Eigen::RowMajor>::Identity().array() * std::pow(1.616E-04, 2)).matrix();
  init_aug_covariance.block<3,3>(kf::dynamics::BasicModelDim::ERROR::ACCEL_BIAS_START_IND,kf::dynamics::BasicModelDim::ERROR::ACCEL_BIAS_START_IND) =
    (Eigen::Matrix<scalar,3,3,Eigen::RowMajor>::Identity().array() * std::pow(0.0327, 2)).matrix();

  // For error budget plots
  std::list<kf::plot::GeneralNoiseWrapper<scalar,Eigen::RowMajor>> noise_sources;

  /// RRT Tools
  rrt::search::FilletTools<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,scalar> rrt_tools;
  // Setup plotter
  rrt::logger::BufferLoggerPtr<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,scalar> tree_logger =
    std::make_shared<rrt::logger::BufferLogger<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,scalar>>();
  rrt::logger::CounterLoggerPtr<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,scalar> counter_logger =
    std::make_shared<rrt::logger::CounterLogger<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,scalar>>();
  auto temp_logger = std::make_shared<rrt::logger::MultiLogger<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,scalar>>();
  temp_logger->addLogger(tree_logger);
  temp_logger->addLogger(counter_logger);
  rrt_tools.logger = temp_logger;

  // Setup Obstacle Checker
  rrt::obs::ProbabilityDetectionMetricObstacleCheckerPtr<kf::dynamics::BasicModelDim,scalar> obs_checker;
  {
    rd::CrossSectionModelPtr<scalar,Eigen::Dynamic> cross_section_model    = std::make_shared<rd::EllipsoidCrossSectionModel<scalar,Eigen::Dynamic>>(0.18, 0.17, 0.2);
    rd::RadarModelPtr<       scalar,Eigen::Dynamic> radar_model_50         = std::make_shared<rd::RadarModel<scalar,Eigen::Dynamic>>(1e-9, 50);
    rd::RadarModelPtr<       scalar,Eigen::Dynamic> radar_model_20         = std::make_shared<rd::RadarModel<scalar,Eigen::Dynamic>>(1e-9, 20);
    const double                                    radar_position_std     = double(100)/double(3);
    const double                                    radar_const_std        = double(1)/double(3);
    const Eigen::Matrix<scalar,1,3,Eigen::RowMajor> radar_position_cov_vec = (Eigen::Matrix<scalar,1,3,Eigen::RowMajor>() << radar_position_std,
                                                                                                                             radar_position_std,
                                                                                                                             radar_position_std).finished();
    const Eigen::Matrix<scalar,3,3,Eigen::RowMajor> radar_position_cov = radar_position_cov_vec.array().sqrt().matrix().asDiagonal();

    kf::noise::NoiseWrapperPtr<3,scalar> radar_pos_cov_obj =
      std::make_shared<kf::noise::NoiseWrapper<3,scalar>>(
        std::make_shared<kf::noise::NormalDistribution<3,false,true,false,scalar>>(Eigen::Matrix<scalar,1,3,Eigen::RowMajor>::Zero(), radar_position_cov),
        "Radar Position");
    kf::noise::NoiseWrapperPtr<1,scalar> radar_const_cov_obj =
      std::make_shared<kf::noise::NoiseWrapper<1,scalar>>(
        std::make_shared<kf::noise::NormalDistribution<1,false,true,false,scalar>>(Eigen::Matrix<scalar,1,1,Eigen::RowMajor>::Zero(), Eigen::Matrix<scalar,1,1,Eigen::RowMajor>::Constant(std::sqrt(radar_const_std))),
        "Radar Constant");
    noise_sources.emplace_back(radar_pos_cov_obj);
    noise_sources.emplace_back(radar_const_cov_obj);

    obs_checker =
      std::make_shared<rrt::obs::ProbabilityDetectionMetricObstacleChecker<kf::dynamics::BasicModelDim,scalar>>(cross_section_model,
                                                                                                                0.1,
                                                                                                                3);
    obs_checker->addRadar(radar_model_50,
                          (Eigen::Matrix<scalar,1,3,Eigen::RowMajor>() << 50e3, 400e3, 0).finished(),
                          radar_pos_cov_obj,
                          radar_const_cov_obj);
    obs_checker->addRadar(radar_model_50,
                          (Eigen::Matrix<scalar,1,3,Eigen::RowMajor>() << -800e3, 200e3, 0).finished(),
                          radar_pos_cov_obj,
                          radar_const_cov_obj);
    obs_checker->addRadar(radar_model_20,
                          (Eigen::Matrix<scalar,1,3,Eigen::RowMajor>() << -600e3, 900e3, 0).finished(),
                          radar_pos_cov_obj,
                          radar_const_cov_obj);
    obs_checker->addRadar(radar_model_20,
                          (Eigen::Matrix<scalar,1,3,Eigen::RowMajor>() << -1500e3, 1000e3, 0).finished(),
                          radar_pos_cov_obj,
                          radar_const_cov_obj);
    obs_checker->addRadar(radar_model_20,
                          (Eigen::Matrix<scalar,1,3,Eigen::RowMajor>() << -1000e3, 1500e3, 0).finished(),
                          radar_pos_cov_obj,
                          radar_const_cov_obj);
    obs_checker->addRadar(radar_model_20,
                          (Eigen::Matrix<scalar,1,3,Eigen::RowMajor>() << 0, 1400e3, 0).finished(),
                          radar_pos_cov_obj,
                          radar_const_cov_obj);

    rrt_tools.obstacle_checker = obs_checker;
  }
  // Setup Sampler
  {
    rrt::sample::point::PointGeneratorPtr<kf::dynamics::BasicModelDim::REF_DIM,scalar> default_point_gen =
      std::make_shared<rrt::sample::point::RandomPointGenerator<kf::dynamics::BasicModelDim::REF_DIM,kf::dynamics::BasicModelDim::REF_DIM-2,scalar>>(
        (Eigen::Matrix<scalar,2,1>() << 900e3, 800e3).finished(),
        (Eigen::Matrix<scalar,2,1>() << -800e3, 700e3).finished());

    rrt::sample::point::PointGeneratorPtr<kf::dynamics::BasicModelDim::REF_DIM,scalar> target_point_gen =
      std::make_shared<rrt::sample::point::CirclePointGenerator<kf::dynamics::BasicModelDim::REF_DIM,0,kf::dynamics::BasicModelDim::REF_DIM-2,scalar>>(
        target_point, 0.1);

    rrt_tools.sampler = std::make_shared<rrt::sample::ReferenceSampler<kf::dynamics::BasicModelDim,scalar>>(
                          100, default_point_gen, target_point_gen);
  }
  // Setup Cost Function
  rrt_tools.cost_function = std::make_shared<rrt::cost::TimeCostFunction<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,
                                                                         kf::dynamics::BasicModelDim::TIME_IND,
                                                                         scalar>>(nominal_velocity);
  // Setup steering function
  {
    // Fillet dist at 90 degree turn: 5009.993335328516
    rrt::steer::SteeringFunctionPtr<2,scalar> temp =
      std::make_shared<rrt::steer::ConstSteeringFunction<2,0,0,scalar>>(50e3, // Edge Length 50e3 works
                                                                        50e3, // Search Radius
                                                                        100, // Number of neighbors to search
                                                                        0);
    rrt_tools.steering_function = std::make_shared<rrt::steer::ReferenceSteeringFunction<kf::dynamics::BasicModelDim,scalar>>(
                                    std::make_shared<rrt::steer::CurveSteeringFunction<0,scalar>>(temp, 0));
  }
  // Setup problem settings
  rrt_tools.problem = std::make_shared<rrt::prob::ReferenceProblem<kf::dynamics::BasicModelDim,scalar>>(
                        starting_point,
                        std::make_shared<rrt::prob::CircleGoal<15,0,13,scalar>>(
                          starting_point.middleCols<kf::dynamics::BasicModelDim::REF_DIM>(kf::dynamics::BasicModelDim::REF_START_IND),
                          (Eigen::Matrix<scalar,1,15,Eigen::RowMajor>() << target_point[0], target_point[1], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).finished(),
                          0.1, // Target radius
                          0, // Max iteration
                          //std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<scalar>(43200*4)), // 48 hour
                          //std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<scalar>(43200*2)), // 24 hour
                          //std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<scalar>(43200)), // 12 hour
                          //std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<scalar>(900)), // 15 min
                          //std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<scalar>(300)), // 5 min
                          //std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<scalar>(150)), // 2.5 min
                          std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<scalar>(1)),
                          999999, //-1, // target cost
                          5e+9)); // Min memory = 5GB
  // Setup KD-tree settings
  rrt_tools.nn_searcher = std::make_shared<rrt::tree::kdt::NearestNeighborSearcher<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,
                                                                                   ReferenceDistanceFunc<kf::dynamics::BasicModelDim,
                                                                                                         scalar,
                                                                                                         Eigen::RowMajor>>>(100,
                                                                                                                            64);
  // Setup edge generator
  kf::Tools<kf::dynamics::BasicModelDim,scalar> tools;
  {
    auto accelerometer_noise =
      std::make_shared<kf::noise::NoiseWrapper<3,scalar>>(
        kf::noise::makeNormalDistribution<3,scalar>(node, "imu.accelerometer.noise"),
        "Accelerometer");
    auto gyroscope_noise =
      std::make_shared<kf::noise::NoiseWrapper<3,scalar>>(
        kf::noise::makeNormalDistribution<3,scalar>(node, "imu.gyroscope.noise"),
        "Gyroscope");
    noise_sources.emplace_back(accelerometer_noise);
    noise_sources.emplace_back(gyroscope_noise);

    auto inertial_measurements_func =
      std::make_shared<kf::sensors::OpenLoopIMU<kf::dynamics::BasicModelDim,scalar>>(accelerometer_noise, gyroscope_noise);
    tools.inertial_measurements_func = inertial_measurements_func;

    // Make Dynamics object
    auto heading_bias_noise =
      std::make_shared<kf::noise::NoiseWrapper<1,scalar>>(
        kf::noise::makeFirstOrderGaussMarkovDrivingNoise<1,scalar>(node, "dynamics.biases.heading.noise"),
        "Heading Bias");
    auto abs_pressure_bias_noise =
      std::make_shared<kf::noise::NoiseWrapper<1,scalar>>(
        kf::noise::makeFirstOrderGaussMarkovDrivingNoise<1,scalar>(node, "dynamics.biases.abs_pressure.noise"),
        "Abs Pressure Bias");
    auto feature_range_bias_noise =
      std::make_shared<kf::noise::NoiseWrapper<1,scalar>>(
        kf::noise::makeFirstOrderGaussMarkovDrivingNoise<1,scalar>(node, "dynamics.biases.feature_range.noise"),
        "Feature Range Bias");
    auto feature_bearing_biases_noise =
      std::make_shared<kf::noise::NoiseWrapper<3,scalar>>(
        kf::noise::makeFirstOrderGaussMarkovDrivingNoise<3,scalar>(node, "dynamics.biases.feature_bearing.noise"),
        "Feature Bearing Bias");
    auto gps_position_biases_noise =
      std::make_shared<kf::noise::NoiseWrapper<3,scalar>>(
        kf::noise::makeFirstOrderGaussMarkovDrivingNoise<3,scalar>(node, "dynamics.biases.gps_position.noise"),
        "GPS Position Bias");
    auto accelerometer_biases_noise =
      std::make_shared<kf::noise::NoiseWrapper<3,scalar>>(
        kf::noise::makeFirstOrderGaussMarkovDrivingNoise<3,scalar>(node, "dynamics.biases.accelerometer.noise"),
        "Accelerometer Bias");
    auto gyroscope_biases_noise =
      std::make_shared<kf::noise::NoiseWrapper<3,scalar>>(
        kf::noise::makeFirstOrderGaussMarkovDrivingNoise<3,scalar>(node, "dynamics.biases.gyroscope.noise"),
        "Gyroscope Bias");
    noise_sources.emplace_back(heading_bias_noise);
    noise_sources.emplace_back(abs_pressure_bias_noise);
    noise_sources.emplace_back(feature_range_bias_noise);
    noise_sources.emplace_back(feature_bearing_biases_noise);
    noise_sources.emplace_back(accelerometer_biases_noise);
    noise_sources.emplace_back(gyroscope_biases_noise);

    tools.dynamics_func =
      std::make_shared<kf::dynamics::BasicModel<scalar>>(
        heading_bias_noise,
        abs_pressure_bias_noise,
        feature_range_bias_noise,
        feature_bearing_biases_noise,
        gps_position_biases_noise,
        accelerometer_biases_noise,
        gyroscope_biases_noise,
        nominal_velocity,
        gravity,
        60,
        60,
        60,
        60,
        60,
        60,
        60);

    // Make controller
    tools.controller =
      std::make_shared<kf::control::OpenLoopController<kf::dynamics::BasicModelDim,scalar,Eigen::RowMajor>>();

    // Make measurement controller
    auto gps_noise =
      std::make_shared<kf::noise::NoiseWrapper<3,scalar>>(
        kf::noise::makeNormalDistribution<3,scalar>(node, "sensors.gps.noise"),
        "GPS");
    auto heading_noise =
      std::make_shared<kf::noise::NoiseWrapper<1,scalar>>(
        kf::noise::makeNormalDistribution<1,scalar>(node, "sensors.heading.noise"),
        "Heading");
    auto altitude_noise =
      std::make_shared<kf::noise::NoiseWrapper<1,scalar>>(
        kf::noise::makeNormalDistribution<1,scalar>(node, "sensors.altitude.noise"),
        "Altitude");
    noise_sources.emplace_back(gps_noise);
    noise_sources.emplace_back(heading_noise);
    noise_sources.emplace_back(altitude_noise);

    tools.measurement_controller =
      std::make_shared<kf::sensors::GPSHeadingAltitudeController<kf::dynamics::BasicModelDim,scalar,Eigen::RowMajor>>(
        std::make_shared<kf::sensors::GPS<     kf::dynamics::BasicModelDim,true,scalar>>(gps_noise,      1),
        std::make_shared<kf::sensors::Heading< kf::dynamics::BasicModelDim,true,scalar>>(heading_noise,  1),
        std::make_shared<kf::sensors::Altitude<kf::dynamics::BasicModelDim,true,scalar>>(altitude_noise, 1));

    // Make mapping object
    tools.mappings = std::make_shared<kf::map::SimpleMapping<kf::dynamics::BasicModelDim,scalar,Eigen::RowMajor>>();

    rrt::edge::FilletEdgeGeneratorPtr<kf::dynamics::BasicModelDim::REF_DIM,scalar> fillet_edge_gen =
      std::make_shared<rrt::edge::EulerSpiralIMUEdgeGenerator<scalar>>(nominal_velocity*fillet_dt,
                                                                       max_curvature,
                                                                       max_curvature_rate,
                                                                       nominal_velocity,
                                                                       nominal_pitch,
                                                                       nominal_down,
                                                                       gravity);
    rrt::edge::EdgeGeneratorPtr<kf::dynamics::BasicModelDim::REF_DIM,scalar> line_edge_gen =
      std::make_shared<rrt::edge::EulerSpiralIMUEdgeGenerator<scalar>>(nominal_velocity*line_dt,
                                                                       max_curvature,
                                                                       max_curvature_rate,
                                                                       nominal_velocity,
                                                                       nominal_pitch,
                                                                       nominal_down,
                                                                       gravity);

    rrt_tools.edge_generator = std::make_shared<rrt::edge::FilletCovarianceEdgeGenerator<kf::dynamics::BasicModelDim,
                                                                                         kf::Versions::NULL_VERSION,
                                                                                         kf::Versions::NULL_VERSION,
                                                                                         scalar>>(
                                 line_edge_gen, fillet_edge_gen, tools, tools);
  }

  // Setup offset vector
  {
    std::vector<Eigen::Matrix<scalar,Eigen::Dynamic,kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,Eigen::RowMajor>> offsets;
    const size_t num_directions = 1;
    const double dir_length = 1500;

    offsets.resize(num_directions);
    for(size_t dir_it = 0; dir_it < num_directions; ++dir_it)
    {
      Eigen::Matrix<scalar,1,kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,Eigen::RowMajor> temp_point(starting_point);

      temp_point[kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::NORTH_IND] += dir_length * std::cos(starting_point[kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::YAW_IND] + (double(dir_it) * (rrt::math::twoPi<double>()/double(num_directions))));
      temp_point[kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::EAST_IND]  += dir_length * std::sin(starting_point[kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::YAW_IND] + (double(dir_it) * (rrt::math::twoPi<double>()/double(num_directions))));
      temp_point[kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::YAW_IND]   += (double(dir_it) * (rrt::math::twoPi<double>()/double(num_directions)));

      rrt_tools.edge_generator->makeEdge(starting_point, temp_point, offsets[dir_it]);
    }

    size_t total_len = 0;
    for(size_t dir_it = 0; dir_it < num_directions; ++dir_it)
    {
      total_len += offsets[dir_it].rows();
    }

    rrt_tools.starting_offset.resize(total_len, Eigen::NoChange);
    total_len = 0;
    for(size_t dir_it = 0; dir_it < num_directions; ++dir_it)
    {
      rrt_tools.starting_offset.middleRows(total_len, offsets[dir_it].rows()) = offsets[dir_it];
      total_len += offsets[dir_it].rows();
    }
  }

  /// Solve with visibility graph planner
  std::cout << "Starting Visibility Graph planner" << std::endl;
  const auto start_time = std::chrono::high_resolution_clock::now();
  const Eigen::Matrix<scalar,Eigen::Dynamic,kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,Eigen::RowMajor> vis_graph_solution =
    rrt::search::radarVisGraphSearch<kf::dynamics::BasicModelDim,scalar,Eigen::RowMajor>(obs_checker,
                                                                         rrt_tools.edge_generator,
                                                                         starting_point,
                                                                         target_point,
                                                                         (Eigen::Matrix<scalar,2,2,Eigen::RowMajor>() << -1700e3, -100e3, 100e3, 1500e3).finished(),
                                                                         0.09,
                                                                         30);
  const auto end_time = std::chrono::high_resolution_clock::now();
  std::cout << "Visibility Graph planner found a solution in " << std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count() << " seconds" << std::endl;
  std::cout << "Solutions end time: " << vis_graph_solution.bottomRows<1>()[kf::dynamics::BasicModelDim::TIME_IND] << std::endl;

  // Set as target cost for RRT
  rrt_tools.problem->target_cost = nominal_velocity * vis_graph_solution.bottomRows<1>()[kf::dynamics::BasicModelDim::TIME_IND];

  /// Solve RRT path planning problem
  rrt::search::SolutionPtr<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,scalar> rrt_solution;

  std::cout << "Starting RRT" << std::endl;

  //rrt_solution = rrt::search::filletRRTSearch<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,rrt::search::filletRRTFlags(),scalar>(rrt_tools);
  //rrt_solution = rrt::search::filletRRTSearch<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,rrt::search::addRepropagation(rrt::search::filletRRTStarFlags(false, true),true,false,true),scalar>(rrt_tools);
  rrt_solution = rrt::search::filletRRTSearch<kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,rrt::search::addRepropagation(rrt::search::filletRRTStarSmartFlags(false,false,true),true,false,true),scalar>(rrt_tools);

  std::cout << "RRT found solution in " << std::chrono::duration_cast<std::chrono::seconds>(rrt_solution->time).count() << " seconds and the solution is of cost " << rrt_solution->cost << std::endl;
  if(rrt_solution->answer.rows() > 0)
  {
    std::cout << "Solutions end time: " << rrt_solution->answer.bottomRows<1>()[kf::dynamics::BasicModelDim::TIME_IND] << std::endl;
  }
  std::cout << "Number of nodes added:    " << counter_logger->cgetNumberNodesAdded() << std::endl;
  std::cout << "Number of nodes removed:  " << counter_logger->cgetNumberNodesRemoved() << std::endl;
  std::cout << "Number of rewires:        " << counter_logger->cgetNumberRewires() << std::endl;
  std::cout << "Number of repropagations: " << counter_logger->cgetNumberRepropagations() << std::endl;
  std::vector<double> time_dist_vec(rrt_solution->answer.rows());
  std::vector<double> x_vec(rrt_solution->answer.rows());
  double max_ptp_dist = -std::numeric_limits<double>::infinity();
  double min_ptp_dist =  std::numeric_limits<double>::infinity();
  for(Eigen::Index point_it = 1; point_it < rrt_solution->answer.rows(); ++point_it)
  {
    const double dist = std::abs(rrt_solution->answer(point_it, 0) - rrt_solution->answer(point_it-1, 0));

    time_dist_vec[point_it] = dist;
    x_vec[point_it] = point_it;

    max_ptp_dist = std::max<double>(max_ptp_dist, dist);
    min_ptp_dist = std::min<double>(min_ptp_dist, dist);

    rrt_solution->answer(point_it, kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::YAW_IND) =
      rrt::math::angleSum<double>(rrt_solution->answer(point_it, kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::YAW_IND));
  }
  std::cout << "Max PtP time difference is " << max_ptp_dist << " and the min is " << min_ptp_dist << std::endl;

/*  matplotlibcpp::figure();
  matplotlibcpp::plot<double>(x_vec, time_dist_vec);*/

  /// Plot results
  matplotlibcpp::figure();
  matplotlibcpp::xlabel("North");
  matplotlibcpp::ylabel("East");
  matplotlibcpp::named_plot<double,double>("Bounds",
                                           {-1700e3,  100e3, 100e3,  -1700e3, -1700e3},
                                           {-100e3,  -100e3, 1500e3,  1500e3, -100e3},
                                           "k");
  matplotlibcpp::named_plot<double,double>("Radar Stations",
                                           {50e3,  -800e3, -600e3, -1500e3, -1000e3, 0},
                                           {400e3,  200e3,  900e3,  1000e3,  1500e3, 1400e3},
                                           "dr");
  matplotlibcpp::named_plot<double,double>("Tree",
                                           toVec(tree_logger->buffer.at(0).col(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::NORTH_IND)),
                                           toVec(tree_logger->buffer.at(0).col(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::EAST_IND)),
                                           "y");
  std::for_each(std::next(tree_logger->buffer.cbegin()),
                tree_logger->buffer.cend(),
                [](const Eigen::Matrix<scalar,Eigen::Dynamic,kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,Eigen::RowMajor>& edge)
                {
                  matplotlibcpp::plot<double,double>(toVec(edge.col(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::NORTH_IND)),
                                                     toVec(edge.col(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::EAST_IND)),
                                                     "y");
                });
  matplotlibcpp::named_plot<double,double>("Start", {start_point[0]}, {start_point[1]}, "ob");
  matplotlibcpp::named_plot<double,double>("Goal", {target_point[0]}, {target_point[1]}, "xb");
  matplotlibcpp::named_plot<double,double>("RRT Solution",
                                           toVec(rrt_solution->answer.col(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::NORTH_IND)),
                                           toVec(rrt_solution->answer.col(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::EAST_IND)),
                                           "g");
  matplotlibcpp::named_plot<double,double>("PDVG Solution",
                                           toVec(vis_graph_solution.col(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::NORTH_IND)),
                                           toVec(vis_graph_solution.col(kf::dynamics::BasicModelDim::REF_START_IND + kf::dynamics::BasicModelDim::REF::EAST_IND)),
                                           "r");
  matplotlibcpp::legend();
  matplotlibcpp::set_aspect_equal();
  matplotlibcpp::title("Birds Eye View");
  //matplotlibcpp::show();

  {
    Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor> probability_of_detection;
    Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor> probability_of_detection_std;
    Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor> radar_cross_section;
    Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor> range;
    obs_checker->getPlotInfo(vis_graph_solution, probability_of_detection, probability_of_detection_std, radar_cross_section, range);

    matplotlibcpp::figure();
    matplotlibcpp::subplot(3, 1, 1);
    matplotlibcpp::xlabel("Time");
    matplotlibcpp::ylabel("PD");
    matplotlibcpp::named_plot<double,double>("PD",
                                             toVec(vis_graph_solution.col(kf::dynamics::BasicModelDim::TIME_IND)),
                                             toVec(probability_of_detection),
                                             "b");
    matplotlibcpp::named_plot<double,double>("PD + 3 sigma",
                                             toVec(vis_graph_solution.col(kf::dynamics::BasicModelDim::TIME_IND)),
                                             toVec(probability_of_detection.array() + (scalar(3)*probability_of_detection_std.array())),
                                             "b--");

    matplotlibcpp::named_plot<double,double>("PD threshold",
                                             std::vector<double>({vis_graph_solution.topRows(1).col(kf::dynamics::BasicModelDim::TIME_IND)[0], vis_graph_solution.bottomRows(1).col(kf::dynamics::BasicModelDim::TIME_IND)[0]}),
                                             std::vector<double>({0.1, 0.1}),
                                             "r--");
    matplotlibcpp::legend();
    matplotlibcpp::subplot(3, 1, 2);
    matplotlibcpp::xlabel("Time");
    matplotlibcpp::ylabel("Range");
    matplotlibcpp::named_plot<double,double>("Range",
                                             toVec(vis_graph_solution.col(kf::dynamics::BasicModelDim::TIME_IND)),
                                             toVec(range),
                                             "b");
    matplotlibcpp::subplot(3, 1, 3);
    matplotlibcpp::xlabel("Time");
    matplotlibcpp::ylabel("RCS");
    matplotlibcpp::named_plot<double,double>("RCS",
                                             toVec(vis_graph_solution.col(kf::dynamics::BasicModelDim::TIME_IND)),
                                             toVec(radar_cross_section),
                                             "b");
//    matplotlibcpp::show();
  }

  kf::plot::plotErrorBudget<kf::dynamics::BasicModelDim,kf::NULL_VERSION,scalar,Eigen::RowMajor>(
    vis_graph_solution.middleCols<kf::dynamics::BasicModelDim::REF_DIM>(kf::dynamics::BasicModelDim::REF_START_IND),
    starting_point,
    tools,
    noise_sources,
    [&obs_checker] (const Eigen::Ref<const Eigen::Matrix<scalar,Eigen::Dynamic,kf::dynamics::BasicModelDim::LINCOV::FULL_STATE_LEN,Eigen::RowMajor>>& state_vector) -> Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor>
      {
        Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor> probability_of_detection;
        Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor> probability_of_detection_std;
        Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor> radar_cross_section;
        Eigen::Matrix<scalar,1,Eigen::Dynamic,Eigen::RowMajor> range;

        obs_checker->getPlotInfo(state_vector, probability_of_detection, probability_of_detection_std, radar_cross_section, range);

        return probability_of_detection_std.array() * scalar(3);
      },
    "3-sigma PD",
    true);

  std::cout << "PDVG figures" << std::endl;
  const kf::map::MappingsBasePtr<kf::dynamics::BasicModelDim,scalar,Eigen::RowMajor>
    temp_mappings = std::make_shared<kf::map::SimpleMapping<kf::dynamics::BasicModelDim,scalar,Eigen::RowMajor>>();
  std::array<bool, 4> plot_types = {true, true, true, true};
  kf::plot::plotAllStatistics<kf::dynamics::BasicModelDim,scalar,Eigen::RowMajor>(
    vis_graph_solution,
    temp_mappings,
    {
      std::make_pair("Position", std::make_tuple(3,
                                                 kf::dynamics::BasicModelDim::REF::POS_START_IND,
                                                 kf::dynamics::BasicModelDim::TRUTH_DISP::POS_START_IND,
                                                 kf::dynamics::BasicModelDim::ERROR::POS_START_IND)),
      std::make_pair("Euler", std::make_tuple(3,
                                              kf::dynamics::BasicModelDim::REF::EULER_START_IND,
                                              kf::dynamics::BasicModelDim::TRUTH_DISP::EULER_START_IND,
                                              kf::dynamics::BasicModelDim::ERROR::EULER_START_IND)),
      std::make_pair("Velocity", std::make_tuple(3,
                                                 kf::dynamics::BasicModelDim::REF::VEL_START_IND,
                                                 kf::dynamics::BasicModelDim::TRUTH_DISP::VEL_START_IND,
                                                 kf::dynamics::BasicModelDim::ERROR::VEL_START_IND)),
      std::make_pair("Heading Bias", std::make_tuple(1,
                                                     -1,
                                                     kf::dynamics::BasicModelDim::TRUTH::HEADING_BIAS_IND,
                                                     kf::dynamics::BasicModelDim::TRUTH_DISP::HEADING_BIAS_IND)),
      std::make_pair("Abs Pressure Bias", std::make_tuple(1,
                                                          -1,
                                                          kf::dynamics::BasicModelDim::TRUTH::ABS_PRESSURE_BIAS_IND,
                                                          kf::dynamics::BasicModelDim::TRUTH_DISP::ABS_PRESSURE_BIAS_IND)),
      std::make_pair("Gyro Bias", std::make_tuple(3,
                                                  -1,
                                                  kf::dynamics::BasicModelDim::TRUTH::GYRO_BIAS_START_IND,
                                                  kf::dynamics::BasicModelDim::TRUTH_DISP::GYRO_BIAS_START_IND)),
      std::make_pair("Accel Bias", std::make_tuple(3,
                                                   -1,
                                                   kf::dynamics::BasicModelDim::TRUTH_DISP::ACCEL_BIAS_START_IND,
                                                   kf::dynamics::BasicModelDim::ERROR::ACCEL_BIAS_START_IND)),
    },
    plot_types,
    true);

/*  std::cout << "RRT figures" << std::endl;
  kf::plot::plotAllStatistics<kf::dynamics::BasicModelDim,scalar,Eigen::RowMajor>(
    rrt_solution->answer,
    temp_mappings,
    {
      std::make_pair("Position", std::make_tuple(kf::dynamics::BasicModelDim::REF::POS_START_IND,
                                                 kf::dynamics::BasicModelDim::TRUTH_DISP::POS_START_IND,
                                                 kf::dynamics::BasicModelDim::ERROR::POS_START_IND)),
      std::make_pair("Euler", std::make_tuple(kf::dynamics::BasicModelDim::REF::EULER_START_IND,
                                              kf::dynamics::BasicModelDim::TRUTH_DISP::EULER_START_IND,
                                              kf::dynamics::BasicModelDim::ERROR::EULER_START_IND)),
      std::make_pair("Velocity", std::make_tuple(kf::dynamics::BasicModelDim::REF::VEL_START_IND,
                                                 kf::dynamics::BasicModelDim::TRUTH_DISP::VEL_START_IND,
                                                 kf::dynamics::BasicModelDim::ERROR::VEL_START_IND)),
      std::make_pair("Gyro Bias", std::make_tuple(-1,
                                                  kf::dynamics::BasicModelDim::TRUTH::GYRO_BIAS_START_IND,
                                                  kf::dynamics::BasicModelDim::TRUTH_DISP::GYRO_BIAS_START_IND)),
      std::make_pair("Accel Bias", std::make_tuple(-1,
                                                   kf::dynamics::BasicModelDim::TRUTH_DISP::ACCEL_BIAS_START_IND,
                                                   kf::dynamics::BasicModelDim::ERROR::ACCEL_BIAS_START_IND)),
    },
    false);

  matplotlibcpp::show();*/

  exit(EXIT_SUCCESS);
}
/* radar_path_planning_demo.cpp */
